<?php
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth', 'web', 'can:administration']], function() {
    Route::get('/inso/migrate', 'Administrator\ArtisanActionsController@migrate')
        ->name('migration');
    Route::get('/inso/clear', 'Administrator\ArtisanActionsController@clear')
        ->name('clear');
});
