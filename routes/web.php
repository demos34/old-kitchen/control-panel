<?php

use App\Models\Consult\ConsultTypesStatus;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Auth::routes();

/*
 *  Auth routes -> begin
 */

// Authentication Routes...
Route::get('login', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@showLoginForm'
]);
Route::post('login', [
    'as' => '',
    'uses' => 'Auth\LoginController@login'
]);
Route::post('logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
]);

// Password Reset Routes...
Route::post('password/email', [
    'as' => 'password.email',
    'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
]);
Route::get('password/reset', [
    'as' => 'password.request',
    'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'
]);
Route::post('password/reset', [
    'as' => 'password.update',
    'uses' => 'Auth\ResetPasswordController@reset'
]);
Route::get('password/reset/{token}', [
    'as' => 'password.reset',
    'uses' => 'Auth\ResetPasswordController@showResetForm'
]);

// Registration Routes...
/*
 * register->login
 */
Route::get('register', [
    'as' => 'register',
    'uses' => 'Auth\LoginController@showLoginForm'
]);
Route::post('register', [
    'as' => '',
    'uses' => 'Auth\LoginController@showLoginForm'
]);

/*
 * Default routes
Route::get('register', [
    'as' => 'register',
    'uses' => 'Auth\RegisterController@showRegistrationForm'
]);
Route::post('register', [
    'as' => '',
    'uses' => 'Auth\RegisterController@register'
]);

*/

/*
 * Auth routes -> ends
 */


Route::get('/', 'Home\HomeController@index')->name('home');
Route::get('/home', 'Home\HomeController@index')->name('home');

Route::namespace('Administrator')->prefix('administrator')->name('administrator.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/administrator', 'AdministratorController',
            [
                'except' => []
            ]);
    });

Route::namespace('Roles')->prefix('roles')->name('roles.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/roles', 'RolesController',
            [
                'except' => []
            ]);
    });

Route::namespace('Tags')->prefix('tags')->name('tags.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/keywords', 'KeywordsController',
            [
                'except' => []
            ]);
    });

Route::namespace('Tags')->prefix('tags')->name('tags.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/tags', 'TagsController',
            [
                'except' => []
            ]);
    });

Route::namespace('Tags')->prefix('tags')->name('tags.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/index', 'IndexTagsController',
            [
                'except' => []
            ]);
    });

Route::namespace('Recipes')->prefix('recipes')->name('recipes.')
    ->middleware('can:administration')->group(function () {
        Route::get('/recipes/{recipes:slug}', 'RecipesController@show');
    });

Route::namespace('Recipes')->prefix('recipes')->name('recipes.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/recipes', 'RecipesController',
            [
                'except' => []
            ]);
    });

Route::namespace('Recipes')->prefix('recipes')->name('recipes.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/posts', 'ProductsController',
            [
                'except' => []
            ]);
    });;
//
//Route::namespace('Recipes')->prefix('recipes')->name('recipes.')
//    ->middleware('can:administration')->group(function (){
//        Route::get('/types/{type:slug}', 'TypesController@show');
//    });

Route::namespace('Recipes')->prefix('recipes')->name('recipes.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/types', 'TypesController',
            [
                'except' => []
            ]);
    });

Route::namespace('Drafts')->prefix('drafts')->name('drafts.')
    ->middleware('can:administration')->group(function () {
        Route::get('/recipes/{recipe:slug|id}', 'DraftRecipesController@show');
    });

Route::namespace('Drafts')->prefix('drafts')->name('drafts.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/recipes', 'DraftRecipesController',
            [
                'except' => []
            ]);
    });

Route::namespace('Drafts')->prefix('drafts')->name('drafts.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/types', 'DraftsTypesController',
            [
                'except' => []
            ]);
    });

Route::namespace('Drafts')->prefix('drafts')->name('drafts.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/posts', 'DraftProductsController',
            [
                'except' => []
            ]);
    });

Route::namespace('Users')->prefix('users')->name('users.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/users', 'UsersController',
            [
                'except' => []
            ]);
    });

Route::namespace('Users')->prefix('users')->name('users.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/password', 'PasswordController',
            [
                'except' => []
            ]);
    });

Route::namespace('Users')->prefix('users')->name('users.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/profile', 'ProfilesController',
            [
                'except' => []
            ]);
    });

Route::namespace('Home')->prefix('home')->name('home.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/home', 'HomeController',
            [
                'except' => []
            ]);
    });

//Route::namespace('Home')->prefix('home')->name('home.')
//    ->middleware('can:administration')->group(function (){
//    Route::resource('/contacts', 'ContactUsController',
//        [
//            'except' => []
//        ]);
//});

Route::namespace('Images')->prefix('images')->name('images.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/images', 'ImagesController',
            [
                'except' => []
            ]);
    });

Route::namespace('Ratings')->prefix('ratings')->name('ratings.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/ratings', 'RatingsController',
            [
                'except' => []
            ]);
    });

Route::namespace('Ratings')->prefix('ratings')->name('ratings.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/values', 'RatingValuesController',
            [
                'except' => []
            ]);
    });

Route::namespace('Comments')->prefix('comments')->name('comments.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/comments', 'CommentsController',
            [
                'except' => []
            ]);
    });

Route::namespace('Comments')->prefix('comments')->name('comments.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/values', 'RatingCommentsValuesController',
            [
                'except' => []
            ]);
    });

Route::namespace('Books')->prefix('books')->name('books.')
    ->middleware('can:administration')->group(function () {
        Route::get('/books/create', 'BooksController@create');
    });

Route::namespace('Books')->prefix('books')->name('books.')
    ->middleware('can:administration')->group(function () {
        Route::get('/books/{books:slug}', 'BooksController@show');
    });

Route::namespace('Books')->prefix('books')->name('books.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/books', 'BooksController',
            [
                'except' => []
            ]);
    });

Route::namespace('Books')->prefix('books')->name('books.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/buy', 'BuyFromController',
            [
                'except' => []
            ]);
    });

Route::namespace('Subscribers')->prefix('subscribers')->name('subscribers.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/subscribers', 'SubscribersController',
            [
                'except' => []
            ]);
    });


/*
 * Publish routes
 */


Route::namespace('Drafts')->prefix('drafts')->name('drafts.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/publish', 'PublishController',
            [
                'except' => ['index', 'show', 'edit', 'destroy', 'create', 'store']
            ]);
    });

Route::namespace('Drafts')->prefix('drafts')->name('drafts.')
    ->middleware('can:administration')->group(function () {
        Route::resource('/preparing', 'PreparingController',
            [
                'except' => ['index', 'show', 'edit', 'destroy', 'create', 'store']
            ]);
    });


Route::group(['middleware' => ['can:administration', 'archive']], function () {

        Route::get('/analyst/archive', 'Administrator\AnalystController@archive')
            ->name('analyst.archive');
        Route::get('/analyst/archive/sort/{value}/{where}', 'Administrator\AnalystController@archiveSort')
            ->name('analyst.archive.sort');
        Route::get('/analyst/archive/delete', 'Administrator\AnalystController@archiveDelete')
            ->name('analyst.archive.delete');

        Route::patch('/analyst/archive/chosen', 'Administrator\AnalystController@archiveChosen')
            ->name('analyst.archive.delete-chosen');


        Route::get('/analyst', 'Administrator\AnalystController@index')
            ->name('analyst.index');
        Route::get('/analyst/sort/{value}/{where}', 'Administrator\AnalystController@sort')
            ->name('analyst.sort');
        Route::get('/analyst/look/{value}/{look}', 'Administrator\AnalystController@look')
            ->name('analyst.look');
        Route::get('/analyst/by-date/{key}', 'Administrator\AnalystController@byDate')
            ->name('analyst.by-date');

        Route::get('/analyst/custom-analyst', 'Administrator\AnalystController@custom')
            ->name('analyst.custom-analyst');
        Route::get('/analyst/custom-analyst/sort/{analystCustom}/{value}/{where}', 'Administrator\AnalystController@customSort')
            ->name('analyst.custom-analyst.sort');
        Route::post('/analyst/custom-analyst', 'Administrator\AnalystController@customStore')
            ->name('analyst.custom-analyst.store');
        Route::get('/analyst/custom-analyst/{analystCustom}', 'Administrator\AnalystController@customShow')
            ->name('analyst.custom-analyst.show');
        Route::delete('/analyst/custom-analyst/{analystCustom}', 'Administrator\AnalystController@customDelete')
            ->name('analyst.custom-analyst.delete');

        Route::get('/analyst/in-progress', 'Administrator\AnalystController@inProgress')
            ->name('analyst.in-progress');

        //counters

        Route::get('/counter/index/{key}', 'Administrator\CountsController@index')
            ->name('counter.index');
        Route::get('/counter/by/{countType}/{key}', 'Administrator\CountsController@by')
            ->name('counter.by');
        Route::get('/counter/custom', 'Administrator\CountsController@custom')
            ->name('counter.custom');
        Route::post('/counter/custom', 'Administrator\CountsController@customStore')
            ->name('counter.custom.store');
        Route::get('/counter/custom/{countableCustom}', 'Administrator\CountsController@customShow')
        ->name('counter.custom.show');
        Route::delete('/counter/custom/{countableCustom}', 'Administrator\CountsController@customDelete')
            ->name('counter.custom.delete');




        Route::get('/counter/admin/{pass}', 'Administrator\CountsController@admin')
            ->name('counter.admin');
        Route::get('/counter/change/{pass}', 'Administrator\CountsController@adminChangeDraftToRecipes')
            ->name('counter.change');



    /*
     * partners
     */

    Route::get('/partners', 'Partners\PartnersController@index')
        ->name('partners');
    Route::post('/partners', 'Partners\PartnersController@store')
        ->name('partners-post');
    Route::get('/partners/{partner}', 'Partners\PartnersController@show')
        ->name('partners-show');
    Route::delete('/partners/{partner}', 'Partners\PartnersController@destroy')
        ->name('partners-destroy');
    Route::patch('/partners/{partner}', 'Partners\PartnersController@update')
        ->name('partners-update');

    /*
     * contact us controller
     */

    Route::get('/contacts/{messageType}', 'Home\ContactUsController@index')
        ->name('contacts');

    Route::get('/contacts/delete/{message}', 'Home\ContactUsController@delete')
        ->name('delete-message');

    /*
     * blog
     */
    Route::get('/posts', 'Home\PostsController@index')
        ->name('blog');
    Route::post('/posts', 'Home\PostsController@store')
        ->name('blog-store');
    Route::get('/posts/{post}', 'Home\PostsController@show')
        ->name('blog-show');
    Route::put('/posts/{post}', 'Home\PostsController@update')
        ->name('blog-update');
    Route::delete('/posts/{post}', 'Home\PostsController@destroy')
        ->name('blog-delete');

    /*
     * youtube
     */
    Route::get('/tube', 'Home\PostsController@tube')
        ->name('youtube');
    Route::post('/tube', 'Home\PostsController@tubeStore')
        ->name('tube-store');
    Route::get('/tube/{tube}', 'Home\PostsController@tubeShow')
        ->name('tube-show');
    Route::put('/tube/{tube}', 'Home\PostsController@tubeUpdate')
        ->name('tube-update');
    Route::delete('/tube/{tube}', 'Home\PostsController@tubeDestroy')
        ->name('tube-delete');

    //consult

    Route::get('/consult', 'Administrator\ConsultController@index')
        ->name('consult-index');
    Route::get('/consult/form', 'Administrator\ConsultController@form')
        ->name('consult-form');
    Route::put('/consult/form', 'Administrator\ConsultController@categoryStore')
        ->name('consult-form-category-store');
    Route::put('/consult/form/{consultCategory}', 'Administrator\ConsultController@categoryUpdate')
        ->name('consult-form-category-update');
    Route::patch('/consult/form', 'Administrator\ConsultController@subcategoryStore')
        ->name('consult-form-subcategory-store');
    Route::get('/consult/form/{consultCategory}', 'Administrator\ConsultController@categoryShow')
        ->name('consult-form-category-show');
    Route::get('/consult/form-sub/{consultSubcategory}', 'Administrator\ConsultController@subcategoryShow')
        ->name('consult-form-subcategory-show');
    Route::patch('/consult/form-sub/{consultSubcategory}', 'Administrator\ConsultController@subcategoryUpdate')
        ->name('consult-form-subcategory-update');

    Route::delete('/consult/form/{consultCategory}', 'Administrator\ConsultController@categoryDelete')
        ->name('consult-form-category-delete');
    Route::delete('/consult/form-sub/{consultSubcategory}', 'Administrator\ConsultController@subcategoryDelete')
        ->name('consult-form-subcategory-delete');


    Route::get('/consult/form-show', 'Administrator\ConsultController@formShow')
        ->name('consult-form-show');

    Route::post('/consult/form/show/{consultType}', 'Administrator\ConsultController@formShowPost')
        ->name('consult-form-show-post');


    Route::get('/consult/inbox/{consultMessage}', 'Administrator\ConsultController@inbox')
        ->name('consult-message-show');

    Route::post('/consult/update-spice/{client}', 'Administrator\ConsultController@updateSpice')
        ->name('consult-update-spice');

    Route::get('/consult/status/{consultStatus}/{client}', 'Administrator\ConsultController@status')
        ->name('consult-status-update');

    Route::get('/consult/status-view/{consultStatus}/{consultType}', 'Administrator\ConsultController@viewAllByStatutes')
        ->name('consult-status-index');

    Route::get('/consult/turn-on-of', 'Administrator\ConsultController@turnOnOffConsult')
        ->name('consult-turn');

    Route::get('/consult/services', 'Administrator\ConsultController@services')
        ->name('consult-services');

    Route::get('/consult/service/{consultService}', 'Administrator\ConsultController@serviceShow')
        ->name('consult-service-show');
    Route::post('/consult/service', 'Administrator\ConsultController@serviceStore')
        ->name('consult-service-store');
    Route::put('/consult/service/{consultService}', 'Administrator\ConsultController@serviceUpdate')
        ->name('consult-service-update');
    Route::delete('/consult/service/{consultService}', 'Administrator\ConsultController@serviceDelete')
        ->name('consult-service-delete');
    Route::patch('/consult/service/{consultService}', 'Administrator\ConsultController@servicePrice')
        ->name('consult-service-set-price');


    Route::get('/consult/service-type/{consultType}', 'Administrator\ConsultController@serviceTypeShow')
        ->name('consult-type-show');

    Route::get('/consult/business-form', 'Administrator\ConsultController@businessFormShow')
        ->name('consult-business-form-show');

    Route::get('/consult/comments/{consultType}', 'Administrator\ConsultController@commentsIndex')
        ->name('consult-comments-index');
    Route::get('/consult/comments/show-hide/{consultComment}/{value}', 'Administrator\ConsultController@showHide')
        ->name('consult-comments-show-hide');

    Route::get('/consult/consult-types/turn/{id}/{value}', 'Administrator\ConsultController@turnOnOff')
        ->name('consult-turn-on-off');

    Route::get('/consult/description/{consultType}', 'Administrator\ConsultController@description')
        ->name('consult-description');
    Route::post('/consult/description/{consultType}', 'Administrator\ConsultController@descriptionStore')
        ->name('consult-description-store');
    Route::put('/consult/description/{consultDescription}', 'Administrator\ConsultController@descriptionUpdate')
        ->name('consult-description-update');


});


