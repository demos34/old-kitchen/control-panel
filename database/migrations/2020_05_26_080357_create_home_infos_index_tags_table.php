<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeInfosIndexTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_info_index_tag', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('home_info_id');
            $table->unsignedBigInteger('index_tag_id');
            $table->timestamps();

            $table->foreign('home_info_id')->references('id')->on('home_infos')->cascadeOnDelete()->onUpdate('cascade');
            $table->foreign('index_tag_id')->references('id')->on('index_tags')->cascadeOnDelete()->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_infos_index_tags');
    }
}
