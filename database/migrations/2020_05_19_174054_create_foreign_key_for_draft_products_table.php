<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeyForDraftProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('draft_products', function (Blueprint $table) {
            $table->foreign('draft_recipe_id')->references('id')->on('draft_recipes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('draft_products', function (Blueprint $table) {
            $table->dropForeign('draft_products_draft_recipe_id');
        });
    }
}
