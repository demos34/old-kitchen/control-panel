<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadedImgFromUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploaded_img_from_users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('img_id')->unique();
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('img_id')->references('id')->on('images')->cascadeOnDelete();
            $table->foreign('user_id')->references('id')->on('users')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploaded_img_from_users');
    }
}
