<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToAnalyzeCustomRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('analyze_custom_records', function (Blueprint $table) {
            $table->unsignedBigInteger('analyst_custom_id')->nullable();
            $table->foreign('analyst_custom_id')->references('id')->on('analyst_customs')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('analyze_custom_records', function (Blueprint $table) {
            //
        });
    }
}
