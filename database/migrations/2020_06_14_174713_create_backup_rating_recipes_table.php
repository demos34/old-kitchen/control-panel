<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBackupRatingRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backup_rating_recipes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('rating_id');
            $table->unsignedBigInteger('draft_recipe_id');
            $table->unsignedBigInteger('cookie')->nullable();
            $table->timestamps();

            $table->foreign('rating_id')->references('id')->on('ratings')->cascadeOnDelete();
            $table->foreign('draft_recipe_id')->references('id')->on('draft_recipes')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backup_rating_recipes');
    }
}
