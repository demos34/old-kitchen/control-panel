<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimeToCooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_to_cooks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('draft_recipe_id');
            $table->integer('days')->nullable();
            $table->integer('hours')->nullable();
            $table->integer('minute')->nullable();
            $table->timestamps();

            $table->foreign('draft_recipe_id')->references('id')->on('draft_recipes')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_to_cooks');
    }
}
