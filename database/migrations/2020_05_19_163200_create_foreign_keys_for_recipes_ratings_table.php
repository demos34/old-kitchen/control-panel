<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeysForRecipesRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recipes_ratings', function (Blueprint $table) {
            $table->foreign('recip_id')->references('id')->on('recipes')->onDelete('cascade');
            $table->foreign('rating_id')->references('id')->on('ratings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recipes_ratings', function (Blueprint $table) {
            $table->dropForeign('recipes_ratings_recip_id');
            $table->dropForeign('recipes_ratings_rating_id');
        });
    }
}
