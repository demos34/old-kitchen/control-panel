<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeysForRecipesImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recipes_images', function (Blueprint $table) {
            $table->foreign('recip_id')->references('id')->on('recipes')->onDelete('cascade');
            $table->foreign('img_id')->references('id')->on('images')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recipes_images', function (Blueprint $table) {
            $table->dropForeign('recipes_images_recip_id');
            $table->dropForeign('recipes_images_img_id');
        });
    }
}
