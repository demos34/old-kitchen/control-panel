<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountableCustomRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countable_custom_records', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('countable_custom_id');
            $table->unsignedBigInteger('count_type_id');
            $table->integer('countable');
            $table->bigInteger('counts');
            $table->timestamps();

            $table->foreign('countable_custom_id')->references('id')->on('countable_customs')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('count_type_id')->references('id')->on('count_types')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countable_custom_records');
    }
}
