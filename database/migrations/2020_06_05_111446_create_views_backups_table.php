<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViewsBackupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('views_backups', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('draft_recipe_id')->unique();
            $table->integer('views');
            $table->timestamps();

            $table->foreign('draft_recipe_id')->references('id')->on('draft_recipes')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('views_backups');
    }
}
