<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnalystArchivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analyst_archives', function (Blueprint $table) {$table->id();
            $table->string('country')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('session_id')->nullable();
            $table->string('referer')->nullable();
            $table->unsignedBigInteger('site_id')->nullable();
            $table->timestamps();
            $table->foreign('site_id')->references('id')->on('sites')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analyst_archives');
    }
}
