<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('count_type_id');
            $table->unsignedBigInteger('site_id');
            $table->string('countable');
            $table->integer('count');
            $table->timestamps();

            $table->foreign('count_type_id')->references('id')->on('count_types')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('site_id')->references('id')->on('sites')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counts');
    }
}
