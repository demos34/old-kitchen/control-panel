<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientConsultSubcategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_consult_subcategory', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('consult_subcategory_id');
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('consult_subcategory_id')->references('id')->on('consult_subcategories')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_consult_subcategory');
    }
}
