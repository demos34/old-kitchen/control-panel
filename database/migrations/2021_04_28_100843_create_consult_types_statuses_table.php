<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsultTypesStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consult_types_statuses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('consult_type_id');
            $table->boolean('is_on');
            $table->timestamps();

            $table->foreign('consult_type_id')->references('id')->on('consult_types')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consult_types_statuses');
    }
}
