<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIsReadyToPublishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('is_ready_to_publishes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('draft_recipe_id');
            $table->boolean('is_ready_to_publish');
            $table->timestamps();

            $table->foreign('draft_recipe_id')->references('id')->on('draft_recipes')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('is_ready_to_publishes');
    }
}
