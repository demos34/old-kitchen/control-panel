<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_information', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('client_id')->unique();
            $table->unsignedBigInteger('spice_id');
            $table->string('fav_foods')->nullable();
            $table->string('fav_rest')->nullable();
            $table->string('allergic')->nullable();
            $table->longText('info')->nullable();
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients')
            ->cascadeOnUpdate()
            ->cascadeOnDelete();
            $table->foreign('spice_id')->references('id')->on('spices')
            ->cascadeOnUpdate()
            ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_information');
    }
}
