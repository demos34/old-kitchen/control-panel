<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsultDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consult_descriptions', function (Blueprint $table) {
            $table->id();
            $table->text('description');
            $table->unsignedBigInteger('consult_type_id');
            $table->timestamps();

            $table->foreign('consult_type_id')->references('id')->on('consult_types')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consult_descriptions');
    }
}
