<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDraftUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('draft_user', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('draft_id')->unique();
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('draft_id')->references('id')->on('draft_recipes')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('draft_user');
    }
}
