<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countables', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('count_type_id');
            $table->integer('countable');
            $table->bigInteger('counts')->default(1);
            $table->timestamps();

            $table->foreign('count_type_id')->references('id')->on('count_types')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countables');
    }
}
