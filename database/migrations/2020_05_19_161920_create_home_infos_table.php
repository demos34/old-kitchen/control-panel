<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_infos', function (Blueprint $table) {
            $table->id();
            $table->text('description')->nullable();
            $table->string('top_image_url')->nullable();
            $table->string('top_home_text')->nullable();
            $table->string('middle_image_url')->nullable();
            $table->longText('middle_home_text')->nullable();
            $table->string('logo_image_url')->nullable();
            $table->integer('recipes_count')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_infos');
    }
}
