<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientCategoryAntherDishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_category_anther_dishes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('consult_category_id');
            $table->longText('description')->nullable();
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('clients')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('consult_category_id')->references('id')->on('consult_categories')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_category_anther_dishes');
    }
}
