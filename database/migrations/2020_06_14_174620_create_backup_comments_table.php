<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBackupCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backup_comments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('draft_recipe_id');
            $table->string('from');
            $table->longText('comment');
            $table->boolean('approved');
            $table->timestamps();

            $table->foreign('draft_recipe_id')->references('id')->on('draft_recipes')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backup_comments');
    }
}
