<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeysForDraftImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('draft_images', function (Blueprint $table) {
            $table->foreign('draft_id')->references('id')->on('draft_recipes')->onDelete('cascade');
            $table->foreign('img_id')->references('id')->on('images')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('draft_images', function (Blueprint $table) {
            $table->dropForeign('draft_images_draft_id');
            $table->dropForeign('draft_images_img_id');
        });
    }
}
