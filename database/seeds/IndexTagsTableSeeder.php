<?php

use App\Models\IndexTag;
use App\Models\Keyword;
use App\Models\Tag;
use Illuminate\Database\Seeder;

class IndexTagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        IndexTag::truncate();
        Tag::truncate();
        Keyword::truncate();
        DB::table('draft_tag')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $indexTagOne = IndexTag::create(
            [
                'name' => 'Стари рецепти',
            ]
        );

        $indexTagTwo = IndexTag::create(
            [
                'name' => 'ХРАНИ',
            ]
        );

        $indexTagThree = IndexTag::create(
            [
                'name' => 'рецепти от старата кухня',
            ]
        );

        $indexTagOne->homeInfos()->attach(1);
        $indexTagTwo->homeInfos()->attach(1);
        $indexTagThree->homeInfos()->attach(1);

        Tag::create(
            [
                'name' => 'хляб',
            ]
        );

        Tag::create(
            [
                'name' => 'тестo',
            ]
        );

        Tag::create(
            [
                'name' => 'тестени издения',
            ]
        );

        Tag::create(
            [
                'name' => 'предястия',
            ]
        );

        Tag::create(
            [
                'name' => 'супи',
            ]
        );

        Tag::create(
            [
                'name' => 'основни',
            ]
        );

        Tag::create(
            [
                'name' => 'ястия с месо',
            ]
        );

        Tag::create(
            [
                'name' => 'месо',
            ]
        );

        Tag::create(
            [
                'name' => 'постни ястия',
            ]
        );

        Tag::create(
            [
                'name' => 'десерти',
            ]
        );

        Keyword::create(
            [
                'name' => 'хляб',
                'eng_name' => 'hlyab',
            ]
        );

        Keyword::create(
            [
                'name' => 'тесто',
                'eng_name' => 'testо',
            ]
        );

        Keyword::create(
            [
                'name' => 'тестени издения',
                'eng_name' => 'testeni izdeliya',
            ]
        );

        Keyword::create(
            [
                'name' => 'предястия',
                'eng_name' => 'predyastiya',
            ]
        );

        Keyword::create(
            [
                'name' => 'супи',
                'eng_name' => 'supi',
            ]
        );

        Keyword::create(
            [
                'name' => 'основни',
                'eng_name' => 'osnovni',
            ]
        );

        Keyword::create(
            [
                'name' => 'ястия с месо',
                'eng_name' => 'yastiya s meso',
            ]
        );

        Keyword::create(
            [
                'name' => 'месо',
                'eng_name' => 'meso',
            ]
        );

        Keyword::create(
            [
                'name' => 'постни ястия',
                'eng_name' => 'postni yasiya',
            ]
        );

        Keyword::create(
            [
                'name' => 'десерт',
                'eng_name' => 'desert',
            ]
        );
    }
}
