<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Role::truncate();
        DB::table('role_user')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Role::create(
            [
                'role' => 'Administrator'
            ]
        );
        Role::create(
            [
                'role' => 'Developer'
            ]
        );
        Role::create(
            [
                'role' => 'Generic User'
            ]
        );
    }
}
