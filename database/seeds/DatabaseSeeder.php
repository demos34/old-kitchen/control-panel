<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(TrafficTableTruncateSeeder::class);
        $this->call(HomeInfoSeeder::class);
        $this->call(IndexTagsTableSeeder::class);
        $this->call(CountTypeSeeder::class);
    }
}
