<?php

use App\Models\Count\CountType;
use Illuminate\Database\Seeder;

class CountTypeSeeder extends Seeder
{

    protected $types = [
        'summary',
        'category',
        'recipe',
        'blog',
        'tube',
        'custom',
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        CountType::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        foreach ($this->types as $type)
        {
            CountType::create(
                [
                    'name' => $type,
                ]
            );
        }

    }
}
