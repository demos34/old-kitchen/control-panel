<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Models\Role;
use App\Models\Profile;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('role_user')->truncate();
        DB::table('profiles')->truncate();
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        $administratorRole = Role::where('role', 'Administrator')->first();
        $developerRole = Role::where('role', 'Developer')->first();
        $genericUserRole = Role::where('role', 'Generic User')->first();
        $uiUserRole = Role::where('role', 'UITester')->first();

        $genericUser = User::create(
            [
                'username' => 'Generic User',
                'email' => 'generic_user@admin.com',
                'password' => Hash::make('123123123')

            ]
        );

        $testUser = User::create(
            [
                'username' => 'Test User',
                'email' => 'test_user@admin.com',
                'password' => Hash::make('123123123')

            ]
        );

        $administrator = User::create(
            [
                'username' => 'Administrator',
                'email' => 'administrator@admin.com',
                'password' => Hash::make('123123123')

            ]
        );

        $developer = User::create(
            [
                'username' => 'Developer',
                'email' => 'ins0mniac1van@gmail.com',
                'password' => Hash::make('123123123')

            ]
        );

        $genericUser->roles()->attach($genericUserRole);
        $testUser->roles()->attach($uiUserRole);
        $administrator->roles()->attach($administratorRole);
        $developer->roles()->attach($developerRole);

        Profile::create(
            [
                'user_id' => $genericUser->id
            ]
        );

        Profile::create(
            [
                'user_id' => $testUser->id
            ]
        );

        Profile::create(
            [
                'user_id' => $administrator->id,
                'first_name' => 'Иван',
                'second_name' => 'Руменов',
                'third_name' => 'Русев',
            ]
        );

        Profile::create(
            [
                'user_id' => $developer->id,
                'first_name' => 'Иван',
                'second_name' => 'Руменов',
                'third_name' => 'Русев',
            ]
        );
    }
}
