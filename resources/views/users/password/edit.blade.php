@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" align="center">
                    <div>
                        <h4>
                            {{ $user->username }}'s Profile edit
                        </h4>
                    </div>
                    <hr />
                </div>
                <form action="{{ route('users.password.update', $user->id) }}" enctype="multipart/form-data" method="POST">
                    @csrf
                    {{ method_field('PATCH') }}
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <div>
                            @if($profile->images()->count() > 0)
                                <img src="{{ implode(', ', $profile->images()->get()->pluck('url')->toArray())}}"
                                     class="rounded-circle mx-auto m-5" style="height: 150px; width: 150px">
                            @else
                                <img src=

                                     "/storage/avatar/no-avatar.jpg"
                                     class="rounded-circle mx-auto m-5" style="max-height: 150px">
                            @endif
                        </div>
                        <div style="border-left: solid 5px white">

                        </div>
                        <div class="my-auto mx-auto">
                            <div class="card-columns">
                                <label for="old-password" class="text-md-right">Old password</label>
                                <h5>
                                    <input id="old-password"
                                           type="password"
                                           class="form-control @error('old-password') is-invalid @enderror"
                                           name="old-password"
                                           value=""
                                           required autocomplete="old-password" autofocus>
                                    @error('old-password')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                            <hr>
                            <div class="card-columns">
                                <div>
                                    <label for="password" class="">New password</label>

                                    <div class="">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="card-columns">
                                <div>
                                    <label for="password-confirm" class="">Confirm new password</label>
                                    <div>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="card-columns">

                            </div>
                        </div>
                    </div>
                </div>
                    <div align="center">
                        <button type="submit" class="btn btn-outline-danger" style="width: 150px;">
                            Change my password
                        </button>
                    </div>
                    <br>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
