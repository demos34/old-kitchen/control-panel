@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" align="center">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h6>
                                <a href="{{ route('users.users.index') }}">
                                    <--
                                </a>
                            </h6>
                        </div>
                        <div>
                            Edit user {{ $user->username }}
                        </div>
                        <div>
                            *
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-header" align="center">
                {{$user->username}}'s information:
            </div>
            <form action="{{ route('users.users.update', $user->id) }}" method="POST">
                @csrf
                {{ method_field('PATCH') }}
            <div class="card-body" align="center">
                <div align="center">
                    <label for="username">Username:</label>
                    <br>
                    <input class="w-75" id="username" name="username" placeholder="{{ $user->username }}" value="{{$user->username}}">
                </div>
                <div align="center">
                    <label for="email">Email:</label>
                    <br>
                    <input class="w-75" id="email" name="email" placeholder="{{ $user->email }}" value="{{$user->email}}">
                </div>
            </div>

            <div class="card-header" align="center">
                Roles
            </div>
                <div class="card-body">
                        @foreach($roles as $role)
                            <div class="form-check" align="center">
                                <input id="role" type="radio" name="role" value="{{ $role->id }}"
                                @if($user->roles->pluck('id')->contains($role->id))
                                    checked
                                @endif
                                >
                                <label for="role">
                                    {{ $role->role }}
                                </label>
                            </div>
                        @endforeach
                        <div align="center">
                            <button type="submit" class="btn btn-success">
                                Edit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
