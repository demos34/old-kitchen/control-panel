@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" align="center">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h6>
                                <a href="{{ route('administrator.administrator.index') }}">
                                    <--
                                </a>
                            </h6>
                        </div>
                        <div>
                            Users
                        </div>
                        <div>
                            *
                        </div>
                    </div>
                    <hr />
                    <div class="d-flex justify-content-center">
                        <a href="{{ route('users.users.create') }}">
                            <button type="button" class="btn btn-primary">
                                Register new user
                            </button>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Username</th>
                            <th scope="col">Email</th>
                            <th scope="col">Role</th>
                            <th scope="col">Actions</th>
                            <th scope="col">Profile</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                <th scope="row">{{ $user->id }}</th>
                                <td>
                                    <a href="">
                                        {{ $user->username }}
                                    </a>
                                </td>
                                <td>
                                    {{ $user->email }}
                                </td>
                                 <td>
                                    {{ implode(', ', $user->roles()->get()->pluck('role')->toArray())}}
                                </td>
                                 <td>
                                     <a href="{{ route('users.users.edit',$user->id) }}" class="float-left">
                                         <button type="button" class="btn btn-dark">Edit</button>
                                     </a>
                                     <form action="{{ route('users.users.destroy',$user->id) }}" method="POST" class="float-left">
                                         @csrf
                                         {{ method_field('DELETE') }}
                                         <button onclick="alert('Are you sure?')" type="submit" class="btn btn-warning">Delete</button>
                                     </form>
                                </td>
                                <td>
                                    <a href="{{route('users.profile.show',$user->id)}}">Profile</a>
                                </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
