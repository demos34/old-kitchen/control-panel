@extends('layouts.app')

@section('content')


<div class="container mb-5">
    <div class="row justify-content-center">
        <div class="col-md-12 my-5">
            <div class="card">
                <div class="card-header" align="center">
                    <div>
                        <h4>
                            {{ $user->username }}'s Profile edit
                        </h4>
                    </div>
                    <hr />
                </div>
                <form action="{{ route('users.profile.update', $profile->id) }}" enctype="multipart/form-data" method="POST">
                    @csrf
                    {{ method_field('PATCH') }}
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <div>
                            @if($profile->images()->count() > 0)
                                <img src="{{ implode(', ', $profile->images()->get()->pluck('url')->toArray())}}"
                                     class="rounded-circle mx-auto m-5" style="height: 150px; width:50%">
                            @else
                                <img src=

                                     "/storage/avatar/no-avatar.jpg"
                                     class="rounded-circle mx-auto m-5" style="max-height: 150px">
                            @endif
                            <br>
                            <label for="image" class="col-md-4 col-form-label text-md-right">Image</label>
                            <div>
                                <input type="file" class="" id="image" name="image">
                                @if($errors->has('image'))
                                    <strong>{{ $errors->first('image') }}</strong>
                                @endif
                            </div>
                        </div>
                        <div class="my-auto mx-auto">
                            <div class="card-columns">
                                <label for="username" class="text-md-right">Username</label>
                                <h5>
                                    <input id="username"
                                           type="text"
                                           class="form-control @error('username') is-invalid @enderror"
                                           name="username"
                                           value="@if(old('username')){{ old('username') }}@else{{ $user->username }}@endif"
                                           required autocomplete="username" autofocus>
                                    @error('username')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                            <hr>
                            <div class="card-columns">
                                <label for="first_name" class="text-md-right">First Name</label>
                                <h5>
                                    <input id="first_name"
                                           type="text"
                                           class="form-control @error('first_name') is-invalid @enderror"
                                           name="first_name"
                                           value=
                                            @if(old('first_name'))
                                                   "{{ old('first_name') }}"
                                            @else
                                               @if($profile->first_name === NULL)
                                               "NOT SET YET"
                                               @else
                                               "{{ $profile->first_name }}"
                                               @endif
                                            @endif
                                    required autocomplete="first_name" autofocus>
                                    @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                            <hr>
                            <div class="card-columns">
                                <label for="second_name" class="text-md-right">Middle Name</label>
                                <h5>
                                    <input id="second_name"
                                           type="text"
                                           class="form-control @error('second_name') is-invalid @enderror"
                                           name="second_name"
                                           value=
                                        @if(old('second_name'))
                                                   "{{ old('second_name') }}"
                                        @else
                                            @if($profile->second_name === NULL)
                                                "NOT SET YET"
                                            @else
                                                "{{ $profile->second_name }}"
                                            @endif
                                        @endif
                                    required autocomplete="second_name" autofocus>
                                    @error('second_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                            <hr>
                            <div class="card-columns">
                                <label for="third_name" class="text-md-right">Last Name</label>
                                <h5>
                                    <input id="third_name"
                                           type="text"
                                           class="form-control @error('third_name') is-invalid @enderror"
                                           name="third_name"
                                           value=
                                            @if(old('third_name'))
                                                   "{{ old('third_name') }}"
                                            @else
                                                @if($profile->third_name === NULL)
                                                    "NOT SET YET"
                                                @else
                                                    "{{ $profile->third_name }}"
                                                @endif
                                            @endif
                                    required autocomplete="third_name" autofocus>
                                    @error('third_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                            <hr>
                            <div class="card-columns">
                                <label for="email" class="text-md-right">Email</label>
                                <h5>
                                    <input id="email"
                                           type="text"
                                           class="form-control @error('email') is-invalid @enderror"
                                           name="email"
                                           value=
                                            @if(old('email'))
                                                   "{{ old('email') }}"
                                            @else
                                                "{{ Auth::user()->email }}"
                                            @endif
                                    required autocomplete="third_name" autofocus>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </h5>
                            </div>
                            <hr>
                            <div class="card-columns">
                                    <button type="submit" class="btn btn-primary">
                                        Edit
                                    </button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
