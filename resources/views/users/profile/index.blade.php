@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" align="center">
                    <div>
                        <h4>
                            {{ $user->username }}'s Profile
                        </h4>
                    </div>
                    <hr />
                </div>
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        @if($profile->images()->count() > 0)
                            <img src="{{ implode(', ', $profile->images()->get()->pluck('url')->toArray())}}"
                                 class="rounded-circle mx-auto m-5" style="height: 150px; width:50%">
                        @else
                            <img src=

                                 "/storage/avatar/no-avatar.jpg"
                                 class="rounded-circle mx-auto m-5" style="max-height: 150px">
                        @endif
                        <div class="my-auto mx-auto">
                            <div class="card-columns">
                                <label>Username:</label>
                                <h5>
                                    {{ $user->username }}
                                </h5>
                            </div>
                            <hr>
                            <div class="card-columns">
                                <label>First name:</label>
                                <h5>
                                    @if($profile->first_name === NULL)
                                        NOT SET YET
                                    @else
                                        {{ $profile->first_name }}
                                    @endif
                                </h5>
                            </div>
                            <hr>
                            <div class="card-columns">
                                <label>Middle name:</label>
                                <h5>
                                @if($profile->second_name === NULL)
                                    NOT SET YET
                                @else
                                    {{ $profile->second_name }}
                                @endif
                                </h5>
                            </div>
                            <hr>
                            <div class="card-columns">
                                <label>Last name:</label>
                                <h5>
                                @if($profile->third_name === NULL)
                                    NOT SET YET
                                @else
                                    {{ $profile->third_name }}
                                @endif
                                </h5>
                            </div>
                            <hr>
                            <div class="card-columns">
                                <a href="{{ route('users.profile.edit', $profile->id) }}" class="my-auto">
                                    <button type="button" class="btn btn-primary">
                                        Edit my information
                                    </button>
                                </a>
                                <a href="{{ route('users.password.edit', $user->id) }}">
                                    <button type="button" class="btn btn-primary">
                                        Change my password
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
