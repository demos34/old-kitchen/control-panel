<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @yield("meta")

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/home.css') }}" rel="stylesheet">
    <link href="{{ asset('css/consult.css') }}" rel="stylesheet">
    <link href="{{ asset('css/analyst.css') }}" rel="stylesheet">
    @yield("style")
</head>
<body>
@include('partials.navbar')
    <div class="container" id="app">
        @yield('consult')
        <main class="py-4 mb-40">
            @include('partials.alerts')
            @yield('content')
        </main>
    </div>
    @include('partials.footer')
    @yield("scripts")
</body>
</html>
