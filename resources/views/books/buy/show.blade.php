@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="text-center font-weight-bold font-italic my-5">
                <h3>
                    {{$publisher->from_where}}
                </h3>
        </div>
        <div class="row mt-5">
            <div class="col-md-4" style="border-right: 1px solid black">
                <img src="{{$publisher->image}}" class="w-100 align-content-center">
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-6 offset-2 my-2">
                        Сайт: {{$publisher->url}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
