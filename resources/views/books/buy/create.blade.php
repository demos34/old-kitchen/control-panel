@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card my-3 mx-auto">
                                <div class="card-header">
                                    <h4 class="text-center">
                                        Издатели
                                    </h4>
                                </div>
                                <div class="card-body">
                                    <div class="well">
                                        <table class="table table-borderless">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Издател</th>
                                                <th>Сайт</th>
                                                <th>Действия</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($publishers as $publisher)
                                                <tr>
                                                    <td>{{$publisher->id}}</td>
                                                    <td>{{$publisher->from_where}}</td>
                                                    <td>{{$publisher->url}}</td>
                                                    <td>
                                                        <a href="{{ route('books.buy.edit', $publisher->id) }}"
                                                           class="float-left mx-1">
                                                            <button type="button" class="btn btn-outline-dark">
                                                                Редактирай
                                                            </button>
                                                        </a>
                                                        <a href="{{ route('books.buy.show', $publisher->id) }}"
                                                           class="float-left mx-1">
                                                            <button type="button" class="btn btn-outline-success">
                                                                Виж
                                                            </button>
                                                        </a>
                                                        <form action="{{ route('books.buy.destroy', $publisher->id) }}"
                                                              method="POST" class="float-left mx-1">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="btn btn-outline-danger"
                                                                    onclick="alert('Сигурен ли сте, че искате да изтриете тази книга?')">
                                                                Изтрий
                                                            </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 my-3">
                            <div class="well">
                                <h4 class="text-center">
                                    <label>
                                        Добави издател
                                    </label>
                                </h4>
                                <form action="{{ route('books.buy.store') }}" method="POST"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="card">
                                        <div class="card-header">
                                            <label>Добави издател:</label>
                                        </div>
                                        <div class="card-body">
                                            <label for="from_where">
                                                Заглавие:
                                            </label>
                                            <input id="from_where"
                                                   type="text"
                                                   class="form-control @error('from_where') is-invalid @enderror"
                                                   name="from_where"
                                                   value="{{ old('from_where') }}"
                                                   required autocomplete="from_where" autofocus>
                                            @error('from_where')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                        <div class="card-body">
                                            <label for="slug">
                                                Заглавие на латиница:
                                            </label>
                                            <input id="slug"
                                                   type="text"
                                                   class="form-control @error('slug') is-invalid @enderror"
                                                   name="slug"
                                                   value="{{ old('slug') }}"
                                                   required autocomplete="slug" autofocus>
                                            @error('slug')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                        <div class="card-body">
                                            <label for="url">
                                                URL:
                                            </label>
                                            <input id="url"
                                                   type="text"
                                                   class="form-control @error('url') is-invalid @enderror"
                                                   name="url"
                                                   value="{{ old('url') }}"
                                                   required autocomplete="url" autofocus>
                                            @error('url')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>

                                        <div class="card-body">
                                            <label for="image">Снимка</label>
                                            <div class="col-md-6">
                                                <input type="file" id="image" name="image">
                                                @if($errors->has('image'))
                                                    <strong>{{ $errors->first('image') }}</strong>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center m-2">
                                        <button type="submit" class="btn btn-dark">
                                            Добави
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
