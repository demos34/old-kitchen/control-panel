@extends('layouts.app')

@section('style')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        <form action="{{ route('books.books.update', $book->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="text-center font-weight-bold font-italic my-5">
                <h3>
                    <div>
                        <label for="title">
                            Заглавие:
                        </label>
                        <input id="title"
                               type="text"
                               class="form-control @error('title') is-invalid @enderror text-center"
                               name="title"
                               value=@if(old('title'))"{{ old('title') }}"@else"{{$book->title}}"@endif
                        required autocomplete="title" autofocus>
                        @error('title')
                        <br>
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                    </div>
                    <div>
                        <label for="slug">
                            Заглавие на латиница:
                        </label>
                        <input id="slug"
                               type="text"
                               class="form-control @error('slug') is-invalid @enderror text-center"
                               name="slug"
                               value=@if(old('slug'))"{{ old('slug') }}"@else"{{$book->slug}}"@endif
                        required autocomplete="slug" autofocus>
                        @error('slug')
                        <br>
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </h3>
            </div>
            <div class="row mt-5">
                <div class="col-md-4" style="border-right: 1px solid black">
                    <img src="{{$book->image}}" class="w-100 align-content-center">
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-lg-6 offset-2 my-2">
                            <div>
                                <label for="description">
                                    Кратко описание:
                                </label>
                                <textarea id="description"
                                          type="text"
                                          class="form-control @error('description') is-invalid @enderror"
                                          name="description"
                                          required autocomplete="description" autofocus>@if(old('description')){{ old('description') }}@else{{$book->description}}@endif</textarea>
                                @error('description')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 offset-2 my-2">
                            <label for="price">
                                Може да я получите на цена:
                            </label>
                            <input id="price"
                                   type="text"
                                   class="form-control @error('price') is-invalid @enderror text-center"
                                   name="price"
                                   value=@if(old('price'))"{{ old('price') }}"@else"{{$book->price}}"@endif
                            required autocomplete="price" autofocus>
                            @error('price')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-lg-6 offset-2 my-2">
                            Може да я закупите от:
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th>Издателство:</th>
                                    <th>Цена:</th>
                                    <th>Закупи:</th>
                                </tr>
                                </thead>
                            </table>
                            <div class="form-group row text-center">
                                <label for="publishers" class="col-md-12">
                                    Издателства:
                                </label>
                                <select id="publishers" class="col-md-12 js-multiple-publishers" name="publishers[]"
                                        multiple="multiple">
                                    @foreach($publishers as $publisher)
                                        <option value="{{$publisher->id}}">{{$publisher->from_where}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="card-body">
                                <label for="image">Снимка</label>
                                <div class="col-md-6">
                                    <input type="file" id="image" name="image">
                                    @if($errors->has('image'))
                                        <strong>{{ $errors->first('image') }}</strong>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row text-center">
                                <button type="submit" class="btn btn-outline-success">Редактирай</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.js-multiple-publishers').select2();
        });
        $('.js-multiple-publishers').select2().val({!! json_encode($book->buyFrom()->get()->pluck('id')->toArray()) !!}).trigger('change');
    </script>
@endsection
