@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card my-3 mx-auto">
                                <div class="card-header">
                                    <h4 class="text-center">
                                        Книги
                                    </h4>
                                </div>
                                <div class="card-body">
                                    <div class="well">
                                        <table class="table table-borderless">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Заглавие</th>
                                                <th>Цена в лв.</th>
                                                <th>Действия</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($books as $book)
                                                <tr>
                                                    <td>{{$book->id}}</td>
                                                    <td>{{$book->title}}</td>
                                                    <td>{{$book->price}} лв.</td>
                                                    <td>
                                                        <a href="{{ route('books.books.edit', $book->id) }}"
                                                           class="float-left mx-1">
                                                            <button type="button" class="btn btn-outline-dark">
                                                                Редактирай
                                                            </button>
                                                        </a>
                                                        <a href="{{ route('books.books.show', $book->slug) }}"
                                                           class="float-left mx-1">
                                                            <button type="button" class="btn btn-outline-success">
                                                                Виж
                                                            </button>
                                                        </a>
                                                        <form action="{{ route('books.books.destroy', $book->id) }}"
                                                              method="POST" class="float-left mx-1">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="btn btn-outline-danger"
                                                                    onclick="alert('Сигурен ли сте, че искате да изтриете тази книга?')">
                                                                Изтрий
                                                            </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 my-3">
                            <div class="well">
                                <h4 class="text-center">
                                    <label>
                                        Добави нова книга
                                    </label>
                                </h4>
                                <form action="{{ route('books.books.store') }}" method="POST"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="card">
                                        <div class="card-header">
                                            <label>Добави нова книга:</label>
                                        </div>
                                        <div class="card-body">
                                            <label for="title">
                                                Заглавие:
                                            </label>
                                            <input id="title"
                                                   type="text"
                                                   class="form-control @error('title') is-invalid @enderror"
                                                   name="title"
                                                   value="{{ old('title') }}"
                                                   required autocomplete="title" autofocus>
                                            @error('title')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                        <div class="card-body">
                                            <label for="slug">
                                                Заглавие на латиница:
                                            </label>
                                            <input id="slug"
                                                   type="text"
                                                   class="form-control @error('slug') is-invalid @enderror"
                                                   name="slug"
                                                   value="{{ old('slug') }}"
                                                   required autocomplete="slug" autofocus>
                                            @error('slug')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                        <div class="card-body">
                                            <label for="description">
                                                Кратко описание:
                                            </label>
                                            <textarea id="description"
                                                      type="text"
                                                      class="form-control @error('description') is-invalid @enderror"
                                                      name="description"
                                                      required autocomplete="description"
                                                      autofocus>{{ old('description') }}</textarea>
                                            @error('description')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                        <div class="card-body">
                                            <label for="price">
                                                Цена:
                                            </label>
                                            <input id="price"
                                                   type="text"
                                                   class="form-control @error('price') is-invalid @enderror"
                                                   name="price"
                                                   value="{{ old('price') }}"
                                                   required autocomplete="price" autofocus>
                                            @error('price')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                        <div class="card-body">
                                            <label for="image">Снимка</label>
                                            <div class="col-md-6">
                                                <input type="file" id="image" name="image">
                                                @if($errors->has('image'))
                                                    <strong>{{ $errors->first('image') }}</strong>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center m-2">
                                        <button type="submit" class="btn btn-dark">
                                            Добави
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
