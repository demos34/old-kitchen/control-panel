@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="text-center font-weight-bold font-italic my-5">
                <h3>
                    {{$book->title}}
                </h3>
        </div>
        <div class="row mt-5">
            <div class="col-md-4" style="border-right: 1px solid black">
                <img src="{{$book->image}}" class="w-100 align-content-center">
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-lg-6 offset-2 my-2" style="border-bottom: 1px solid gray">
                        {{$book->description}}
                    </div>
                    <div class="col-lg-6 offset-2 my-2">
                        Може да я получите на цена: {{$book->price}} лв.
                    </div>
                    <div class="col-lg-6 offset-2 my-2">
                        Може да я закупите от:
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th>Издателство:</th>
                                    <th>Закупи:</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                            </tr>
                            @foreach($book->buyFrom as $publisher)
                                <tr>
                                    <td>
                                        <a href="{{$publisher->url}}">{{$publisher->from_where}}</a>
                                    </td>
                                    <td>
                                        <a href="">
                                            <button type="button" class="btn btn-secondary">Купи</button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="align-items-center text-center">
            <label>Управление:</label>
            <div>
                <a href="{{ route('books.books.edit', $book->id) }}">
                    <button type="button" class="btn btn-outline-dark">Edit</button>
                </a>
            </div>
        </div>
    </div>
@endsection
