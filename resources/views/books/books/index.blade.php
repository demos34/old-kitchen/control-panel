@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row my-3 justify-content-center">
            <label class="text-center">
                Blog:
            </label>
        </div>
    </div>
    <div class="container">
        <div class="row mx-auto my-2 justify-content-center">
            @foreach($books as $book)
                <div class="mx-5">
                    <div class="text-center">
                        <label class="font-weight-bold" style="font-size: 18px">
                            {{ $book->title }}
                        </label>
                        <hr>
                    </div>
                    <div class="row m-5">
                        <div class="col-sm-5">
                            <img src="{{ $book->image }}" class="w-75">
                        </div>
                        <div class="col-sm-7">
                            <p>
                                {{$book->description}}
                            </p>
                            <p>
                                Цена: {{$book->price}}
                            </p>
                            <ul>
                                Издателство:
                                @foreach($book->buyFrom as $publisher)
                                    <li>
                                        <a href="{{$publisher->url}}">
                                            <span class="font-weight-bold">
                                                {{$publisher->from_where}}
                                            </span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>

                            <div class="text-center">
                                <a href="{{route('books.books.show', $book->slug)}}">
                                    <button type="button" class="btn btn-dark">Покажи</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
