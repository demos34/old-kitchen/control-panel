@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card my-3 mx-auto">
                                <div class="card-header">
                                    <h4 align="center">
                                        Мета-таг <strong>{{$tag->name}}</strong>:
                                    </h4>
                                </div>
                                <div class="card-body">
                                    <div class="card-group">
                                        <div class="mx-5">#</div>
                                        <div class="mx-5">Name</div>
                                        <div class="mx-5">Delete</div>
                                    </div>
                                    <hr>
                                    <div class="card-group">
                                        <div class="mx-5">{{$tag->id}}</div>
                                        <div class="mx-5"><a href="{{ route('tags.index.show', $tag->id) }}">{{$tag->name}}</a></div>
                                        <div class="mx-5">
                                            <form action="{{ route('tags.index.destroy', $tag->id) }}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-danger" onclick="return confirm('Сигурен ли сте?')">
                                                    &times;
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 my-3">
                            <div class="well">
                                <h4 align="center">
                                    <label>
                                        Редактирай мета-тага
                                    </label>
                                </h4>
                                <form action="{{ route('tags.index.update', $tag->id) }}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="name">
                                                Name:
                                            </label>
                                        </div>
                                        <div class="card-body">
                                            <h5>
                                                <input id="name"
                                                       type="text"
                                                       class="form-control @error('name') is-invalid @enderror"
                                                       name="name"
                                                       value=@if(old('name'))"{{ old('name') }}"@else"{{$tag->name}}"@endif
                                                       required autocomplete="name" autofocus>
                                                @error('name')
                                                <br>
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center m-2">
                                        <button type="submit" class="btn btn-dark">
                                            Change
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
