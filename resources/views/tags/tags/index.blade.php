@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="alert alert-warning" role="alert">
            Таговете са важни, за да може да се индексира правилно всеки един сайт в търсачките (гугъл, бинг и т.н.) В контролния панел има възможност за добавяне на тагове както към заглавната страница, така и за всяка една рецепта. Таговете за блога за изнесени в секция "Блог". При добавяне на нова рецепта в драфт, задължително трябва да се изберат всички тагове, които отговарят на рецептата. Ако няма такива, трябва да се създадат от полето в дясно. Същото важни и за заглавната страница. Там, допълнителното е, че има и описание, което също трябва да се добави.
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card my-3 mx-auto">
                                <div class="card-header">
                                    <h4>
                                        Описание на заглавната страница:
                                    </h4>
                                </div>
                                <div class="card-body">
                                    {{ $homeInfo->description }}
                                </div>
                            </div>
                            <div class="card my-3 mx-auto">
                                <div class="card-header">
                                    <h4 align="center">
                                        Мета-тагове за заглавната (index page) страница:
                                    </h4>
                                </div>
                                <div class="card-body">
                                    <div class="card-group">
                                        <div class="mx-5">#</div>
                                        <div class="mx-5">Name</div>
                                    </div>
                                    <hr>
                                    @if($indexTags->count()>0)
                                        @foreach($indexTags as $tag)
                                            <div class="card-group">
                                                <div class="mx-5">{{$tag->id}}</div>
                                                <div class="mx-5"><a href="{{ route('tags.index.show', $tag->id) }}">{{$tag->name}}</a></div>
                                            </div>
                                            <hr>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 my-3">
                            <div class="well">
                                <h4 align="center">
                                    <label>
                                        Добави нов мета-таг
                                    </label>
                                </h4>
                                <form action="{{ route('tags.index.store') }}" method="POST">
                                    @csrf

                                    <div class="card">
                                        <div class="card-header">
                                            <label for="name">
                                                Name:
                                            </label>
                                        </div>
                                        <div class="card-body">
                                            <h5>
                                                <input id="name"
                                                       type="text"
                                                       class="form-control @error('name') is-invalid @enderror"
                                                       name="name"
                                                       value="{{ old('name') }}"
                                                       required autocomplete="name" autofocus>
                                                @error('name')
                                                <br>
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center m-2">
                                        <button type="submit" class="btn btn-dark">
                                            Add
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card my-3 mx-auto">
                                <div class="card-header">
                                    <h4 align="center">
                                        Мета-тагове за рецепти:
                                    </h4>
                                </div>
                                <div class="card-body">
                                    <div class="card-group">
                                        <div class="mx-5">#</div>
                                        <div class="mx-5">Name</div>
                                    </div>
                                    <hr>
                                    @if($tags->count()>0)
                                        @foreach($tags as $tag)
                                            <div class="card-group">
                                                <div class="mx-5">{{$tag->id}}</div>
                                                <div class="mx-5"><a href="{{ route('tags.tags.show', $tag->id) }}">{{$tag->name}}</a></div>
                                            </div>
                                            <hr>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 my-3">
                            <div class="well">
                                <h4 align="center">
                                    <label>
                                        Добави нов мета-таг
                                    </label>
                                </h4>
                                <form action="{{ route('tags.tags.store') }}" method="POST">
                                    @csrf

                                    <div class="card">
                                        <div class="card-header">
                                            <label for="name">
                                                Name:
                                            </label>
                                        </div>
                                        <div class="card-body">
                                            <h5>
                                                <input id="name"
                                                       type="text"
                                                       class="form-control @error('name') is-invalid @enderror"
                                                       name="name"
                                                       value="{{ old('name') }}"
                                                       required autocomplete="name" autofocus>
                                                @error('name')
                                                <br>
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center m-2">
                                        <button type="submit" class="btn btn-dark">
                                            Add
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
