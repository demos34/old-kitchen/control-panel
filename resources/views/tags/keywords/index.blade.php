@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="alert alert-warning" role="alert">
            Ключовите думи, подобно на таговете, са важни, за да може да се индексира правилно всеки един сайт в търсачките (гугъл, бинг и т.н.) В контролния панел има възможност за добавяне на ключови думи. При добавяне на нова рецепта в драфт, задължително трябва да се изберат всички ключови, които отговарят на рецептата. Ако няма такива, трябва да се създадат от полето долу. Добавя се както изписана на български, така и на английски.
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card my-3 mx-auto">
                                <div class="card-header">
                                    <h4 align="center">
                                        Списък на съществуващите ключови думи:
                                    </h4>
                                </div>
                                <div class="card-body">
                                    <td class="card-group">
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Дума</th>
                                                <th>Дума на латиница</th>
                                                <th>Действия</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @if($keywords->count()>0)
                                                    @foreach($keywords as $keyword)
                                                        <tr>
                                                        <td>{{$keyword->id}}</td>
                                                        <td><a href="{{ route('tags.keywords.show', $keyword->id) }}">{{$keyword->name}}</a></td>
                                                        <td><a href="{{ route('tags.keywords.show', $keyword->id) }}">{{$keyword->eng_name}}</a></td>
                                                            <td>
                                                                <form action="{{route('tags.keywords.destroy', $keyword->id)}}" method="POST">
                                                                    @csrf
                                                                    {{ method_field('DELETE') }}
                                                                    <button type="submit" class="btn btn-outline-dark">
                                                                        Изтрий
                                                                    </button>
                                                                </form>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 my-3">
                            <div class="well">
                                <h4 align="center">
                                    <label>
                                        Добави нова ключова дума
                                    </label>
                                </h4>
                                <form action="{{ route('tags.keywords.store') }}" method="POST">
                                    @csrf

                                    <div class="card">
                                        <div class="card-header">
                                            <label for="name">
                                                Наименование:
                                            </label>
                                        </div>
                                        <div class="card-body">
                                            <h5>
                                                <input id="name"
                                                       type="text"
                                                       class="form-control @error('name') is-invalid @enderror"
                                                       name="name"
                                                       value="{{ old('name') }}"
                                                       required autocomplete="name" autofocus>
                                                @error('name')
                                                <br>
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="eng_name">
                                                Наименование на латиница:
                                            </label>
                                        </div>
                                        <div class="card-body">
                                            <h5>
                                                <input id="eng_name"
                                                       type="text"
                                                       class="form-control @error('eng_name') is-invalid @enderror"
                                                       name="eng_name"
                                                       value="{{ old('eng_name') }}"
                                                       required autocomplete="eng_name" autofocus>
                                                @error('eng_name')
                                                <br>
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center m-2">
                                        <button type="submit" class="btn btn-dark">
                                            Добави
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
