@extends('layouts.app')
@include('cookieConsent::index')
@section('content')
    <div class="container">
        <form action="{{route('comments.comments.update', $comment->id)}}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row my-1">
                <div class="col-md-12 mx-auto">
                    <div class="card">
                        <div class="card-header">
                            Напиши коментар:
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="from" class="col-md-4 col-form-label text-md-right">Име:</label>
                                <div class="col-md-6">
                                    <input id="from"
                                           type="text"
                                           class="form-control @error('from') is-invalid @enderror"
                                           name="from"
                                           value=@if(old('from')) "{{ old('from') }}" @else "{{$comment->from}}" @endif
                                    required autocomplete="from" autofocus>

                                    @error('from')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-md-12 text-center">
                                    Одобрен ли е?
                                </div>
                                <div class="col-md-12 text-center">
                                    <div class="row">
                                        @if($comment->approved == 1)
                                            <div class="col-md-12 text-center">
                                                <h4>Да</h4>
                                            </div>
                                        @else
                                            <div class="col-md-12 text-center">
                                                <h4>Не</h4>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <label for="comment" class="col-md-4 col-form-label text-md-right">Коментар:</label>
                                <div class="col-md-6">
                                <textarea style="min-height: 100px"
                                          id="comment"
                                          type="text"
                                          class="form-control @error('comment') is-invalid @enderror"
                                          name="comment"
                                          required autocomplete="comment"
                                          autofocus>@if(old('comment')){{old('comment')}}@else{{$comment->comment}}@endif</textarea>

                                    @error('comment')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div align="center">
                                <button type="submit" class="btn btn-dark">
                                    Промени
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
