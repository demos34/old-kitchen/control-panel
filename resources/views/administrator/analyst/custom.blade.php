@extends('administrator.analyst.analyst-layout')
@section('cont')
    <div class="analyst-body">
        <div class="card my-2">
            <div class="card-header">
                Моят анализатор
            </div>
            <div class="card-body">
                Създай нов:
            </div>
            <div class="card-footer">
                <form action="{{ route('analyst.custom-analyst.store') }}" method="POST">
                    @csrf
                    <label for="name">
                        Име:
                    </label>
                    <input id="name"
                           type="text"
                           class="form-control @error('name') is-invalid @enderror"
                           name="name"
                           value="{{ old('name') }}"
                           required autocomplete="name">
                    @error('name')
                    <br>
                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                    @enderror
                    <br>
                    <label for="start_date">
                        От дата:
                    </label>
                    <input id="start_date"
                           type="datetime-local"
                           class="form-control @error('start_date') is-invalid @enderror"
                           name="start_date"
                           value="{{ old('start_date') }}"
                           required autocomplete="start_date">
                    @error('start_date')
                    <br>
                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                    @enderror
                    <br>
                    <label for="end_date">
                        До дата:
                    </label>
                    <input id="end_date"
                           type="datetime-local"
                           class="form-control @error('end_date') is-invalid @enderror"
                           name="end_date"
                           value="{{ old('end_date') }}"
                           required autocomplete="end_date">
                    @error('end_date')
                    <br>
                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                    @enderror
                    <div class="d-flex justify-content-center m-2">
                        <button type="submit" class="btn btn-dark">
                            Добави
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <div class="card text-center">
            <div class="card-header">
                <strong>
                    Моите анализатори:
                </strong>
            </div>
            <div class="card-body">
                <div class="d-flex justify-content-between w-100 font-weight-bolder text-center">
                    <div class="text-center">
                        Име
                    </div>
                    <div class="text-center">
                        От дата
                    </div>
                    <div class="text-center">
                        До дата
                    </div>
                    <div class="text-center">
                        Действия
                    </div>
                </div>
                @foreach($myAnalysts as $my)
                    <div class="d-flex justify-content-between w-100 my-1 p-1">
                        <div>
                            {{ $my->name }}
                        </div>
                        <div>
                            {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $my->start_date)->format('H:i:s d-m-Y') }}
                        </div>
                        <div>
                            {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $my->end_date)->format('H:i:s d-m-Y') }}
                        </div>
                        <div class="d-flex justify-content-between">
                            <a href="{{ route('analyst.custom-analyst.show', $my->id) }}">
                                <button class="btn btn-outline-success">
                                    Виж
                                </button>
                            </a>
                            <form action="{{ route('analyst.custom-analyst.delete', $my->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" onclick="return confirm('Сигурен ли сте?')">
                                    Изтрий
                                </button>
                            </form>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
