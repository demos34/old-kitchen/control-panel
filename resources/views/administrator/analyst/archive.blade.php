@extends('administrator.analyst.analyst-layout')
@section('cont')
    <div class="analyst-body">
        <form action="{{ route('analyst.archive.delete-chosen') }}" method="post">
            @csrf
            @method('PATCH')
            <div class="card my-2">
                <div class="card-header">
                    <strong>
                        Архив
                    </strong>
                </div>
                <div class="card-body">
                    Тук са всички записи, които са по-стари от 3 месеца. След като изминат 3 месеца от посещението в
                    даден линк, те автоматично се преместват тук и се трият от основната страница.
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-between">
                        <div>
                            <a href="{{ route('analyst.archive.delete') }}">
                                <button type="button" class="btn btn-outline-danger" onclick="return confirm('Сигурен ли сте?')">
                                    Изтрий целия архив
                                </button>
                            </a>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-outline-danger" onclick="return confirm('Сигурен ли сте?')">
                                Изтрий избраните
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card text-center" style="font-size: .85em">
                <div class="card-header w-100">
                    <div class="analyst-information font-weight-bolder">
                        <div>
                            Изтрий
                        </div>
                        <div>
                            Държава
                            <a href="{{ route('analyst.archive.sort', ['country', 'asc']) }}"
                               style="text-transform: none; text-decoration: none">
                                &#8593;
                            </a>
                            <a href="{{ route('analyst.archive.sort', ['country', 'desc']) }}"
                               style="text-transform: none; text-decoration: none">
                                &#8595;
                            </a>
                        </div>
                        <div>
                            Ip адрес
                            <a href="{{ route('analyst.archive.sort', ['ip_address', 'asc']) }}"
                               style="text-transform: none; text-decoration: none">
                                &#8593;
                            </a>
                            <a href="{{ route('analyst.archive.sort', ['ip_address', 'desc']) }}"
                               style="text-transform: none; text-decoration: none">
                                &#8595;
                            </a>
                        </div>
                        <div>
                            Потребител (id)
                            <a href="{{ route('analyst.archive.sort', ['session_id', 'asc']) }}"
                               style="text-transform: none; text-decoration: none">
                                &#8593;
                            </a>
                            <a href="{{ route('analyst.archive.sort', ['session_id', 'desc']) }}"
                               style="text-transform: none; text-decoration: none">
                                &#8595;
                            </a>
                        </div>
                        <div>
                            На дата
                            <a href="{{ route('analyst.archive.sort', ['created_at', 'asc']) }}"
                               style="text-transform: none; text-decoration: none">
                                &#8593;
                            </a>
                            <a href="{{ route('analyst.archive.sort', ['created_at', 'desc']) }}"
                               style="text-transform: none; text-decoration: none">
                                &#8595;
                            </a>
                        </div>
                        <div>
                            Идва от линк
                            <a href="{{ route('analyst.archive.sort', ['referer', 'asc']) }}"
                               style="text-transform: none; text-decoration: none">
                                &#8593;
                            </a>
                            <a href="{{ route('analyst.archive.sort', ['referer', 'desc']) }}"
                               style="text-transform: none; text-decoration: none">
                                &#8595;
                            </a>
                        </div>
                    </div>
                </div>
                @foreach($analyzes as $analyze)
                    <div class="card-body border-bottom">
                        <div class="analyst-information">
                            <div class="d-flex flex-column justify-content-center">
                                <input type="checkbox" name="archives[]" value="{{ $analyze->id }}">
                            </div>
                            <div>
                                <div class="px-1">
                                    {{ $analyze->country }}
                                </div>
                                <br>
                                <br>
                                @if($analyze->country !== NULL)
                                    <a href="{{ route('analyst.look', ['country', $analyze->country]) }}">
                                        Виж всички от тази държава
                                    </a>
                                @endif
                            </div>
                            <div>
                                <div class="px-1">
                                    {{ $analyze->ip_address }}
                                </div>
                                <br>
                                <br>
                                <a href="{{ route('analyst.look', ['ip_address', $analyze->ip_address]) }}">
                                    Виж всичко от този IP адрес
                                </a>
                            </div>
                            <div>
                                <div class="px-1">
                                    {{ $analyze->session_id }}
                                </div>
                                <br>
                                <br>
                                <a href="{{ route('analyst.look', ['session_id', $analyze->session_id]) }}">
                                    Виж всички от този потребител
                                </a>
                            </div>
                            <div>
                                <div class="px-1">
                                    {{ $analyze->created_at }}
                                </div>
                                <br>
                                <br>
                                <a href="{{ route('analyst.look', ['created_at', $analyze->created_at->format('Y-m-d')]) }}">
                                    Виж всички на тази дата
                                </a>
                            </div>
                            <div>
                                <div class="px-1">
                                    {{ $analyze->referer }}
                                </div>
                                <br>
                                <br>
                                @if($analyze->referer)
                                    <a href="{{ route('analyst.look', ['referer', $analyze->site->string_id]) }}">
                                        Виж всички от този линк
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="mx-auto w-100">
                <div class="analyst-footer">
                    {{ $analyzes->onEachSide(0)->links() }}
                </div>
            </div>
        </form>
    </div>
@endsection
