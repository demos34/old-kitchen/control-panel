@extends('administrator.analyst.analyst-layout')
@section('cont')
    <div class="analyst-body">
        <div class="card my-2">
            <div class="card-header">
                @if($key === '1')
                    <strong>За последните 24 часа:</strong>
                @elseif($key === '7')
                    <strong>За последните 7 дни:</strong>
                @elseif($key === '30')
                    <strong>За последния 1 месец:</strong>
                @endif
                <br>
                <strong>Общо: {{ $count }}</strong>
            </div>
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <div>
                        <a href="{{ route('analyst.by-date', [1]) }}">
                            <button class="btn btn-outline-secondary">
                                Покажи за 24 часа
                            </button>
                        </a>
                    </div>
                    <div>
                        <a href="{{ route('analyst.by-date', [7]) }}">
                            <button class="btn btn-outline-secondary">
                                Покажи за 7 дни
                            </button>
                        </a>
                    </div>
                    <div>
                        <a href="{{ route('analyst.by-date', [30]) }}">
                            <button class="btn btn-outline-secondary">
                                Покажи за 1 месец
                            </button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <a href="{{ route('analyst.index') }}">
                    <button class="btn btn-success">
                        Назад
                    </button>
                </a>
            </div>
        </div>

        <div class="card text-center" style="font-size: .85em">
            <div class="card-header w-100">
                <div class="analyst-information font-weight-bolder">
                    <div>
                        #
                    </div>
                    <div>
                        Държава
                        <a href="{{ route('analyst.sort', ['country', 'asc']) }}" style="text-transform: none; text-decoration: none">
                            &#8593;
                        </a>
                        <a href="{{ route('analyst.sort', ['country', 'desc']) }}" style="text-transform: none; text-decoration: none">
                            &#8595;
                        </a>
                    </div>
                    <div>
                        Ip адрес
                        <a href="{{ route('analyst.sort', ['ip_address', 'asc']) }}" style="text-transform: none; text-decoration: none">
                            &#8593;
                        </a>
                        <a href="{{ route('analyst.sort', ['ip_address', 'desc']) }}" style="text-transform: none; text-decoration: none">
                            &#8595;
                        </a>
                    </div>
                    <div>
                        Потребител (id)
                        <a href="{{ route('analyst.sort', ['session_id', 'asc']) }}" style="text-transform: none; text-decoration: none">
                            &#8593;
                        </a>
                        <a href="{{ route('analyst.sort', ['session_id', 'desc']) }}" style="text-transform: none; text-decoration: none">
                            &#8595;
                        </a>
                    </div>
                    <div>
                        На дата
                        <a href="{{ route('analyst.sort', ['created_at', 'asc']) }}" style="text-transform: none; text-decoration: none">
                            &#8593;
                        </a>
                        <a href="{{ route('analyst.sort', ['created_at', 'desc']) }}" style="text-transform: none; text-decoration: none">
                            &#8595;
                        </a>
                    </div>
                    <div>
                        Идва от линк
                        <a href="{{ route('analyst.sort', ['referer', 'asc']) }}" style="text-transform: none; text-decoration: none">
                            &#8593;
                        </a>
                        <a href="{{ route('analyst.sort', ['referer', 'desc']) }}" style="text-transform: none; text-decoration: none">
                            &#8595;
                        </a>
                    </div>
                </div>
            </div>
            @foreach($analyzes as $analyze)
                <div class="card-body border-bottom">
                    <div class="analyst-information font-weight-bolder">
                        <div class="d-flex flex-column justify-content-center">
                            {{ $loop->iteration }}
                        </div>
                        <div>
                            <div class="px-1">
                                {{ $analyze->country }}
                            </div>
                            <br>
                            <br>
                            @if($analyze->country !== NULL)
                            <a href="{{ route('analyst.look', ['country', $analyze->country]) }}">
                                Виж всички от тази държава
                            </a>
                            @endif
                        </div>
                        <div>
                            <div class="px-1">
                                {{ $analyze->ip_address }}
                            </div>
                            <br>
                            <br>
                            <a href="{{ route('analyst.look', ['ip_address', $analyze->ip_address]) }}">
                                Виж всичко от този IP адрес
                            </a>
                        </div>
                        <div>
                            <div class="px-1">
                                {{ $analyze->session_id }}
                            </div>
                            <br>
                            <br>
                            <a href="{{ route('analyst.look', ['session_id', $analyze->session_id]) }}">
                                Виж всички от този потребител
                            </a>
                        </div>
                        <div>
                            <div class="px-1">
                                {{ $analyze->created_at }}
                            </div>
                            <br>
                            <br>
                            <a href="{{ route('analyst.look', ['created_at', $analyze->created_at->format('Y-m-d')]) }}">
                                Виж всички на тази дата
                            </a>
                        </div>
                        <div>
                            <div class="px-1">
                                {{ $analyze->referer }}
                            </div>
                            <br>
                            <br>
                            <a href="{{ route('analyst.look', ['referer', $analyze->site->string_id]) }}">
                                Виж всички от този сайт
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="mx-auto w-100">
            <div class="analyst-footer">
                {{ $analyzes->onEachSide(0)->links() }}
            </div>
        </div>
    </div>
@endsection
