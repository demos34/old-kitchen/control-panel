@extends('administrator.analyst.analyst-layout')
@section('cont')
    <div class="analyst-body">
        <div class="card my-2">
            <div class="card-header">
                <div>
                    Анализатор: <strong>{{ $analystCustom->name }}</strong>
                </div>
                <div>
                    От дата: <strong>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $analystCustom->start_date)->format('H:i:s d-m-Y') }}</strong>
                </div>
                <div>
                    До дата: <strong>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $analystCustom->end_date)->format('H:i:s d-m-Y') }}</strong>
                </div>
            </div>
            <div class="card-body">
                <strong>Общо: {{ $analystCustom->analyzeCustomRecords->count() }}</strong>
            </div>
            <div class="card-footer">
                <a href="{{ route('analyst.custom-analyst') }}">
                    <button class="btn btn-success">
                        Назад
                    </button>
                </a>
            </div>
        </div>

        <div class="card text-center" style="font-size: .85em">
            <div class="card-header w-100">
                <div class="analyst-information font-weight-bolder">
                    <div>
                        #
                    </div>
                    <div>
                        Държава
                        <a href="{{ route('analyst.custom-analyst.sort', [$analystCustom->id, 'country', 'asc']) }}" style="text-transform: none; text-decoration: none">
                            &#8593;
                        </a>
                        <a href="{{ route('analyst.custom-analyst.sort', [$analystCustom->id, 'country', 'desc']) }}" style="text-transform: none; text-decoration: none">
                            &#8595;
                        </a>
                    </div>
                    <div>
                        Ip адрес
                        <a href="{{ route('analyst.custom-analyst.sort', [$analystCustom->id, 'ip_address', 'asc']) }}" style="text-transform: none; text-decoration: none">
                            &#8593;
                        </a>
                        <a href="{{ route('analyst.custom-analyst.sort', [$analystCustom->id, 'ip_address', 'desc']) }}" style="text-transform: none; text-decoration: none">
                            &#8595;
                        </a>
                    </div>
                    <div>
                        Потребител (id)
                        <a href="{{ route('analyst.custom-analyst.sort', [$analystCustom->id, 'session_id', 'asc']) }}" style="text-transform: none; text-decoration: none">
                            &#8593;
                        </a>
                        <a href="{{ route('analyst.custom-analyst.sort', [$analystCustom->id, 'session_id', 'desc']) }}" style="text-transform: none; text-decoration: none">
                            &#8595;
                        </a>
                    </div>
                    <div>
                        На дата
                        <a href="{{ route('analyst.custom-analyst.sort', [$analystCustom->id, 'created_at', 'asc']) }}" style="text-transform: none; text-decoration: none">
                            &#8593;
                        </a>
                        <a href="{{ route('analyst.custom-analyst.sort', [$analystCustom->id, 'created_at', 'desc']) }}" style="text-transform: none; text-decoration: none">
                            &#8595;
                        </a>
                    </div>
                    <div>
                        Идва от линк
                        <a href="{{ route('analyst.custom-analyst.sort', [$analystCustom->id, 'referer', 'asc']) }}" style="text-transform: none; text-decoration: none">
                            &#8593;
                        </a>
                        <a href="{{ route('analyst.custom-analyst.sort', [$analystCustom->id, 'referer', 'desc']) }}" style="text-transform: none; text-decoration: none">
                            &#8595;
                        </a>
                    </div>
                </div>
            </div>
            @foreach($analyzes as $analyze)
                <div class="card-body border-bottom">
                    <div class="analyst-information font-weight-bolder">
                        <div class="d-flex flex-column justify-content-center">
                            {{ $loop->iteration }}
                        </div>
                        <div>
                            <div class="px-1">
                                {{ $analyze->country }}
                            </div>
                            <br>
                            <br>
                            @if($analyze->country !== NULL)
                            <a href="{{ route('analyst.look', ['country', $analyze->country]) }}">
                                Виж всички от тази държава
                            </a>
                            @endif
                        </div>
                        <div>
                            <div class="px-1">
                                {{ $analyze->ip_address }}
                            </div>
                            <br>
                            <br>
                            <a href="{{ route('analyst.look', ['ip_address', $analyze->ip_address]) }}">
                                Виж всичко от този IP адрес
                            </a>
                        </div>
                        <div>
                            <div class="px-1">
                                {{ $analyze->session_id }}
                            </div>
                            <br>
                            <br>
                            <a href="{{ route('analyst.look', ['session_id', $analyze->session_id]) }}">
                                Виж всички от този потребител
                            </a>
                        </div>
                        <div>
                            <div class="px-1">
                                {{ $analyze->created_at }}
                            </div>
                            <br>
                            <br>
                            <a href="{{ route('analyst.look', ['created_at', $analyze->created_at->format('Y-m-d')]) }}">
                                Виж всички на тази дата
                            </a>
                        </div>
                        <div>
                            <div class="px-1">
                                {{ $analyze->referer }}
                            </div>
                            <br>
                            <br>
                            <a href="{{ route('analyst.look', ['referer', $analyze->site->string_id]) }}">
                                Виж всички от този сайт
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="mx-auto mt-2 w-100">
            <div class="analyst-footer">
                {{ $analyzes->links() }}
            </div>
        </div>
    </div>
@endsection
