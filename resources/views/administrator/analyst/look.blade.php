@extends('administrator.analyst.analyst-layout')
@section('cont')
    <div class="analyst-body">
        <div class="card my-2">
            <div class="card-header">
                @if($value === 'country')
                    <strong>От държава: </strong>
                @elseif($value === 'ip_address')
                    <strong>От IP адрес: </strong>
                @elseif($value === 'session_id')
                    <strong>За потребител: </strong>
                @elseif($value === 'created_at')
                    <strong>На дата: </strong>
                @elseif($value === 'referer')
                    <strong>От сайт: </strong>
                @endif
                <strong>{{ $look }}</strong>
            </div>
            <div class="card-body">
                <strong>Общо: {{ $count }}</strong>
            </div>
            <div class="card-footer">
                <a href="{{ route('analyst.index') }}">
                    <button class="btn btn-success">
                        Назад
                    </button>
                </a>
            </div>
        </div>

        <div class="card text-center" style="font-size: .85em">
            <div class="card-header w-100">
                <div class="analyst-information font-weight-bolder">
                    <div>
                        #
                    </div>
                    <div>
                        Държава
                    </div>
                    <div>
                        Ip адрес
                    </div>
                    <div>
                        Потребител (id)
                    </div>
                    <div>
                        На дата
                    </div>
                    <div>
                        Идва от линк
                    </div>
                </div>
            </div>
            @foreach($analyzes as $analyze)
                <div class="card-body border-bottom">
                    <div class="analyst-information font-weight-bolder">
                        <div class="d-flex flex-column justify-content-center">
                            {{ $loop->iteration }}
                        </div>
                        <div>
                            <div class="px-1">
                                {{ $analyze->country }}
                            </div>
                            <br>
                            <br>
                            @if($analyze->country !== NULL)
                            <a href="{{ route('analyst.look', ['country', $analyze->country]) }}">
                                Виж всички от тази държава
                            </a>
                            @endif
                        </div>
                        <div>
                            <div class="px-1">
                                {{ $analyze->ip_address }}
                            </div>
                            <br>
                            <br>
                            <a href="{{ route('analyst.look', ['ip_address', $analyze->ip_address]) }}">
                                Виж всичко от този IP адрес
                            </a>
                        </div>
                        <div>
                            <div class="px-1">
                                {{ $analyze->session_id }}
                            </div>
                            <br>
                            <br>
                            <a href="{{ route('analyst.look', ['session_id', $analyze->session_id]) }}">
                                Виж всички от този потребител
                            </a>
                        </div>
                        <div>
                            <div class="px-1">
                                {{ $analyze->created_at }}
                            </div>
                            <br>
                            <br>
                            <a href="{{ route('analyst.look', ['created_at', $analyze->created_at->format('Y-m-d')]) }}">
                                Виж всички на тази дата
                            </a>
                        </div>
                        <div>
                            <div class="px-1">
                                {{ $analyze->referer }}
                            </div>
                            <br>
                            <br>
                            <a href="{{ route('analyst.look', ['referer', $analyze->site->string_id]) }}">
                                Виж всички от този сайт
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="mx-auto w-100">
            <div class="analyst-footer">
                {{ $analyzes->onEachSide(0)->links() }}
            </div>
        </div>
    </div>
@endsection
