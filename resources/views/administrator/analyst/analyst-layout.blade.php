@extends('layouts.app')
@include('partials.alerts')
@section('consult')
    <div class="analyst-wrapper">
        <div class="analyst-dashboard">
            <div class="consult-dashboard-title">
                <strong>
                    Дашборд
                </strong>
            </div>
            <div class="consult-dashboard-links">
                <ul>
                    <li>
                        <a href="{{route('analyst.index')}}" class="consult-dashboard-links-atags">
                            Цялата статистика:
                        </a>
                    </li>
                    <li>
                        <strong>
                            Анализатори:
                        </strong>
                        <ul>
                            <li>
                                <a href="{{route('analyst.by-date', [1])}}" class="consult-dashboard-links-atags">
                                    По дата
                                </a>
                            </li>
                            <li>
                                <a href="{{route('analyst.custom-analyst')}}" class="consult-dashboard-links-atags">
                                    Моите
                                </a>
                            </li>
                            <li>
                                <a href="{{route('analyst.archive')}}" class="consult-dashboard-links-atags">
                                    Архив
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <strong>
                            Брояч на посещения:
                        </strong>
                        <ul>
                            <li>
                                <a href="{{route('counter.index', 'sum')}}" class="consult-dashboard-links-atags">
                                    Общо за сайта
                                </a>
                            </li>
                            <li>
                                <a href="{{route('counter.by', [2, 'sum'])}}" class="consult-dashboard-links-atags">
                                    По категории
                                </a>
                            </li>
                            <li>
                                <a href="{{route('counter.by', [3, 'sum'])}}" class="consult-dashboard-links-atags">
                                    По рецепти
                                </a>
                            </li>
                            <li>
                                <a href="{{route('counter.by', [4, 'sum'])}}" class="consult-dashboard-links-atags">
                                    Блог
                                </a>
                            </li>
                            <li>
                                <a href="{{route('counter.by', [5, 'sum'])}}" class="consult-dashboard-links-atags">
                                    Youtube
                                </a>
                            </li>
                            <li>
                                <a href="{{route('counter.custom')}}" class="consult-dashboard-links-atags">
                                    Къстъм брояч
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        @yield('cont')
    </div>
@endsection
