@extends('administrator.consult.consult-layout')
@section('cont')
    <div class="consult-content">
        <div class="d-flex justify-content-between border-bottom my-2 p-1">
            <div>
                От: <strong>{{$client->first_name}} {{$client->last_name}}</strong>
            </div>
            <div>
                Подадено на: <strong>{{$message->created_at}}</strong>
            </div>
            <div>
                Прочетено на: <strong>{{$message->updated_at}}</strong>
            </div>
            @if($client->start_date > now())
                <div class="consult-status-active">
                    Статус:
                    <strong class="consult-status-strong-active">
                        Активна
                    </strong>
                    <div id="consult-status-active-description">
                        Датата, за която трябва да са налични рецептите, все още не е настъпила!
                    </div>
                </div>
            @else
                <div class="consult-status-un-active">
                    <strong class="consult-status-strong-un-active">
                        Статус: Неактивна
                    </strong>
                    <div id="consult-status-un-active-description">
                        Датата, за която трябва да са налични рецептите, вече е минала!
                    </div>
                </div>
            @endif
        </div>
        @if($client->consult_type_id == 2)
        <div class="border-bottom d-flex justify-content-between">
            <div>
                Име на компанията: <strong>{{$client->businessMessage->company}}</strong>
            </div>
            <div>
                Тип на събитието:
                <strong>
                    @if($client->businessMessage->type == NULL)
                        Не е подадено
                    @else
                        {{$client->businessMessage->type}}
                    @endif
                </strong>
            </div>
        </div>
        @endif
        <div class="border-bottom">
            <div id="consult-statuses-wrapper-info">
                <strong>
                    Състояние към момента:
                    <br>
                    <strong
                        @if(implode(',', $client->consultStatuses()->get()->pluck('id')->toArray()) == 1)
                        class="consult-status-strong-un-active"
                        @else
                        class="consult-status-strong-active"
                        @endif
                    >
                        {{implode(',', $client->consultStatuses()->get()->pluck('name')->toArray())}}
                    </strong>
                </strong>
                <div id="consult-statuses-info-description" class="consult-content-status-description">
                    <p>
                        {{implode(',', $client->consultStatuses()->get()->pluck('description')->toArray())}}
                    </p>
                </div>
            </div>
            <div>
                <strong>
                    Възможни състояния:
                </strong>
            </div>
            <div class="d-flex justify-content-between">
                @foreach($statuses as $status)
                    <div class="consult-content-status-wrapper">
                        <div class="w-100">
                            <a href="{{route('consult-status-update', [$status->id, $client->id])}}">
                                <button class="btn btn-info" @if($client->start_date < now()) disabled @endif>
                                    {{$status->name}}
                                </button>
                            </a>
                        </div>
                        <div class="consult-content-status-description">
                            <p>
                                {{$status->description}}
                            </p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="row my-2 p-1 border-bottom text-center" id="consult-address-bar">
            <div class="col-md-3">
                Страна:
                <strong>
                    @if($client->country === NULL)
                        Не е подадена!
                    @else
                        {{$client->country}}
                    @endif
                </strong>
            </div>
            <div class="col-md-3">
                Град:
                <strong>
                    @if($client->city === NULL)
                        Не е подаден!
                    @else
                        {{$client->city}}
                    @endif
                </strong>
            </div>
            <div class="col-md-3">
                Адрес 1:
                <strong>
                    @if($client->address_one === NULL)
                        Не е подаден!
                    @else
                        {{$client->address_one}}
                    @endif
                </strong>
            </div>
            <div class="col-md-3">
                Адрес 2:
                <strong>
                    @if($client->address_two === NULL)
                        Не е подаден!
                    @else
                        {{$client->address_two}}
                    @endif
                </strong>
            </div>
        </div>
        <div class="row my-2 border-bottom text-center">
            <div class="col-md-6">
                Имейл:
                <strong>
                    {{$client->email}}
                </strong>
            </div>
            <div class="col-md-6">
                Телефон:
                <strong>
                    {{$client->phone}}
                </strong>
            </div>
        </div>
        @if($client->consult_type_id == 1)
            <div class="my-3 p-2 border-bottom row">
                <div class="col-md-12 text-center">
                    <h3>
                        Категории:
                    </h3>
                </div>
                @foreach($client->consultCategories as $category)
                    <div class="p-1 col-md-3 offset-1 border">
                        <div>
                            Категория: <strong>{{$category->name}}</strong>
                        </div>
                        <div class="p-1">
                            Подкатегории:
                            <ul>
                                @foreach($client->consultSubcategories->where('consult_category_id', $category->id) as $sub)
                                    <li>
                                        <strong>{{$sub->name}}</strong>
                                    </li>
                                @endforeach
                                @if($client->clientCategoryAntherDishes->where('consult_category_id', $category->id)->first())
                                    <li>
                                        <strong>
                                            Друго (описание):
                                        </strong>
                                        <p class="font-weight-bold font-italic my-1">
                                            {{$client->clientCategoryAntherDishes->where('consult_category_id', $category->id)->first()['description']}}
                                        </p>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="my-3 p-2 border-bottom">
                <div class="d-flex justify-content-between my-1">
                    <div>
                        Люто: <strong>{{$client->clientInformation->spice->name}}</strong>
                    </div>
                    <div>
                        <form action="{{route('consult-update-spice', $client->id)}}" method="post">
                            @csrf
                            В случай на нужда:
                            <label for="spice"></label>
                            <select id="spice" name="spice" class="">
                                @foreach($spices as $spice)
                                    <option @if($client->clientInformation->spice_id === $spice->id) selected
                                            @endif value="{{$spice->id}}">{{$spice->name}}</option>
                                @endforeach
                            </select>
                            <button class="btn btn-dark">Промени:</button>
                        </form>
                    </div>
                </div>
                <div class="my-2 p-1 border-bottom">
                    Любими храни:
                    @if($client->clientInformation->fav_foods === NULL)
                        <strong>Не са подадени!</strong>
                    @else
                        <strong>
                            {{$client->clientInformation->fav_foods}}
                        </strong>
                    @endif
                </div>
                <div class="my-2 p-1 border-bottom">
                    Любими ресторанти:
                    @if($client->clientInformation->fav_rest === NULL)
                        <strong>Не са подадени!</strong>
                    @else
                        <strong>
                            {{$client->clientInformation->fav_rest}}
                        </strong>
                    @endif
                </div>
                <div class="my-2 p-1 font-weight-bold font-italic border-bottom">
                    Алергии:
                    <strong>
                        {{$client->clientInformation->allergic}}
                    </strong>
                </div>
                <div class="my-2 p-1 font-weight-bold font-italic border-bottom">
                    Допълнителна информация:
                    <strong>
                        {{$client->clientInformation->info}}
                    </strong>
                </div>
            </div>
        @else
            <div class="my-3 p-2 border-bottom">
                <div class="my-2 p-1 font-weight-bold font-italic">
                    Допълнителна информация:
                    <strong>
                        {{$client->businessMessage->message}}
                    </strong>
                </div>
            </div>
        @endif
    </div>
@endsection
