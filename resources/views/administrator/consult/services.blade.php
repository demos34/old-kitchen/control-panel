@extends('administrator.consult.consult-layout')
@section('cont')
    <div class="consult-content">
        <div class="mx-auto text-center">
            <strong>
                Данни, които ще бъдат публикувани в страницата за консултации:
            </strong>
        </div>
        <div>
            <div>
                <strong>
                    Предлагани услуги
                </strong>
                <br>
                <small>Тук са всички услуги, които се предлагат</small>
            </div>
            <div class="row mx-auto w-100">
                <div class="col-lg-8">
                    <div class="mx-auto text-center">
                        <strong>
                            Съществуващи услуги:
                        </strong>
                    </div>
                    <div class="mx-auto">
                        <strong>
                            Услуги:
                        </strong>
                    </div>
                    <div class="consult-content-form-categories mx-auto text-center w-100">
                        <div class="row w-100">
                            <div class="col-md-1 my-2 ml-3">
                                <span>#</span>
                            </div>
                            <div class="col-md-6 my-2 ml-1">
                                Услуга
                            </div>
                            <div class="col-md-3 my-2 ml-1">
                                Вид събитие
                            </div>
                            <div class="col-md-1 my-2 text-center">
                                Виж
                            </div>
                        @foreach($services as $service)
                            <div class="col-md-1 my-1 ml-3" style="display: flex; flex-direction: column; justify-content: center; border-radius: 7px; border: 1px solid gray">
                                <span>{{$service->id}}</span>
                            </div>
                            <div class="col-md-6 my-1 ml-1" style="display: flex; flex-direction: column; justify-content: center; border-radius: 7px; border: 1px solid gray">
                                <span>{{$service->service}}</span>
                            </div>
                            <div class="col-md-3 my-1 ml-1" style="display: flex; flex-direction: column; justify-content: center; color: white; border-radius: 7px; background: @if($service->consultType->id == 1) #88B475 @else #38261E @endif">
                                <a class="consult-service-type" href="{{route('consult-type-show', $service->consultType->slug)}}">{{$service->consultType->name}}</a>
                            </div>
                            <div class="col-md-1 my-1 text-center" style="display: flex; flex-direction: column; justify-content: center">
                                <a href="{{route('consult-service-show', $service->id)}}">
                                    <button class="btn btn-success">Виж</button>
                                </a>
                            </div>
                            <div class="col-md-12 border-bottom">

                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 border-element">
                    <div class="mx-auto text-center">
                        <strong>
                            Добави нова услуга:
                        </strong>
                    </div>
                    <div class="mx-auto text-center">
                        <form action="{{route('consult-service-store')}}" method="post">
                            @csrf
                            <div class="mx-auto my-1">
                                <label for="service">
                                    Услуга:
                                </label>
                                <input value="{{ old('service') }}" id="service" name="service" class="form-control @error('service') is-invalid @enderror" required autocomplete="service">
                                @error('service')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mx-auto my-1">
                                <label for="type">
                                    Избери за кой вид консултации
                                </label>
                                <select name="type" id="type">
                                    @foreach($types as $type)
                                        <option value="{{$type->id}}">
                                            {{$type->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="text-center mx-auto my-1">
                                <button class="btn btn-dark">
                                    Добави!
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
