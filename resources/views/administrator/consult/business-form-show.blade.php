@extends('administrator.consult.consult-layout')
@section('cont')
    <div class="consult-content">
        <form action="{{route('consult-form-show-post', $type->slug)}}" method="post">
            @csrf
            <div class="consult-content-title">
                <strong>
                    Нов клиент:
                </strong>
            </div>
            <div class="consult-content-name">
                <strong>Имена*</strong>
                <br>
                <label for="first_name">Собствено*</label>
                <input type="text" id="first_name" class="@error('first_name') is-invalid @enderror" name="first_name" required autocomplete="first_name">
                @error('first_name')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
                <label for="last_name">Фамилно*</label>
                <input type="text" id="last_name" class="@error('last_name') is-invalid @enderror" name="last_name" required autocomplete="last_name">
                @error('last_name')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>

            <div class="consult-content-address">
                <strong>Фирма/Компания/Предприятие и Тип събитие</strong>
                <br>
                <label for="company">Име*</label>
                <input type="company" id="company" class="@error('company') is-invalid @enderror" name="company" required autocomplete="company">
                @error('company')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
                <label for="type">Тип събитие:</label>
                <input type="text" id="type" class="@error('type') is-invalid @enderror" name="type" autocomplete="type">
                @error('type')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="consult-content-address">
                <strong>Адрес</strong>
                <br>
                <label for="country">Държава</label>
                <input type="text" id="country" class="@error('country') is-invalid @enderror" name="country" autocomplete="country">
                @error('country')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
                <label for="city">Град</label>
                <input type="text" id="city" class="@error('city') is-invalid @enderror" name="city" autocomplete="city">
                @error('city')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
                <br>
                <label for="address_one">Адрес</label>
                <input type="text" id="address_one" class="@error('address_one') is-invalid @enderror" name="address_one" autocomplete="address_one">
                @error('address_one')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
                <label for="address_two">Адрес 2</label>
                <input type="text" id="address_two" class="@error('address_two') is-invalid @enderror" name="address_two" autocomplete="address_two">
            </div>
            <div class="consult-content-address">
                <strong>Имейл и телефон*</strong>
                <br>
                <label for="email">Имейл*</label>
                <input type="email" id="email" class="@error('email') is-invalid @enderror" name="email" required autocomplete="email">
                @error('email')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
                <label for="phone">Телефон*</label>
                <input type="text" id="phone" class="@error('phone') is-invalid @enderror" name="phone" required autocomplete="phone">
                @error('phone')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="consult-content-address">
                <strong>За дата*</strong>
                <br>
                <label class="consult-other-label" for="start_date">Моля, посочете дата, за която искате да са готови рецептите:</label>
                <br>
                <input value="{{old('start_date')}}" type="datetime-local" id="start_date" class="@error('start_date') is-invalid @enderror consult-other-input" name="start_date" autocomplete="start_date">
                @error('start_date')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="consult-content-address">
                <strong>Допълнителна информация*</strong>
                <br>
                <label class="consult-other-label" for="info">Моля, ако има нещо, което сме пропуснали във формуляра си, опишете го тук:</label>
                <br>
                <textarea type="text" id="allergic" class="@error('info') is-invalid @enderror consult-other-textarea" name="info" autocomplete="info">{{old('info')}}</textarea>
                @error('info')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="mx-auto text-center">
                <button class="btn btn-dark">
                    Submit
                </button>
            </div>
        </form>
    </div>
@endsection
