@extends('layouts.app')
@include('partials.alerts')
@section('consult')
    <div class="consult-wrapper">
        <div class="consult-dashboard">
            <div class="consult-dashboard-title">
                <strong>
                    Дашборд
                </strong>
            </div>
            <div class="consult-dashboard-links">
                <ul>
                    <li>
                        <a href="{{route('consult-index')}}" class="consult-dashboard-links-atags">
                            Входящи съобщения:
                            @if($unreadConsult->count()>0)
                                <strong
                                    style="background: white; color: green; border-radius: 2px; font-size: 1.1em">{{$unreadConsult->count()}}</strong>
                            @endif
                        </a>
                    </li>
                    <li>
                        <strong>
                            Услуги:
                        </strong>
                        <ul>
                            <li>
                                <a href="{{route('consult-services')}}" class="consult-dashboard-links-atags">
                                    Добави/промени услуга
                                </a>
                            </li>
                            <li>
                                <a href="{{route('consult-type-show', 'beginner')}}" class="consult-dashboard-links-atags">
                                    Услуги тип "Лични"
                                </a>
                            </li>
                            <li>
                                <a href="{{route('consult-type-show', 'business')}}" class="consult-dashboard-links-atags">
                                    Услуги тип "Бизнес"
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <strong>За лични събития:</strong>
                        <ul>
                            <li>
                                <a href="{{route('consult-description', ['beginner'])}}" class="consult-dashboard-links-atags">
                                    Описание
                                </a>
                            </li>
                            <li>
                                <a href="{{route('consult-form')}}" class="consult-dashboard-links-atags">
                                    Данни за формуляр
                                </a>
                            </li>
                            <li>
                                <a href="{{route('consult-form-show')}}" class="consult-dashboard-links-atags">
                                    Виж формуляра
                                </a>
                            </li>
                            <li>
                                <a href="{{route('consult-status-index', [3, 'beginner'])}}" class="consult-dashboard-links-atags">
                                    Консултации
                                </a>
                            </li>
                            <li>
                                <a href="{{route('consult-comments-index', 'beginner')}}" class="consult-dashboard-links-atags">
                                    Коментари и отзиви
                                </a>
                            </li>
                            <li>
                                Личните консултации са:
                                <br>
                                @if($consultBeginner->is_on == false)
                                    <strong style="color: red; background: white">
                                        Изключени
                                    </strong>
                                @else
                                    <strong style="color: green; background: white">
                                        Включени
                                    </strong>
                                @endif
                            </li>
                            <li>
                                @if($consultBeginner->is_on == false)
                                    <a href="{{route('consult-turn-on-off', [1, 'on'])}}">
                                        <button class="btn btn-success">
                                            Включи!
                                        </button>
                                    </a>
                                @else
                                    <a href="{{route('consult-turn-on-off', [1, 'off'])}}">
                                        <button class="btn btn-warning">
                                            Изключи!
                                        </button>
                                    </a>
                                @endif
                            </li>
                        </ul>
                    </li>
                    <li>
                        <strong>За бизнес събития:</strong>
                        <ul>
                            <li>
                                <a href="{{route('consult-description', ['business'])}}" class="consult-dashboard-links-atags">
                                    Описание
                                </a>
                            </li>
                            <li>
                                <a href="{{route('consult-business-form-show')}}" class="consult-dashboard-links-atags">
                                    Виж формуляра
                                </a>
                            </li>
                            <li>
                                <a href="{{route('consult-status-index', [3, 'business'])}}" class="consult-dashboard-links-atags">
                                    Консултации
                                </a>
                            </li>
                            <li>
                                <a href="{{route('consult-comments-index', 'business')}}" class="consult-dashboard-links-atags">
                                    Коментари и отзиви
                                </a>
                            </li>
                            <li>
                                Бизнес консултациите са:
                                <br>
                                @if($consultBusiness->is_on == false)
                                    <strong style="color: red; background: white">
                                        Изключени
                                    </strong>
                                @else
                                    <strong style="color: green; background: white">
                                        Включени
                                    </strong>
                                @endif
                            </li>
                            <li>
                                @if($consultBusiness->is_on == false)
                                    <a href="{{route('consult-turn-on-off', [2, 'on'])}}">
                                        <button class="btn btn-success">
                                            Включи!
                                        </button>
                                    </a>
                                @else
                                    <a href="{{route('consult-turn-on-off', [2, 'off'])}}">
                                        <button class="btn btn-warning">
                                            Изключи!
                                        </button>
                                    </a>
                                @endif
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="mx-auto my-5 text-center">
                <div>
                    В момента консултациите са:
                    @if($on->is_on == false)
                        <strong style="background: red; color: white">
                            Изключени
                        </strong>
                    @else
                        <strong style="background: green; color: white">
                            Включени
                        </strong>
                    @endif
                </div>
                <div class="my-2">
                    @if($on->is_on == false)
                        <a href="{{route('consult-turn')}}">
                            <button class="btn btn-success">
                                Включи
                            </button>
                        </a>
                    @else
                        <a href="{{route('consult-turn')}}">
                            <button class="btn btn-danger">
                                Изключи
                            </button>
                        </a>
                    @endif
                </div>
            </div>
            <div class="consult-dashboard-links mx-auto text-center my-5">
                @yield('consult-back')
            </div>
        </div>
        @yield('cont')
    </div>
@endsection
