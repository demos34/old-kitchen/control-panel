@extends('administrator.consult.consult-layout')
@section('cont')
    <div class="consult-content">
        <div class="mx-auto text-center border-bottom">
            <strong>
                Меню консултации за "{{$type->name}}"
            </strong>
        </div>
        <div class="d-flex justify-content-between my-2 p-3 border-bottom">
            @foreach($statuses as $status)
                <div @if($default->id === $status->id) class="consult-status-active" @else class="consult-status-index-links" @endif>
                    <a href="{{route('consult-status-index', [$status->id, $type->slug])}}">
                        {{$status->name}}
                    </a>
                </div>
            @endforeach
        </div>
        @foreach($clients as $client)
            <div class="border my-3" @if(!$client->message->is_read) style="background: greenyellow"@endif>
                <div class="d-flex border-bottom justify-content-between my-1 p-1">
                    <div>
                        <strong>
                            От: {{$client->first_name}} {{$client->last_name}}
                        </strong>
                    </div>
                    <div>
                        <strong>
                            Получено на: {{$client->message->created_at}}
                        </strong>
                    </div>
                </div>
                <div class="text-justify my-1 p-1">
                    <p>{{$client->message->message}}</p>
                </div>
                <div class="d-flex justify-content-end p-3">
                    <a href="{{route('consult-message-show', $client->message->id)}}">
                        <button class="btn btn-primary">Виж:</button>
                    </a>
                </div>
            </div>
        @endforeach
        {{$clients->links()}}
    </div>
@endsection
