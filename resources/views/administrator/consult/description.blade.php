@extends('administrator.consult.consult-layout')
@section('cont')
    <div class="consult-content">
        <div class="w-100">
            <div class="w-100 text-center border-bottom">
                <h3>
                    Промени описание за "{{ $consultType->name }}"
                </h3>
            </div>
            <div class="w-100">
                @if($consultType->description !== NULL)
                    <form action="{{ route('consult-description-update', $consultType->description->id) }}" method="post">
                        @csrf
                        @method('PUT')
                        <div>
                            <label class="form-check-label my-2 p-3" for="description">
                                <u>
                                    Описание:
                                </u>
                            </label>
                            <textarea class="form-control h-25 w-100" name="description" id="description"
                            >@if(old('description')){{old('description')}}@else{{$consultType->description->description}}@endif</textarea>
                        </div>
                        <button class="btn btn-primary my-3">Промени</button>
                    </form>
                @else
                    Все още няма зададено описание за този тип консултации.
                    <br>
                    <strong>
                        Създай:
                    </strong>
                    <form action="{{ route('consult-description-store', $consultType->slug) }}" method="post">
                        @csrf
                        <div>
                            <label class="form-check-label my-2 p-3" for="description">
                                <u>
                                    Описание:
                                </u>
                            </label>
                            <textarea class="form-control h-25 w-100" name="description"
                                      id="description">{{old('description')}}</textarea>
                        </div>
                        <button class="btn btn-primary my-3">Създай</button>
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection
