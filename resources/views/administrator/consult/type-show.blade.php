@extends('administrator.consult.consult-layout')
@section('cont')
    <div class="consult-content">
        <div class="mx-auto text-center">
            <strong>
                Услуги тип "{{$type->name}}"
            </strong>
        </div>
        <div>
            <div>
                <strong>
                    Предлагани услуги
                </strong>
                <br>
                <small>Тук са всички услуги, които се предлагат</small>
            </div>
            <div class="row mx-auto w-100">
                <div class="mx-auto text-center">
                    <strong>
                        Съществуващи услуги:
                    </strong>
                </div>
                <div class="mx-auto">
                    <strong>
                        Услуги:
                    </strong>
                </div>
                <div class="consult-content-form-categories mx-auto text-center w-100">
                    <div class="row w-100">
                        <div class="col-md-1 my-2 ml-3">
                            <span>#</span>
                        </div>
                        <div class="col-md-6 my-2 ml-1">
                            Услуга
                        </div>
                        <div class="col-md-3 my-2 ml-1">
                            Цена в лева
                        </div>
                        <div class="col-md-1 my-2 text-center">
                            Виж
                        </div>
                        @foreach($type->consultServices as $service)
                            <div class="col-md-1 my-1 ml-3"
                                 style="display: flex; flex-direction: column; justify-content: center; border-radius: 7px; border: 1px solid gray">
                                <span>{{$service->id}}</span>
                            </div>
                            <div class="col-md-6 my-1 ml-1"
                                 style="display: flex; flex-direction: column; justify-content: center; border-radius: 7px; border: 1px solid gray">
                                <span>{{$service->service}}</span>
                            </div>
                            <div class="col-md-3 my-1 ml-1">
                                @if($service->consultPrice !== NULL)
                                    {{$service->consultPrice->price}} лв.
                                @else
                                    Няма все още
                                @endif
                            </div>
                            <div class="col-md-1 my-1 text-center"
                                 style="display: flex; flex-direction: column; justify-content: center">
                                <a href="{{route('consult-service-show', $service->id)}}">
                                    <button class="btn btn-success">Виж</button>
                                </a>
                            </div>
                            <div class="col-md-12 border-bottom">

                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
