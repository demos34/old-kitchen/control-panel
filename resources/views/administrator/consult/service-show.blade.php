@extends('administrator.consult.consult-layout')
@section('consult-back')
    <a href="{{route('consult-services')}}">
        <button class="btn btn-success">
            Назад
        </button>
    </a>
@endsection
@section('cont')
    <div class="consult-content">
        <div class="mx-auto text-center my-3">
            <strong>
                {{$service->service}}
            </strong>
        </div>
        <div class="mx-auto my-2 text-center">
            <div>
                <strong>
                    Цена:
                    @if($service->consultPrice !== NULL)
                        {{$service->consultPrice->price}}
                    @else
                        Няма все още
                    @endif
                </strong>
            </div>
            <div>
                <strong>Промени/Задай цена:</strong>
                <div class="w-25 mx-auto">
                    <form action="{{route('consult-service-set-price', $service->id)}}" method="post">
                        @csrf
                        @method('PATCH')
                        <div class="mx-auto my-1">
                            <label for="price">
                                Име:
                            </label>

                            @if($service->consultPrice !== NULL)<input
                                value="@if(old('price')){{old('price')}}@else {{$service->consultPrice->price}}@endif"
                                id="price"
                                name="price"
                                class="form-control @error('price') is-invalid @enderror"
                                required autocomplete="price">
                            @error('price')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                            @else
                                <input
                                    value="{{old('price')}}"
                                    id="price"
                                    name="price"
                                    class="form-control @error('price') is-invalid @enderror"
                                    required autocomplete="price">
                                @error('price')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            @endif
                        </div>
                        <div class="text-center mx-auto mt-3">
                            <button class="btn btn-dark">
                                Промени!
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="text-center mx-auto my-3">
            <div>
                <strong>
                    Промени {{$service->service}}
                </strong>
            </div>
            <form action="{{route('consult-service-update', $service->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="mx-auto my-1">
                    <label for="service">
                        Име:
                    </label>
                    <input
                        value="@if(old('service')){{old('service')}}@else {{$service->service}}@endif"
                        id="service"
                        name="service"
                        class="form-control @error('service') is-invalid @enderror"
                        required autocomplete="service">
                    @error('service')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>

                <div class="mx-auto my-1">
                    <label for="type">
                        Избери за кой вид консултации
                    </label>
                    <br>
                    <select name="type" id="type">
                        @foreach($types as $type)
                            <option value="{{$type->id}}" @if($service->consultType->id == $type->id) selected @endif>
                                {{$type->name}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="text-center mx-auto mt-3">
                    <button class="btn btn-dark">
                        Промени!
                    </button>
                </div>
            </form>
            <span>
                Или
            </span>
            <form action="{{route('consult-service-delete', $service->id)}}" method="post">
                @csrf
                @method('DELETE')
                <div class="text-center mx-auto mt-3">
                    <button class="btn btn-danger" onclick="return confirm('Сигурен ли сте?')">
                        Изтрий!
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
