@extends('administrator.consult.consult-layout')
@section('cont')
    <div class="consult-content">
        @foreach($messages as $message)
            <div class="border my-3" @if(!$message->is_read) style="background: greenyellow"@endif>
                <div class="d-flex border-bottom justify-content-between my-1 p-1">
                    <div>
                        <strong>
                            От: {{$message->client->first_name}} {{$message->client->last_name}}
                        </strong>
                    </div>
                    <div>
                        <strong>
                        @if($message->is_business == true)
                            Бизнес събитие
                        @else
                            Лично събитие
                        @endif
                        </strong>
                    </div>
                    <div>
                        <strong>
                            Получено на: {{$message->created_at}}
                        </strong>
                    </div>
                </div>
                <div class="text-justify my-1 p-1">
                    <p>{{$message->message}}</p>
                </div>
                <div class="d-flex justify-content-end p-3">
                    <a href="{{route('consult-message-show', $message->id)}}">
                        <button class="btn btn-primary">Виж:</button>
                    </a>
                </div>
            </div>
        @endforeach
        {{$messages->links()}}
    </div>
@endsection
