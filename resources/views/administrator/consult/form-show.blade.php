@extends('administrator.consult.consult-layout')
@section('cont')
    <div class="consult-content">
        <form action="{{route('consult-form-show-post', $type->slug)}}" method="post">
            @csrf
            <div class="consult-content-title">
                <strong>
                    Нов клиент:
                </strong>
            </div>
            <div class="consult-content-name">
                <strong>Имена*</strong>
                <br>
                <label for="first_name">Собствено</label>
                <input type="text" id="first_name" class="@error('first_name') is-invalid @enderror" name="first_name" required autocomplete="first_name">
                @error('first_name')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
                <label for="last_name">Фамилно</label>
                <input type="text" id="last_name" class="@error('last_name') is-invalid @enderror" name="last_name" required autocomplete="last_name">
                @error('last_name')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="consult-content-address">
                <strong>Адрес</strong>
                <br>
                <label for="country">Държава</label>
                <input type="text" id="country" class="@error('country') is-invalid @enderror" name="country" autocomplete="country">
                @error('country')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
                <label for="city">Град</label>
                <input type="text" id="city" class="@error('city') is-invalid @enderror" name="city" autocomplete="city">
                @error('city')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
                <br>
                <label for="address_one">Адрес</label>
                <input type="text" id="address_one" class="@error('address_one') is-invalid @enderror" name="address_one" autocomplete="address_one">
                @error('address_one')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
                <label for="address_two">Адрес 2</label>
                <input type="text" id="address_two" class="@error('address_two') is-invalid @enderror" name="address_two" autocomplete="address_two">
            </div>
            <div class="consult-content-address">
                <strong>Имейл (Електронна поща)*</strong>
                <br>
                <label for="email">Имейл</label>
                <input type="email" id="email" class="@error('email') is-invalid @enderror" name="email" required autocomplete="email">
                @error('email')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="consult-content-address">
                <strong>Телефон*</strong>
                <br>
                <label for="phone">Phone:</label>
                <input type="text" id="phone" class="@error('phone') is-invalid @enderror" name="phone" required autocomplete="phone">
                @error('phone')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            @foreach($categories as $category)
                <div class="consult-content-category">
                    <strong>{{$category->name}}*</strong>
                    <br>
                    @foreach($category->consultSubcategories as $subcategory)
                        <div class="pl-5">
                            <input type="checkbox" id="{{$category->name . '_' . $subcategory->id}}" class="" name="{{$category->name}}[]" value="{{$subcategory->id}}">
                            <label for="{{$category->name . '_' . $subcategory->id}}">{{$subcategory->name}}</label>
                        </div>
                    @endforeach
                    <div class="pl-5">
                        <input type="checkbox" id="{{$category->name . '_other'}}" class="" name="{{$category->name}}[]" value="{{$category->name}}">
                        <label for="{{$category->name . '_other'}}">Друго?</label>
                    </div>
                    <div class="pl-5">
                        <label for="{{$category->name . '_descr'}}">Ако сте избрали "Друго", моля опишете:</label>
                        <br>
                        <input value="{{old($category->name . '_descr')}}" type="text" id="{{$category->name . '_descr'}}" class="@error('$category->name' . '_descr') is-invalid @enderror consult-other-input" name="{{$category->name . '_descr'}}" autocomplete="{{$category->name . '_descr'}}">
                        @error('$category->name' . '_descr')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
            @endforeach
            <div class="consult-content-address">
                <strong>Люто?*</strong>
                <br>
                <label for="spice"></label>
                <select id="spice" name="spice" class="">
                    @foreach($spices as $spice)
                        <option value="{{$spice->id}}">{{$spice->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="consult-content-address">
                <strong>Любими храни</strong>
                <br>
                <label class="consult-other-label" for="fav_foods">Моля, избройте любимите си храни:</label>
                <br>
                <input value="{{old('fav_foods')}}" type="text" id="fav_foods" class="@error('fav_foods') is-invalid @enderror consult-other-input" name="fav_foods" autocomplete="fav_foods">
                @error('fav_foods')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="consult-content-address">
                <strong>Любими ресторанти</strong>
                <br>
                <label class="consult-other-label" for="fav_rest">Моля, избройте любимите си ресторанти:</label>
                <br>
                <input value="{{old('fav_rest')}}" type="text" id="fav_rest" class="@error('fav_rest') is-invalid @enderror consult-other-input" name="fav_rest" autocomplete="fav_rest">
                @error('fav_rest')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="consult-content-address">
                <strong>Алергии към храни*</strong>
                <br>
                <label class="consult-other-label" for="allergic">Моля, избройте, ако имате, храни, към които имате алергии, a ако нямате, то просто напишете "Не":</label>
                <br>
                <input value="{{old('allergic')}}" type="text" id="allergic" class="@error('allergic') is-invalid @enderror consult-other-input" name="allergic" autocomplete="allergic">
                @error('allergic')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="consult-content-address">
                <strong>За дата*</strong>
                <br>
                <label class="consult-other-label" for="start_date">Моля, посочете дата, за която искате да са готови рецептите:</label>
                <br>
                <input value="{{old('start_date')}}" type="datetime-local" id="start_date" class="@error('start_date') is-invalid @enderror consult-other-input" name="start_date" autocomplete="start_date">
                @error('start_date')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="consult-content-address">
                <strong>Допълнителна информация*</strong>
                <br>
                <label class="consult-other-label" for="info">Моля, ако има нещо, което сме пропуснали във формуляра си, опишете го тук:</label>
                <br>
                <textarea type="text" id="allergic" class="@error('info') is-invalid @enderror consult-other-textarea" name="info" autocomplete="info">{{old('info')}}</textarea>
                @error('info')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="mx-auto text-center">
                <button class="btn btn-dark">
                    Submit
                </button>
            </div>
        </form>
    </div>
@endsection
