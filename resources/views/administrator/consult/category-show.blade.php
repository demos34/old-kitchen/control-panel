@extends('administrator.consult.consult-layout')
@section('consult-back')
    <a href="{{route('consult-form')}}">
        <button class="btn btn-success">
            Назад
        </button>
    </a>
@endsection
@section('cont')
    <div class="consult-content">
        <div class="mx-auto text-center my-3">
            <strong>
                {{$category->name}}
            </strong>
        </div>
        <div class="text-center mx-auto my-3">
            <div>
                <strong>
                    Промени {{$category->name}}
                </strong>
            </div>
            <form action="{{route('consult-form-category-update', $category->id)}}" method="POST">
                @csrf
                @method('PUT')
                <div class="mx-auto my-1">
                    <label for="category">
                        Име:
                    </label>
                    <input value="@if(old('category')){{old('category')}}@else {{$category->name}}@endif" id="category" name="category" class="form-control @error('category') is-invalid @enderror" required autocomplete="category">
                    @error('category')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="text-center mx-auto mt-3">
                    <button class="btn btn-dark">
                        Промени!
                    </button>
                </div>
            </form>
            <span>
                Или
            </span>
            <form action="{{route('consult-form-category-delete', $category->id)}}" method="post">
                @csrf
                @method('DELETE')
                <div class="text-center mx-auto mt-3">
                    <button class="btn btn-danger" onclick="return confirm('Сигурен ли сте?')">
                        Изтрий!
                    </button>
                </div>
            </form>
        </div>
        <div class="row mx-auto w-100 my-3">
            <div class="col-lg-8">
                <div class="mx-auto text-center">
                    <strong>
                        Съществуващи подкатегории:
                    </strong>
                </div>
                <div class="mx-auto">
                    <strong>
                        Подкатегории:
                    </strong>
                </div>
                <div class="consult-content-form-categories mx-auto text-center w-100">
                    <div class="row w-100">
                        <div class="col-md-1 my-2">
                            <span>#</span>
                        </div>
                        <div class="col-md-7 my-2">
                            Подкатегория
                        </div>
                        <div class="col-md-4 my-2">
                            Виж
                        </div>
                        @foreach($category->consultSubcategories as $subcategory)
                            <div class="col-md-1 my-1">
                                <span>{{$subcategory->id}}</span>
                            </div>
                            <div class="col-md-7 my-1">
                                <span>{{$subcategory->name}}</span>
                            </div>
                            <div class="col-md-4 my-1">
                                <a href="{{route('consult-form-subcategory-show', $subcategory->id)}}">
                                    <button class="btn btn-success">Виж</button>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-4 border-element">
                <div class="mx-auto text-center">
                    <strong>
                        Добави нова подкатегория:
                    </strong>
                </div>
                <div class="mx-auto text-center">
                    <form action="{{route('consult-form-subcategory-store')}}" method="post">
                        @csrf
                        @method('PATCH')
                        <div class="mx-auto my-1">
                            <input type="number" hidden name="category" value="{{$category->id}}">
                            <label for="subcategory">
                                Име:
                            </label>
                            <input value="{{ old('subcategory') }}" id="subcategory" name="subcategory"
                                   class="form-control @error('subcategory') is-invalid @enderror" required
                                   autocomplete="category">
                            @error('subcategory')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="text-center mx-auto my-1">
                            <button class="btn btn-dark">
                                Добави!
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
