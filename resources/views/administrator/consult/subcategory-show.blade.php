@extends('administrator.consult.consult-layout')
@section('consult-back')
    <a href="{{route('consult-form-category-show', $subcategory->consult_category_id)}}">
        <button class="btn btn-success">
            Назад
        </button>
    </a>
@endsection
@section('cont')
    <div class="consult-content">
        <div class="mx-auto text-center my-3">
            <strong>
                {{$subcategory->name}}
            </strong>
        </div>
        <div class="text-center mx-auto my-3">
            <div>
                <strong>
                    Промени {{$subcategory->name}}
                </strong>
            </div>
            <form action="{{route('consult-form-subcategory-update', $subcategory->id)}}" method="POST">
                @csrf
                @method('PATCH')
                <div class="mx-auto my-1">
                    <label for="subcategory">
                        Име:
                    </label>
                    <input value="@if(old('subcategory')){{old('subcategory')}}@else {{$subcategory->name}}@endif" id="subcategory" name="subcategory" class="form-control @error('subcategory') is-invalid @enderror" required autocomplete="subcategory">
                    @error('subcategory')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="text-center mx-auto mt-3">
                    <button class="btn btn-dark">
                        Промени!
                    </button>
                </div>
            </form>
            <span>
                Или
            </span>
            <form action="{{route('consult-form-subcategory-delete', $subcategory->id)}}" method="post">
                @csrf
                @method('DELETE')
                <div class="text-center mx-auto mt-3">
                    <button class="btn btn-danger" onclick="return confirm('Сигурен ли сте?')">
                        Изтрий!
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
