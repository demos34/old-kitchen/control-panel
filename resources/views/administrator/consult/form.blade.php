@extends('administrator.consult.consult-layout')
@section('cont')
    <div class="consult-content">
        <div class="mx-auto text-center">
            <strong>
                Данни, които ще бъдат публикувани във формуляра за клиенти:
            </strong>
        </div>
        <div>
            <div>
                <strong>
                    Категории храни
                </strong>
                <br>
                <small>Тук са всички видове категории - с месо, с риба, веган, вегетариански и т.н.</small>
            </div>
            <div class="row mx-auto w-100">
                <div class="col-lg-8">
                    <div class="mx-auto text-center">
                        <strong>
                            Съществуващи категории:
                        </strong>
                    </div>
                    <div class="mx-auto">
                        <strong>
                            Категории:
                        </strong>
                    </div>
                    <div class="consult-content-form-categories mx-auto text-center w-100">
                        <div class="row w-100">
                            <div class="col-md-1 my-2">
                                <span>#</span>
                            </div>
                            <div class="col-md-7 my-2">
                                Категория
                            </div>
                            <div class="col-md-4 my-2">
                                Виж
                            </div>
                        @foreach($categories as $category)
                                <div class="col-md-1 my-1">
                                    <span>{{$category->id}}</span>
                                </div>
                                <div class="col-md-7 my-1">
                                    <span>{{$category->name}}</span>
                                </div>
                                <div class="col-md-4 my-1">
                                    <a href="{{route('consult-form-category-show', $category->id)}}">
                                        <button class="btn btn-success">Виж</button>
                                    </a>
                                </div>
                        @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 border-element">
                    <div class="mx-auto text-center">
                        <strong>
                            Добави нова категория:
                        </strong>
                    </div>
                    <div class="mx-auto text-center">
                        <form action="{{route('consult-form-category-store')}}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="mx-auto my-1">
                                <label for="category">
                                    Име:
                                </label>
                                <input value="{{ old('category') }}" id="category" name="category" class="form-control @error('category') is-invalid @enderror" required autocomplete="category">
                                @error('category')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="text-center mx-auto my-1">
                                <button class="btn btn-dark">
                                    Добави!
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
