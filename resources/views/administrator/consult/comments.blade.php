@extends('administrator.consult.consult-layout')
@section('cont')
    <div class="consult-content">
        <div class="text-center">
            <h2>
                Коментари и отзиви за
                <strong>
                    {{ $consultType->name }}
                </strong>
            </h2>
        </div>
        <div class="px-5 border-bottom"></div>

        <div class="w-100 my-5 px-2">
            @if($consultType->consultComments->count() < 1)
                <div class="m-5 p-5 text-center">
                    <h2>
                        <strong>
                            Все още няма добавени коментари и отзиви
                        </strong>
                    </h2>
                </div>
            @endif
            @foreach($comments as $comment)
                <div class="my-2 px-5 border">
                    <div class="d-flex justify-content-between px-5 align-items-center p-1 border-bottom">
                        <div class="text-center">
                            <h6>
                                От:
                                <strong>
                                    {{$comment->from}}
                                </strong>
                            </h6>
                        </div>
                        <div>
                            <h6>
                                На дата:
                                <strong>
                                    {{ $comment->created_at }}
                                </strong>
                            </h6>
                        </div>
                        <div>
                            <h6>
                                Вижда ли се -
                                @if($comment->is_visible)
                                    <strong>
                                        Да
                                    </strong>
                                @else
                                    <strong>
                                        Не
                                    </strong>
                                @endif
                            </h6>
                        </div>
                        <div>
                            @if($comment->is_visible)
                                <a href="{{ route('consult-comments-show-hide', [$comment->id, 'hide']) }}">
                                    <button class="btn btn-danger">
                                        Скрий
                                    </button>
                                </a>
                            @else
                                <a href="{{ route('consult-comments-show-hide', [$comment->id, 'show']) }}">
                                    <button class="btn btn-success">
                                        Покажи
                                    </button>
                                </a>
                            @endif

                        </div>
                    </div>
                    <div class="text-left">
                        @foreach(explode(PHP_EOL, $comment->message) as $paragraph)
                            <p>
                                {{ $paragraph }}
                            </p>
                        @endforeach
                    </div>
                </div>
            @endforeach
                <div class="w-100 mx-auto text-center p-5">
                    {{$comments->links()}}
                </div>
        </div>
    </div>
@endsection
