@extends('administrator.analyst.analyst-layout')
@section('cont')
    <div class="analyst-body">
        <div class="card my-2">
            <div class="card-header">
                @if($key === 'sum')
                    <strong>От началото</strong>
                @elseif($key === '1')
                    <strong>За последните 24 часа:</strong>
                @elseif($key === '7')
                    <strong>За последните 7 дни:</strong>
                @elseif($key === '30')
                    <strong>За последния 1 месец:</strong>
                @endif
                <br>
                <strong>Общо: {{ $counts->count() }}</strong>
            </div>
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <div>
                        <a href="{{ route('counter.index', ['sum']) }}">
                            <button class="btn btn-outline-secondary">
                                Покажи общо
                            </button>
                        </a>
                    </div>
                    <div>
                        <a href="{{ route('counter.index', [1]) }}">
                            <button class="btn btn-outline-secondary">
                                Покажи за 24 часа
                            </button>
                        </a>
                    </div>
                    <div>
                        <a href="{{ route('counter.index', [7]) }}">
                            <button class="btn btn-outline-secondary">
                                Покажи за 7 дни
                            </button>
                        </a>
                    </div>
                    <div>
                        <a href="{{ route('counter.index', [30]) }}">
                            <button class="btn btn-outline-secondary">
                                Покажи за 1 месец
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
