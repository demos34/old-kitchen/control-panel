@extends('administrator.analyst.analyst-layout')
@section('cont')
    <div class="analyst-body">
        <div class="card my-2">
            <div class="card-header">
                <strong>
                    {{ $type }}
                </strong>
                <br>
                <strong>Общо: {{ $counts->count() }}</strong>
            </div>
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <div>
                        <a href="{{ route('counter.by', [$countType->id, 'sum']) }}">
                            <button class="btn btn-outline-secondary">
                                Покажи общо
                            </button>
                        </a>
                    </div>
                    <div>
                        <a href="{{ route('counter.by', [$countType->id, 1]) }}">
                            <button class="btn btn-outline-secondary">
                                Покажи за 24 часа
                            </button>
                        </a>
                    </div>
                    <div>
                        <a href="{{ route('counter.by', [$countType->id, 7]) }}">
                            <button class="btn btn-outline-secondary">
                                Покажи за 7 дни
                            </button>
                        </a>
                    </div>
                    <div>
                        <a href="{{ route('counter.by', [$countType->id, 30]) }}">
                            <button class="btn btn-outline-secondary">
                                Покажи за 1 месец
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @if($mostViewed !== NULL)

        <div class="my-5">
            <div class="card my-3">
                <div class="card-header">
                    С най-много прегледи -
                    <strong>
                        {{ $countType->countables()->orderBy('counts', 'DESC')->first()->counts }}
                    </strong>
                </div>
                <div class="card-body my-2">
                    <div class="d-flex justify-content-around">
                        @if($countType->id === 2)
                            <div>
                                <div class="w-100 border-bottom text-center my-2">
                                    <strong>
                                        {{ $mostViewed->type }}
                                    </strong>
                                </div>
                                <div class="border-bottom">
                                    @foreach(explode(PHP_EOL,$mostViewed->description) as $typeRecipe)
                                        <p>
                                            {{ $typeRecipe }}
                                        </p>
                                    @endforeach
                                </div>
                                <div class="text-center border-top">
                                    <a href="{{ route('recipes.types.show', $mostViewed) }}">
                                        Виж
                                    </a>
                                </div>
                            </div>
                        @endif
                        @if($countType->id === 3)
                            <div>
                                <div class="w-100 border-bottom text-center my-2">
                                    <strong>
                                        {{ $mostViewed->title }}
                                    </strong>
                                </div>
                                <div class="border-bottom">
                                    <p>
                                        {{$mostViewed->subtitle}}
                                    </p>
                                </div>
                                <div class="text-center border-top">
                                    <a href="{{ route('recipes.recipes.show', $mostViewed->slug) }}">
                                        Виж
                                    </a>
                                </div>
                            </div>
                        @endif
                        @if($countType->id === 4)
                            <div>
                                <div class="w-100 border-bottom text-center my-2">
                                    <strong>
                                        {{ $mostViewed->title }}
                                    </strong>
                                </div>
                                <div class="border-bottom">
                                    <p>
                                        {{$mostViewed->description}}
                                    </p>
                                </div>
                                <div class="text-center border-top">
                                    <a href="{{ route('blog-delete', $mostViewed->slug) }}">
                                        Виж
                                    </a>
                                </div>
                            </div>
                        @endif
                        @if($countType->id === 5)
                            <div>
                                <div class="w-100 border-bottom text-center my-2">
                                    <strong>
                                        {{ $mostViewed->title }}
                                    </strong>
                                </div>
                                <div class="border-bottom">
                                    <p>
                                        {{$mostViewed->description}}
                                    </p>

                                    <div class="mb-3">
                                        <iframe style="width: 100%" height="auto" src="{{$mostViewed->tube_link}}"
                                                frameborder="0"
                                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                    </div>
                                </div>
                                <div class="text-center border-top">
                                    <a href="{{ route('tube-show', $mostViewed->slug) }}">
                                        Виж
                                    </a>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
@endsection
