@extends('administrator.analyst.analyst-layout')
@section('cont')
    <div class="analyst-body">
        <div class="card my-2">
            <div class="card-header">
                <strong>
                    {{ $type }}
                </strong>
                <br>
                От дата: <strong>{{$countableCustom->start_date}}</strong>
                <br>
                До дата: <strong>{{$countableCustom->end_date}}</strong>
                <br>
                @if($countableCustom->countableCustomRecords->count() > 0)
                    @if($countableCustom->count_type_id == 1)
                        <strong>Общо: {{ $countableCustom->countableCustomRecords->first()->counts }}</strong>
                    @else
                        <strong>Общо: {{ $countableCustom->countableCustomRecords->sum('counts') }}</strong>
                    @endif
                @else
                    <strong>
                        Няма записи за този период.
                    </strong>
                @endif
            </div>
        </div>

        <div class="my-5">
            <div class="card my-3">
                <div class="card-header">
                    С най-много прегледи -
                    <strong>
                        {{ $countableCustom->countableCustomRecords()->orderBy('counts', 'DESC')->first()->counts }}
                    </strong>
                </div>
                <div class="card-body my-2">
                    <div class="d-flex justify-content-around">
                        @if($countableCustom->count_type_id === 2)
                            <div>
                                <div class="w-100 border-bottom text-center my-2">
                                    <strong>
                                        {{ $mostViewed->type }}
                                    </strong>
                                </div>
                                <div class="border-bottom">
                                    @foreach(explode(PHP_EOL,$mostViewed->description) as $typeRecipe)
                                        <p>
                                            {{ $typeRecipe }}
                                        </p>
                                    @endforeach
                                </div>
                                <div class="text-center border-top">
                                    <a href="{{ route('recipes.types.show', $mostViewed) }}">
                                        Виж
                                    </a>
                                </div>
                            </div>
                        @endif
                        @if($countableCustom->count_type_id === 3)
                            <div>
                                <div class="w-100 border-bottom text-center my-2">
                                    <strong>
                                        {{ $mostViewed->title }}
                                    </strong>
                                </div>
                                <div class="border-bottom">
                                    <p>
                                        {{$mostViewed->subtitle}}
                                    </p>
                                </div>
                                <div class="text-center border-top">
                                    <a href="{{ route('recipes.recipes.show', $mostViewed->slug) }}">
                                        Виж
                                    </a>
                                </div>
                            </div>
                        @endif
                        @if($countableCustom->count_type_id === 4)
                            <div>
                                <div class="w-100 border-bottom text-center my-2">
                                    <strong>
                                        {{ $mostViewed->title }}
                                    </strong>
                                </div>
                                <div class="border-bottom">
                                    <p>
                                        {{$mostViewed->description}}
                                    </p>
                                </div>
                                <div class="text-center border-top">
                                    <a href="{{ route('blog-delete', $mostViewed->slug) }}">
                                        Виж
                                    </a>
                                </div>
                            </div>
                        @endif
                        @if($countableCustom->count_type_id === 5)
                            <div>
                                <div class="w-100 border-bottom text-center my-2">
                                    <strong>
                                        {{ $mostViewed->title }}
                                    </strong>
                                </div>
                                <div class="border-bottom">
                                    <p>
                                        {{$mostViewed->description}}
                                    </p>

                                    <div class="mb-3">
                                        <iframe style="width: 100%" height="auto" src="{{$mostViewed->tube_link}}"
                                                frameborder="0"
                                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                    </div>
                                </div>
                                <div class="text-center border-top">
                                    <a href="{{ route('tube-show', $mostViewed->slug) }}">
                                        Виж
                                    </a>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
