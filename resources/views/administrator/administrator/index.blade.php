@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" align="center">
                    Административен контролен панел
                </div>
            </div>
            <br>
            <div class="card">
                <div class="card-header" align="center"><a href="{{ route('home.home.index') }}">Начална страница</a></div>
            </div>
            <br>
            <div class="card">
                <div class="card-header" align="center"><a href="{{ route('users.users.index') }}">Потребители</a></div>
            </div>
            <br>
            <div class="card">
                <div class="card-header" align="center"><a href="{{ route('roles.roles.index') }}">Роли</a></div>
            </div>
            <br>
            <div class="card">
                <div class="card-header" align="center"><a href="{{ route('ratings.ratings.index') }}">Рейтинги</a></div>
            </div>
            <br>
            <div class="card">
                <div class="card-header" align="center"><a href="{{ route('books.books.create') }}">Книги</a></div>
            </div>
            <br>
            <div class="card">
                <div class="card-header" align="center"><a href="{{ route('books.buy.create') }}">Издатели</a></div>
            </div>
            <br>
            <div class="card">
                <div class="card-header" align="center"><a href="{{ route('contacts', 1) }}">За контакт с нас</a></div>
            </div>
        </div>
    </div>
</div>
@endsection
