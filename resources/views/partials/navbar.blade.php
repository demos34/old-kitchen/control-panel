<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm" style="border-bottom: 2px solid red">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <div class="d-flex justify-content-between">
                <div class="mx-1">

                    <img src="{{ $homeInfo->logo_image_url }}" style="max-height: 60px;">
                </div>
                <div class="my-auto mx-1">
                    {{ config('app.name', 'Старата кухня') }}
                </div>
            </div>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>


        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            @can('administration')
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            Рецепти <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">
                            @if($types->count()>0)
                                @foreach($types as $type)
                                    <a class="dropdown-item" href="{{ route('recipes.types.show', $type->slug) }}">
                                        {{ $type->type }}
                                    </a>
                                @endforeach
                            @endif
                            <a class="dropdown-item" href="{{ route('recipes.types.index') }}">
                                Виж всички категории
                            </a>
                            <a class="dropdown-item" href="{{ route('recipes.recipes.index') }}">
                                Виж всички рецепти
                            </a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            Драфт <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">
                            @if($types->count()>0)
                                @foreach($types as $type)
                                    <a class="dropdown-item" href="{{ route('drafts.types.show', $type->slug) }}">
                                        {{ $type->type }}
                                    </a>
                                @endforeach
                            @endif
                            <a class="dropdown-item" href="{{ route('drafts.recipes.index') }}">
                                Виж всички драфт-рецепти (непубликувани)
                            </a>
                            <a class="dropdown-item" href="{{ route('drafts.recipes.create') }}">
                                Добави драфт-рецепта (непубликувана)
                            </a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown"
                           class="nav-link dropdown-toggle"
                           href="#" role="button"
                           data-toggle="dropdown"
                           aria-haspopup="true"
                           aria-expanded="false" v-pre
                           @if($unread->count()> 0)
                           style="background: green"
                            @endif
                        >
                            Съобщения
                            @if($unread->count()> 0)
                                {{$unread->count()}}
                            @endif
                            <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{route('contacts', 2)}}"
                               @if($contactType->count()>0)
                               style="background: greenyellow"
                                @endif
                            >
                                От контакти
                                @if($contactType->count()>0)
                                    <span style="
                                    background: darkgreen;
                                    color: whitesmoke;
                                    border-radius: 50%;
                                    font-size: 0.85em;
">
                                        {{$contactType->count()}}</span>
                                @endif
                            </a>

                            <a class="dropdown-item" href="{{route('contacts', 1)}}"
                               @if($buyType->count()>0)
                               style="background: greenyellow"
                                @endif
                            >
                                За закупуване на книги
                                @if($buyType->count()>0)
                                    <span style="
                                    background: darkgreen;
                                    color: whitesmoke;
                                    border-radius: 50%;
                                    font-size: 0.85em;
">
                                        {{$buyType->count()}}</span>
                                @endif
                            </a>
                        </div>
                    </li>
                    <li class="font-weight-bold font-italic" style="font-size: 16px">
                        <a class="nav-link" href="{{route('books.books.index')}}" role="button">
                            Проекти
                        </a>
                    </li>
                    <li class="font-weight-bold font-italic" style="font-size: 16px">
                        <a class="nav-link" href="{{route('partners')}}" role="button">
                            Партньори
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            Знания <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('blog') }}">
                                Блог
                            </a>
                            <a class="dropdown-item" href="{{ route('youtube') }}">
                                Youtube канал
                            </a>
                        </div>
                    </li>
                    {{--                    <li class="font-weight-bold font-italic" style="font-size: 16px">--}}
                    {{--                        <a class="nav-link" href="{{route('blog')}}" role="button">--}}
                    {{--                            Блог--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}
                </ul>
        @endcan

        <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            {{--<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
                        </li>
                    @endif
                @else

                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            Анализатор <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('analyst.index') }}">
                                Влез в менюто
                            </a>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>

                            {{ Auth::user()->username }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('consult-index') }}">
                                Консултации
                                @if($unreadConsult->count()>0)
                                    <div id="unread-consult">{{$unreadConsult->count()}}</div>
                                @endif
                            </a>
                            <a class="dropdown-item" href="{{ route('users.profile.index') }}">
                                Профил
                            </a>
                            <a class="dropdown-item" href="{{ route('tags.tags.index') }}">
                                Мета тагове - панел!
                            </a>
                            <a class="dropdown-item" href="{{ route('tags.keywords.index') }}">
                                Ключови думи - панел!
                            </a>
                            <a class="dropdown-item" href="{{ route('administrator.administrator.index') }}">
                                Админ панел за управление!
                            </a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                Изход от сайта
                            </a>


                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
