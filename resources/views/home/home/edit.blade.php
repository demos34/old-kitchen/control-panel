@extends('layouts.app')

@section('content')
<form action="{{ route('home.home.update', 1) }}" method="POST" enctype="multipart/form-data">
    @csrf
    {{ method_field('PATCH') }}
        <div class="row mb-5" id="logo" align="center">
            <div class="col-md-4">
                <img src="{{ $homeInfo->logo_image_url }}" style="max-height: 200px; max-width: 90%">
            </div>
            <div class="col-md-8">
                <label for="logo_image_url" class="col-md-4 col-form-label text-md-right">Лого:</label>
                <div>
                    <input type="file" class="" id="logo_image_url" name="logo_image_url">
                    @if($errors->has('logo_image_url'))
                        <strong>{{ $errors->first('logo_image_url') }}</strong>
                    @endif
                </div>
            </div>
        </div>
        <hr>
        <div class="row mb-5" id="top-image" align="center">
            <div class="col-md-4">
                <img src="{{ $homeInfo->top_image_url }}" class="mx-auto" style="max-height: 600px; width: 90%">
            </div>
            <div class="col-md-8">
                <div>
                    <div>
                        <label for="top_home_text" class="text-md-right">Текст в изображението:</label>
                        <h5>
                        <textarea id="top_home_text"
                               type="text"
                               class="form-control @error('top_home_text') is-invalid @enderror"
                               name="top_home_text"
                               required autocomplete="top_home_text" autofocus>@if(old('top_home_text')){{ old('top_home_text') }} @else{{ $homeInfo->top_home_text }}@endif</textarea>
                        @error('top_home_text')
                        <br>
                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                            @enderror
                        </h5>
                    </div>
                    <hr>
                    <label for="top_image_url">Изображение  (не трябва да е по-голям от 10мб като големина и по-малък от 1200px като ширина!):</label>
                    <div>
                        <input type="file" class="" id="top_image_url" name="top_image_url">
                        @if($errors->has('top_image_url'))
                            <strong>{{ $errors->first('top_image_url') }}</strong>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="m-5">
                <div class="my-5" align="center">
                    <img src="/storage/home/about-me.png" style="width: 15%">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <img src="{{$homeInfo->middle_image_url}}" style="width: 90%">
                </div>
                <div class="col-md-9" align="center">
                    <div>
                        <label for="middle_home_text" class="text-md-right">Текст до изображението:</label>
                        <h5>
                        <textarea id="middle_home_text"
                                  type="text"
                                  class="form-control @error('middle_home_text') is-invalid @enderror"
                                  name="middle_home_text"
                                  required autocomplete="top_home_text"
                                  style="height: 300px"
                                  autofocus>@if(old('middle_home_text')){{ old('middle_home_text') }} @else{{$homeInfo->middle_home_text}}@endif</textarea>
                            @error('middle_home_text')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                            @enderror
                        </h5>
                    </div>
                    <hr>
                    <label for="middle_image_url" class="col-md-4 col-form-label text-md-right">Изображение:</label>
                    <div>
                        <input type="file" class="" id="middle_image_url" name="middle_image_url">
                        @if($errors->has('middle_image_url'))
                            <strong>{{ $errors->first('middle_image_url') }}</strong>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="m-5">
                <div class="my-5" align="center">
                    <img src="/storage/home/last-recipes.png" style="width: 35%">
                </div>
            </div>
            <div class="row my-5 justify-content-center">
                <div class="col-md-5" align="center">
                    <div class="card">
                        <div class="card-header">
                            <label for="recipes_count" class="text-md-right">Брой рецепти, които се виждат:</label>
                        </div>
                            <h5>
                                <input id="recipes_count"
                                       type="text"
                                       class="form-control @error('recipes_count') is-invalid @enderror"
                                       name="recipes_count"
                                       align="center"
                                       value="@if(old('recipes_count')){{ old('recipes_count') }}@else{{$homeInfo->recipes_count}}@endif"
                                       required autocomplete="recipes_count" autofocus>
                                @error('recipes_count')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                @enderror
                            </h5>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    <div class="d-flex justify-content-center">
        <button type="submit" class="btn btn-outline-success" style="width: 25%; height: 100px; font-size: 20px">
            Edit
        </button>
    </div>
</form>
@endsection
