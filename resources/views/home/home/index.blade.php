@extends('layouts.app')

@section('meta')
    <meta name="description" content="{{$homeInfo->description}}">
    <meta name="keywords"
          content="@foreach($tags as $tag) @if($lastTag->name == $tag->name){{$tag->name}}@else{{$tag->name}}, @endif @endforeach">
    <meta name="author" content="Димитър Митко">
@endsection

@section('content')
    @if(Auth::check())
        <div class="container" align="center">
            <div class="alert alert-warning" role="alert" style="font-size: 24px">
                Така изглежда заглавната страница. Под всеки, който може да се подмени, с <i>наклонени букви</i>, ще бъде
                изписано.
                <br>
                Елементите могат да се подменят от бутон:
            </div>
            <a href="{{ route('home.home.edit', 1) }}">
                <button class="btn btn-outline-warning" style="color: black">Промени</button>
            </a>
            <hr>
            <p class="font-italic font-weight-bold">
                <u>
                    Забележки:
                </u>
            </p>
            <p class="font-italic font-weight-bold">
                1. Също така може да се промени и логото, което се намира в горния ляв ъгъл!
            </p>
            <p class="font-italic font-weight-bold">
                2. Ще може да се промени и броят на последните рецепти, които са показани в най-долната част на страницата,
                като трябва да се спазва правилото да е кратно на 3!
            </p>
            <p class="font-italic font-weight-bold">
                3. Не могат да се променят големината, видът и цвета на шрифта!
            </p>
            <p class="font-italic font-weight-bold">
                4. В основния сайт, изгледът започва от тук надолу, като нагоре не се показва!
            </p>
            <hr>
        </div>
        <div class="row mb-5" id="top-image" align="center" style="position: relative; display: inline-block">
            <div class="col-md-12">
                <div class="m 0 auto">
                    <img src="{{$homeInfo->top_image_url}}" class="mx-auto" style="width: 90%">
                </div>
                <div style="
        position: absolute;
            z-index: 999;
            margin: 0 auto;
            left: 50%; right: 0;
            top: 70%;
            width: 40%;
            color: whitesmoke;
            font-size: 36px;">
                    <p>
                        <b>
                            <i>
                                {{$homeInfo->top_home_text}}
                            </i>
                        </b>
                </div>
            </div>
        </div>
        <div class="container">
            <div align="center" style="font-size: 22px">
                <p>
                    <i>
                        <u>
                            Могат да се променят и изображението, и текстът!
                        </u>
                    </i>
                </p>
            </div>
        </div>
        <div class="container">
            <div class="m-5">
                <div class="my-5" align="center">
                    <img src="/storage/home/about-me.png" style="width: 15%">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 offset-2">
                    <img src="{{$homeInfo->middle_image_url}}" style="width: 100%">
                </div>
                <div class="col-md-5 offset-1" style="width: 75%;" align="justify">
                    <p>
                        {{$homeInfo->middle_home_text}}
                    </p>
                </div>
            </div>
        </div>
        <div class="container">
            <div align="center" style="font-size: 22px">
                <p>
                    <i>
                        <u>
                            Могат да се променят и изображението и текстът!
                        </u>
                    </i>
                </p>
            </div>
        </div>
        <div class="container">
            <div class="m-5">
                <div class="my-5" align="center">
                    <img src="/storage/home/last-recipes.png" style="width: 35%">
                </div>
            </div>
            <div class="row my-5">
                @foreach($lastRecipes as $recipe)
                    <div class="col-md-3 offset-1">
                        <div>
                            <a href="{{route('recipes.recipes.show', $recipe->slug)}}">
                                <img class="index-last-recipe-image"
                                     src="{{implode(',', $recipe->images()->get()->pluck('url')->toArray())}}"
                                     alt="index-last-recipe">
                            </a>
                        </div>
                        <div>
                            <h1 align="center" class="font-italic font-weight-bold">
                                {{Str::limit($recipe->title, 10)}}
                            </h1>
                        </div>
                        <div style="max-height: 100px; overflow: hidden" align="center">
                            <p class="font-italic">
                                {{Str::limit($recipe->how_to, 60)}}
                            </p>
                        </div>
                        <div class="my-5" align="center">
                            <p>
                                <a style="color: orange; text-transform: uppercase; text-decoration: none"
                                   href="{{route('recipes.recipes.show', $recipe->slug)}}" class="font-weight-bold">
                                    повече
                                </a>
                            </p>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="d-flex justify-content-center my-5">
                <div align="center">
                    <a href="{{route('recipes.recipes.index')}}">
                        <button type="button" class="btn"
                                style="border-radius: 25px;
                        background-color: orangered;
                        text-transform: uppercase;
                        height: 40px;
                        width: 200px;
                        color: whitesmoke">
                            виж всички
                        </button>
                    </a>
                </div>
            </div>
        </div>
    @endif
@endsection
