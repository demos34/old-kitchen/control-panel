@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div>
                    <a href="{{redirect()->back()}}">
                        <button class="btn btn-outline-dark">Back</button>
                    </a>
                </div>
                <div class="card my-3 mx-auto">
                    <div class="card-header">
                        <h4 class="text-center">
                            <strong>Партньор {{$partner->title}}:</strong>
                        </h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>slug</th>
                                <th>Image</th>
                            </tr>
                            </thead>
                            <tbody>
                                    <tr>
                                        <td>
                                            {{$partner->id}}
                                        </td>
                                        <td>
                                            <strong>
                                                {{$partner->title}}
                                            </strong>
                                        </td>
                                        <td>
                                            {{$partner->slug}}
                                        </td>
                                        <td>
                                            <img src="{{$partner->image}}" style="height: 2em; width: 2em" class="rounded-circle" alt="">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="3" class="text-center">Description</th>
                                        <th>URL</th>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="text-justify">
                                            {{$partner->description}}
                                        </td>
                                        <td>
                                            {{$partner->url}}
                                        </td>
                                    </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4 my-3">
                <div class="well">
                    <h4 class="text-center">
                        <label>
                            Промени <strong>{{$partner->title}}</strong>
                        </label>
                    </h4>
                    <form action="{{route('partners-update', $partner->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="card">
                            <div class="card-body">
                                <label for="title">
                                    Title:
                                </label>
                                    <input id="title"
                                           type="text"
                                           class="form-control @error('title') is-invalid @enderror"
                                           name="title"
                                           value=@if(old('title'))"{{ old('title') }}"@else"{{$partner->title}}"@endif
                                    required autocomplete="title" autofocus>
                                    @error('title')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                            </div>
                            <div class="card-body">
                                <label for="slug">
                                    Slug:
                                </label>
                                    <input id="slug"
                                           type="text"
                                           class="form-control @error('slug') is-invalid @enderror"
                                           name="slug"
                                           value=@if(old('slug'))"{{ old('slug') }}"@else"{{$partner->slug}}"@endif
                                    required autocomplete="slug" autofocus>
                                    @error('slug')
                                    <br>
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                            </div>
                            <div class="card-body">
                                <label for="url">
                                    URL:
                                </label>
                                <input id="url"
                                       type="text"
                                       class="form-control @error('url') is-invalid @enderror"
                                       name="url"
                                       value=@if(old('url'))"{{ old('url') }}"@else"{{$partner->url}}"@endif
                                required autocomplete="url" autofocus>
                                @error('url')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="card-body">
                                <label for="description">
                                    Description:
                                </label>
                                <textarea id="description"
                                          type="text"
                                          class="form-control"
                                          name="description">@if(old('description')){{ old('description') }}@else{{$partner->description}}@endif</textarea>
                                @error('description')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                @enderror
                            </div>
                            <div class="form-group row">
                                <label for="image" class="col-md-4 col-form-label text-md-right">
                                    Image <img src="{{$partner->image}}" style="height: 15px; width: 15px" class="rounded-circle" alt="">
                                </label>
                                <div class="col-md-6">
                                    <input type="file" class="form-control-file" id="image" name="image">
                                    @if($errors->has('image'))
                                        <strong>{{ $errors->first('image') }}</strong>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center m-2">
                            <button type="submit" class="btn btn-outline-primary">
                                Update
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

