@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card my-3 mx-auto">
                                <div class="card-header">
                                    <h4>
                                        Партньори:
                                    </h4>
                                </div>
                                <div class="card-body">
                                    <table class="table table-borderless">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Image</th>
                                                <th>URL</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($partners as $partner)
                                            <tr>
                                                <td>
                                                    {{$partner->id}}
                                                </td>
                                                <td>
                                                    {{$partner->title}}
                                                </td>
                                                <td>
                                                    <img src="{{$partner->image}}" alt="{{$partner->image}}" style="max-width: 2em">
                                                </td>
                                                <td>
                                                    {{$partner->url}}
                                                </td>
                                                <td>
                                                    <a href="{{route('partners-show', $partner->id)}}"><button type="button" class="btn btn-dark float-left">Промени</button></a>
                                                    <form action="{{route('partners-destroy', $partner->id)}}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger float-left" onclick="alert('Сигурни ли сте?')">Изтрий</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 my-3">
                            <div class="well">
                                <h4 class="text-center">
                                    <label>
                                        Добави партньор
                                    </label>
                                </h4>
                                <form action="{{route('partners-post')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="card">
                                        <div class="card-body">
                                            <label for="title">
                                                Name:
                                            </label>
                                            <input id="title"
                                                   type="text"
                                                   class="form-control @error('title') is-invalid @enderror"
                                                   name="title"
                                                   value="{{ old('title') }}"
                                                   required autocomplete="name" autofocus>
                                            @error('title')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="card-body">
                                            <label for="slug">
                                                Slug:
                                            </label>
                                            <input id="slug"
                                                   type="text"
                                                   class="form-control @error('slug') is-invalid @enderror"
                                                   name="slug"
                                                   value="{{ old('slug') }}"
                                                   required autocomplete="name" autofocus>
                                            @error('slug')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="card-body">
                                            <label for="url">
                                                URL:
                                            </label>
                                            <input id="url"
                                                   type="text"
                                                   class="form-control @error('url') is-invalid @enderror"
                                                   name="url"
                                                   value="{{ old('url') }}">
                                            @error('url')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="card-body">
                                            <label for="description">
                                                Description:
                                            </label>
                                            <textarea id="description"
                                                   type="text"
                                                   class="form-control"
                                                   name="description">{{ old('description') }}</textarea>
                                            @error('description')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group row">
                                            <label for="image" class="col-md-4 col-form-label text-md-right">Image</label>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control-file" id="image" name="image">
                                                @if($errors->has('image'))
                                                    <strong>{{ $errors->first('image') }}</strong>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center m-2">
                                        <button type="submit" class="btn btn-dark">
                                            Добави
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
