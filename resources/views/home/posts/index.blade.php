@extends('layouts.app')

@section('style')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container my-5">
        <div class="row w-100">
            <div class="col-md-6">
                <div class="row w-100">
                    @foreach($posts as $post)
                        <div class="col-md-4 border">
                            <div class="w-100 text-center">
                                <strong>
                                    {{$post->title}}
                                </strong>
                            </div>
                            <div class="w-100 text-center">
                                <span>
                                    {{$post->subtitle}}
                                </span>
                            </div>
                            <div class="mb-3">
                                <img class="w-100" src="{{$post->image}}" alt="{{$post->image}}">
                            </div>
                            <div>
                                {{Str::limit($post->body, 75)}}
                            </div>
                            <div class="text-right">
                                <a href="{{route('blog-show', $post->slug)}}">
                                    more...
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Добави нов пост</div>
                    <div class="card-body">
                        <form method="POST" action="{{route('blog-store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="title" class="col-md-4 col-form-label text-md-right">Заглавие</label>
                                <div class="col-md-6">
                                    <input id="title"
                                           type="text"
                                           class="form-control @error('title') is-invalid @enderror"
                                           name="title" value="{{ old('title') }}"
                                           required autocomplete="title" autofocus>

                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="subtitle" class="col-md-4 col-form-label text-md-right">Подзаглавие</label>
                                <div class="col-md-6">
                                    <input id="subtitle"
                                           type="text"
                                           class="form-control @error('subtitle') is-invalid @enderror"
                                           name="subtitle" value="{{ old('subtitle') }}"
                                           required autocomplete="subtitle" autofocus>

                                    @error('subtitle')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row text-center">
                                <label for="body" class="col-md-12 col-form-label text-center">Пост</label>
                                <div class="col-md-12">
                                    <textarea id="body"
                                           type="text"
                                           class="form-control @error('body') is-invalid @enderror"
                                           name="body"
                                              style="width: 100%; height: 15em"
                                              required autocomplete="body" autofocus>{{ old('body') }}</textarea>
                                    @error('body')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row text-center">
                                <label for="keywords" class="col-md-12">
                                    Ключови думи:
                                </label>
                                <select id="keywords" class="col-md-12 js-multiple" name="keywords[]"
                                        multiple="multiple">
                                    @foreach($keywords as $keyword)
                                        <option value="{{$keyword->id}}">{{$keyword->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <hr>
                            <div class="form-group row text-center">
                                <label for="tags" class="col-md-12">
                                    Тагове:
                                </label>
                                <select id="tags" class="col-md-12 js-multiple" name="tags[]" multiple="multiple">
                                    @foreach($tags as $tag)
                                        <option value="{{$tag->id}}">{{$tag->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Кратко
                                    описание</label>
                                <div class="col-md-6">
                                    <input id="description"
                                           type="text"
                                           class="form-control @error('description') is-invalid @enderror"
                                           name="description" value="{{ old('description') }}"
                                           required autocomplete="description" autofocus>

                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="image" class="col-md-4 col-form-label text-md-right">Снимка</label>
                                <div class="col-md-6">
                                    <input type="file" class="form-control-file" id="image" name="image">
                                    @if($errors->has('image'))
                                        <strong>{{ $errors->first('image') }}</strong>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4" align="center">
                                    <button type="submit" class="btn btn-primary">
                                        Добави
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.js-multiple').select2();
        });
    </script>
@endsection
