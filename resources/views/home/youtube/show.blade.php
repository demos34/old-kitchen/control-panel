@extends('layouts.app')

@section('style')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container my-5">
        <div class="row w-100">
            <div class="col-md-6 text-center border">
                <div class="w-100 text-center">
                    <strong>
                        {{$post->title}}
                    </strong>
                </div>
                <div class="w-100 text-center">
                    <strong>
                        {{$post->subtitle}}
                    </strong>
                </div>
                <div class="float-right">
                    <strong class="mr-5">
                        Delete
                    </strong>
                    <form action="{{route('tube-delete', $post->slug)}}" method="post" class="float-right">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" onclick="return confirm('Are you sure?')">
                            &times;
                        </button>
                    </form>
                </div>
                <div class="mb-3 mt-1 w-100">
                    <iframe style="width: 100%; height: 20em;" src="{{$post->tube_link}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="row w-100 mx-auto my-2 border">
                    <div class="col-sm-6 text-center">
                        <span>
                            Date: {{$post->created_at}}
                        </span>
                    </div>
                    <div class="col-sm-6 text-center">
                        <span>
                            Author: {{$post->user->username}}
                        </span>
                    </div>
                </div>
                <div class="text-justify">
                    @foreach(explode(PHP_EOL, $post->body) as $paragraph)
                        <p>
                            {{$paragraph}}
                        </p>
                    @endforeach
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Добави нов пост</div>
                    <div class="card-body">
                        <form method="POST" action="{{route('tube-update', $post->slug)}}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group row">
                                <label for="title" class="col-md-4 col-form-label text-md-right">Заглавие</label>
                                <div class="col-md-6">
                                    <input id="title"
                                           type="text"
                                           class="form-control @error('title') is-invalid @enderror"
                                           name="title" value="@if(old('title')){{ old('title') }}@else{{$post->title}}@endif"
                                           required autocomplete="title" autofocus>

                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="subtitle" class="col-md-4 col-form-label text-md-right">Подзаглавие</label>
                                <div class="col-md-6">
                                    <input id="subtitle"
                                           type="text"
                                           class="form-control @error('subtitle') is-invalid @enderror"
                                           name="subtitle" value="@if(old('subtitle')){{ old('subtitle') }}@else{{$post->subtitle}}@endif"
                                           required autocomplete="subtitle" autofocus>

                                    @error('subtitle')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row text-center">
                                <label for="body" class="col-md-12 col-form-label text-center">Пост</label>
                                <div class="col-md-12">
                                    <textarea id="body"
                                           type="text"
                                           class="form-control @error('body') is-invalid @enderror"
                                           name="body"
                                              style="width: 100%; height: 15em"
                                              required autocomplete="body" autofocus>@if(old('body')){{ old('body') }}@else{{$post->body}}@endif</textarea>
                                    @error('body')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row text-center">
                                <label for="keywords" class="col-md-12">
                                    Ключови думи:
                                </label>
                                <select id="keywords" class="col-md-12 js-multiple-keywords" name="keywords[]"
                                        multiple="multiple">
                                    @foreach($keywords as $keyword)
                                        <option value="{{$keyword->id}}">{{$keyword->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <hr>
                            <div class="form-group row" align="center">
                                <label for="tags" class="col-md-12">
                                    Тагове:
                                </label>
                                <select id="tags" class="col-md-12 js-multiple" name="tags[]" multiple="multiple">
                                    @foreach($tags as $tag)
                                        <option value="{{$tag->id}}">{{$tag->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Кратко
                                    описание</label>
                                <div class="col-md-6">
                                    <input id="description"
                                           type="text"
                                           class="form-control @error('description') is-invalid @enderror"
                                           name="description" value="@if(old('description')){{ old('description') }}@else{{$post->description}}@endif"
                                           required autocomplete="description" autofocus>

                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="tube_link" class="col-md-4 col-form-label text-md-right">Линк към Youtube видео</label>
                                <div class="col-md-6">
                                    <input id="tube_link"
                                           type="text"
                                           class="form-control @error('tube_link') is-invalid @enderror"
                                           name="tube_link" value="@if(old('tube_link')){{ old('tube_link') }}@else{{$post->tube_link}}@endif"
                                           required autocomplete="tube_link" autofocus>

                                    @error('tube_link')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4" align="center">
                                    <button type="submit" class="btn btn-primary">
                                        Добави
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.js-multiple').select2();
        });
        $('.js-multiple')
            .select2()
            .val({!! json_encode($post->tags()->get()->pluck('id')->toArray()) !!}).trigger('change');

        $(document).ready(function() {
            $('.js-multiple-keywords').select2();
        });
        $('.js-multiple-keywords')
            .select2().
        val({!! json_encode($post->keywords()->get()->pluck('id')->toArray()) !!}).trigger('change');
        </script>
@endsection
