@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row m-5">
        @foreach($drafts as $draft)
            <div class="col-md-10 m-3">
                <div class="row">
                    <div class="col-md-4">
                        <img src="{{implode(',',$draft->images->pluck('url')->toArray())}}" style="width: 100%; max-height: 300px" class="my-auto">
                        <div>
                            <small>
                                Снимка: {{ implode(', ',$draft->images->pluck('author')->toArray()) }}
                            </small>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <h5 class="mx-5">
                            {{$draft->title}}
                        </h5>
                        <hr>
                        <div class="font-italic" style="font-size: 10px">
                            Автор: {{ Auth::user()->username }}, на: {{ $draft->created_at }}
                        </div>
                        <hr>
                        <div class="font-weight-bold">
                            {{ $draft->subtitle }}
                        </div>
                        <div class="font-weight-normal" style="overflow: hidden; max-height: 100px; font-size: 11px">
                            {{ $draft->how_to }}
                        </div>
                    </div>
                </div>
                <hr>
                <div class="d-flex justify-content-center">
                    <div class="font-italic">
                        <a href="{{ route('drafts.recipes.show', $draft->slug) }}">
                            Още
                        </a>
                    </div>
                </div>
                <hr>
            </div>
        @endforeach
        <div class="row">
            <div class="col-md-4 offset-8">
                {{ $drafts->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
