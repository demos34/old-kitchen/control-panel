@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row m-5">
            <div class="col-md-12 m-3">
                <h3 class="font-weight-bold" align="center">
                    {{$draft->title}}
                </h3>
                <hr>
                <div class="row">
                    <div class="col-md-5">
                        <img src="{{implode(',',$draft->images->pluck('url')->toArray())}}" style="width: 100%; max-height: 500px" class="my-auto">
                        <div>
                            <small>
                                Снимка: {{ implode(', ',$draft->images->pluck('author')->toArray()) }}
                            </small>
                        </div>
                    </div>
                    <div class="col-md-7">

                        <div class="font-italic" style="font-size: 10px">
                            Автор: {{ Auth::user()->username }}, на: {{ $draft->created_at }}
                        </div>
                        <div class="font-italic" style="font-size: 10px">
                            @if($draft->isReadyToPublish->is_ready_to_publish == 0)
                                Статус(подготвена за публикация): <b style="color: red"> НЕ Е ГОТОВА!</b>
                            @else
                                Статус(публикувана): <b style="color: green">ГОТОВА Е!</b>
                            @endif
                        </div>
                        <div class="font-italic" style="font-size: 10px">
                            @if($draft->publishedDraft->is_published == 0)
                                Статус(публикувана): <b style="color: red">НЕПУБЛИКУВАНА!</b>
                            @else
                                Статус(публикувана): <b style="color: green">ПУБЛИКУВАНА!</b>
                            @endif
                        </div>
                        <hr>
                        <div class="font-weight-bold">
                            {{ $draft->subtitle }}
                        </div>
                        <div class="my-auto">
                            В категории:
                            @foreach($draft->types as $type)
                                <a href="" style="text-decoration: none; color: black">
                                    <span style="background-color: gray; border-radius: 3px; margin: 4px">
                                        {{$type->type}}
                                </span>
                                </a>
                            @endforeach
                        </div>
                        <div class="my-auto">
                            Тагове:
                                @foreach($draft->tags as $tag)
                                    <a href="" style="text-decoration: none; color: black">
                                        <span style="background-color: gray; border-radius: 3px; margin: 4px">
                                        {{$tag->name}}
                                    </span>
                                    </a>
                                @endforeach
                        </div>
                        <div class="my-1 py-2 mx-auto">
                            <div>
                                <label class="font-weight-bold">
                                    Управление:
                                </label>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <a href="{{ route('drafts.recipes.edit', $draft->id) }}">
                                        <button type="button" class="btn btn-dark">Редактирай</button>
                                    </a>
                                </div>
                                <div class="col-sm-3">
                                    <form action="{{ route('drafts.recipes.destroy', $draft->id) }}" method="POST">
                                        @csrf
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="btn btn-outline-danger">Изтрий</button>
                                    </form>
                                </div>
                                @if($draft->isReadyToPublish->is_ready_to_publish == 0)
                                    <div class="col-sm-3">
                                        <form action="{{route('drafts.preparing.update', $draft->id)}}" method="POST">
                                            @csrf
                                            @method('PATCH')
                                            <input name="set" type="hidden" value="{{ (int)1 }}">
                                            <button type="submit" class="btn btn-success">Готова е</button>
                                        </form>
                                    </div>
                                    <div class="col-sm-3">
                                        <form action="{{route('drafts.publish.update', $draft->id)}}" method="POST">
                                            @csrf
                                            @method('PATCH')
                                            <input name="set" type="hidden" value="{{ (int)1 }}">
                                            <button type="submit" class="btn btn-success" disabled>Публикувай</button>
                                        </form>
                                    </div>
                                @else
                                    <div class="col-sm-3">
                                        <form action="{{route('drafts.preparing.update', $draft->id)}}" method="POST">
                                            @csrf
                                            @method('PATCH')
                                                <input name="set" type="hidden" value="{{ (int)0 }}">
                                                <button type="submit" class="btn btn-danger">Не е готова</button>
                                        </form>
                                    </div>
                                    <div class="col-sm-3">
                                        <form action="{{route('drafts.publish.update', $draft->id)}}" method="POST">
                                            @csrf
                                            @method('PATCH')
                                            <input name="set" type="hidden"
                                               @if($draft->publishedDraft->is_published == 0)
                                                   value="{{ (int)1 }}"
                                               @else
                                                   value="{{ (int)0 }}"
                                               @endif

                                            >
                                            @if($draft->publishedDraft->is_published == 0)
                                                <button type="submit" class="btn btn-success">Публикувай</button>
                                            @else
                                                <button type="submit" class="btn btn-danger">Спри от публикация</button>
                                            @endif
                                        </form>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="my-5 row" style="background-color: whitesmoke; border-radius: 5px">
                    <div class="col-md-7 offset-1 my-2">
                        <ul>
                            Необходими продукти:
                            @foreach($draft->draftProducts as $product)
                                <li>
                                    {{ $product->product }};
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-md-4 my-2">
                        Необходимо време за приготвяне на рецептата:
                        <div class="font-weight-bold font-italic">
                        @if($draft->timeToCook->days > 0)
                            @if($draft->timeToCook->days < 2)
                                {{$draft->timeToCook->days}} ден
                            @else
                                {{$draft->timeToCook->days}} дни
                            @endif
                        @endif
                        @if($draft->timeToCook->hours > 0)
                            @if($draft->timeToCook->hours < 2)
                                {{$draft->timeToCook->hours}} час
                            @else
                                {{$draft->timeToCook->hours}} часа
                            @endif
                        @endif
                        @if($draft->timeToCook->minute > 0)
                            @if($draft->timeToCook->minute < 2)
                                {{$draft->timeToCook->minute}} минута
                            @else
                                {{$draft->timeToCook->minute}} минути
                            @endif
                        @endif
                        </div>
                    </div>
                </div>
                <div id="how-to-cook" class="row my-5">
                    <div class="col-md-10 mx-auto" style="background-color: whitesmoke; border-radius: 5px">
                        <div align="justify">
                            @foreach(explode(PHP_EOL,$draft->how_to) as $paragraph)
                                <p>
                                    {{$paragraph}}
                                </p>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection
