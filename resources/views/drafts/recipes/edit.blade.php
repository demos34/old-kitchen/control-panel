@extends('layouts.app')

@section('style')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Добави нова рецепта в Драфт</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('drafts.recipes.update', $draft->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')

                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">Заглавие на рецептата</label>
                            <div class="col-md-6">
                                <input id="title"
                                       type="text"
                                       class="form-control @error('title') is-invalid @enderror"
                                       name="title"
                                       value=@if(old('title')) "{{ old('title') }}" @else "{{ $draft->title }}" @endif
                                       required autocomplete="title" autofocus>

                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="subtitle" class="col-md-4 col-form-label text-md-right">Подзаглавие на рецептата</label>
                            <div class="col-md-6">
                                <input id="subtitle"
                                       type="text"
                                       class="form-control @error('subtitle') is-invalid @enderror"
                                       name="subtitle"
                                       value=@if(old('subtitle')) "{{ old('subtitle') }}" @else "{{ $draft->subtitle }}" @endif
                                       required autocomplete="subtitle" autofocus>

                                @error('subtitle')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <label for="slug" class="col-md-4 col-form-label text-md-right">Наименование на латиница*</label>
                            <div class="col-md-6">
                                <input id="slug"
                                       type="text"
                                       class="form-control @error('slug') is-invalid @enderror"
                                       name="slug"
                                       value=@if(old('slug')) "{{ old('slug') }}" @else "{{ $slug }}" @endif
                                       required
                                       autocomplete="lat_slug" autofocus>

                                @error('slug')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 font-italic">
                                *Забележка - наименование на латиница, а не на английски. Например, ако Наименованието на категорията е "Хляб", наименование на латиница би било "hlqb" или "hlyab". Ако наименованието е повече от една дума, напр. "Мек хляб", се изписва с тире (-): "mek-hlyab". Задължнително с малки букви!
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row" align="center">
                            <label for="keywords" class="col-md-12">
                                Ключови думи:
                            </label>
                            <select id="keywords" class="col-md-12 js-multiple-keywords" name="keywords[]" multiple="multiple">
                                @foreach($keywords as $keyword)
                                    <option value="{{$keyword->id}}">{{$keyword->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <hr>
                        <div class="form-group row" align="center">
                            <label for="types" class="col-md-12">
                                Категория:
                            </label>
                            <select id="types" class="col-md-12 js-multiple-types" name="types[]" multiple="multiple">
                                @foreach($types as $type)
                                    <option value="{{$type->id}}">{{$type->type}}</option>
                                @endforeach
                            </select>
                        </div>
                        <hr>
                        <div class="form-group row" align="center">
                            <label for="tags" class="col-md-12">
                                Тагове:
                            </label>
                            <select id="tags" class="col-md-12 js-multiple" name="tags[]" multiple="multiple">
                                @foreach($tags as $tag)
                                <option value="{{$tag->id}}">{{$tag->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <hr>
                        <div class="form-group row" align="center">
                            <label class="col-md-12 mx-auto">Време за приготвяне:</label>
                            <br>
                            <label for="days" class="col-md-2 mx-auto">Дни:</label>
                            <div class="col-md-2">
                                <input id="days"
                                       type="text"
                                       class="form-control @error('days') is-invalid @enderror"
                                       name="days"
                                       value=@if(old('days')) @if(!empty(old('days')))"{{ old('days') }}"@else "0" @endif @else "{{ $draft->timeToCook->days }}" @endif
                                       required autocomplete="days" autofocus>

                                @error('days')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <label for="hours" class="col-md-2 mx-auto">Часове:</label>
                            <div class="col-md-2">
                                <input id="hours"
                                       type="text"
                                       class="form-control @error('hours') is-invalid @enderror"
                                       name="hours"
                                       value=@if(old('hours')) @if(!empty(old('hours')))"{{ old('hours') }}"@else "0" @endif @else "{{ $draft->timeToCook->hours }}" @endif
                                       required autocomplete="hours" autofocus>

                                @error('hours')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <label for="minutes" class="col-md-2 mx-auto">Минути:</label>
                            <div class="col-md-2">
                                <input id="minutes"
                                       type="text"
                                       class="form-control @error('minutes') is-invalid @enderror"
                                       name="minutes"
                                       value=@if(old('minutes')) @if(!empty(old('minutes')))"{{ old('minutes') }}"@else "0" @endif @else "{{ $draft->timeToCook->minute }}" @endif
                                       required autocomplete="minutes" autofocus>

                                @error('minutes')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <label for="meta_description" class="col-md-4 col-form-label text-md-right">Кратко описание</label>
                            <div class="col-md-6">
                                <input id="meta_description"
                                       type="text"
                                       class="form-control @error('meta_description') is-invalid @enderror"
                                       name="meta_description"
                                       value=@if(old('meta_description')) "{{ old('meta_description') }}" @else "{{ $draft->meta_description }}" @endif
                                       required autocomplete="meta_description" autofocus>

                                @error('meta_description')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="mx-auto">
                                <label for="products" class="col-md-12 col-form-label text-md-right">Необходими продукти (отделят се със точка и запетая):</label>
                            </div>
                            <div class="col-md-12">
                                <input style="min-height: 200px"
                                          id="products"
                                          type="text"
                                          class="form-control @error('posts') is-invalid @enderror"
                                          name="products"
                                          value=@if(old('posts')) "{{ old('posts') }}" @else "{{ implode(';',$draft->draftProducts->pluck('product')->toArray()) }}" @endif
                                          required autocomplete="products"
                                          autofocus >

                                @error('posts')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <label for="how_to" class="col-md-4 col-form-label text-md-right">Начин на приготвяне</label>
                            <div class="col-md-6">
                                <textarea style="min-height: 200px"
                                          id="how_to"
                                          type="text"
                                          class="form-control @error('how_to') is-invalid @enderror"
                                          name="how_to"
                                          required autocomplete="how_to"
                                          autofocus>@if(old('how_to')) {{ old('how_to') }} @else {{ $draft->how_to }} @endif</textarea>

                                @error('how_to')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <div class="col-md-12" align="center">
                                <img src="{{ implode(', ',$draft->images->pluck('url')->toArray()) }}" style="width: 50%">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="author" class="col-md-4 col-form-label text-md-right">Произход на снимката</label>
                            <div class="col-md-6">
                                <input id="author"
                                       type="text"
                                       class="form-control @error('author') is-invalid @enderror"
                                       name="author" value=@if(old('author')) "{{ old('author') }}" @else "{{ implode('; ',$draft->images->pluck('author')->toArray()) }}" @endif
                                       required autocomplete="author" autofocus>

                                @error('author')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">Снимка</label>
                            <div class="col-md-6">
                                <input type="file" class="form-control-file" id="image" name="image">
                                @if($errors->has('image'))
                                        <strong>{{ $errors->first('image') }}</strong>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4" align="center">
                                <button type="submit" class="btn btn-primary">
                                    Редактирай
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.js-multiple').select2();
        });
        $('.js-multiple').select2().val({!! json_encode($draft->tags()->get()->pluck('id')->toArray()) !!}).trigger('change');
        $(document).ready(function() {
            $('.js-multiple-keywords').select2();
        });
        $('.js-multiple-keywords').select2().val({!! json_encode($draft->keywords()->get()->pluck('id')->toArray()) !!}).trigger('change');
        $(document).ready(function() {
            $('.js-multiple-types').select2();
        });
        $('.js-multiple-types').select2().val({!! json_encode($draft->types()->get()->pluck('id')->toArray()) !!}).trigger('change');
    </script>
@endsection
