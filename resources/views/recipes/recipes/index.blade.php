@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row m-5">
        @foreach($recipes->sortByDesc('created_at') as $recipe)
            <div class="col-md-10 m-3">
                <div class="row">
                    <div class="col-md-4">
                        <img src="{{implode(',',$recipe->images->pluck('url')->toArray())}}" style="width: 100%; max-height: 300px" class="my-auto">
                        <div>
                            <small>
                                Снимка: {{ implode(', ',$recipe->images->pluck('author')->toArray()) }}
                            </small>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <small>Рейтинг: <b>5/5</b></small>
                            </div>
                            <div class="col-sm-4">
                                <small>Коментари: <b>{{ $recipe->comments()->count()}}</b></small>
                            </div>
                            <div class="col-sm-4">
                                <small>Прегледи: <b>{{ implode(', ',  $recipe->views()->pluck('views')->toArray()) }}</b></small>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <h5 class="mx-5">
                            {{$recipe->title}}
                        </h5>
                        <hr>
                        <div class="font-italic" style="font-size: 10px">
                            Автор: {{ Auth::user()->username }}, на: {{ $recipe->created_at }}
                        </div>
                        <hr>
                        <div class="font-weight-bold">
                            {{ $recipe->subtitle }}
                        </div>
                        <div class="font-weight-normal" style="overflow: hidden; max-height: 100px; font-size: 11px">
                            {{ $recipe->how_to }}
                        </div>
                    </div>
                </div>
                <hr>
                <div class="d-flex justify-content-center">
                    <div class="font-italic">
                        <a href="{{ route('recipes.recipes.show', $recipe->slug) }}">
                            Още
                        </a>
                    </div>
                </div>
                <hr>
            </div>
        @endforeach
        <div class="row">
            <div class="col-md-4 offset-8">
                {{ $recipes->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
