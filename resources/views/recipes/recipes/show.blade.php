@extends('layouts.app')
@include('cookieConsent::index')
@section('content')
    <div class="container">
        <div class="row m-5">
            <div class="col-md-12 m-3">
                <h3 class="font-weight-bold" align="center">
                    {{$draft->title}}
                </h3>
                <hr>
                <div class="row">
                    <div class="col-md-5">
                        <img src="{{implode(',',$draft->images->pluck('url')->toArray())}}"
                             style="width: 100%; max-height: 500px" class="my-auto">
                        <div>
                            <small>
                                Снимка: {{ implode(', ',$draft->images->pluck('author')->toArray()) }}
                            </small>
                        </div>
                    </div>
                    <div class="col-md-7">

                        <div class="font-italic" style="font-size: 10px">
                            Автор: {{ Auth::user()->username }}, на: {{ $draft->created_at }}
                        </div>
                        <hr>
                        <div class="font-weight-bold">
                            {{ $draft->subtitle }}
                        </div>
                        <div class="my-auto">
                            В категории:
                            @foreach($draft->types as $type)
                                <a href="" style="text-decoration: none; color: black">
                                    <span style="background-color: gray; border-radius: 3px; margin: 4px">
                                        {{$type->type}}
                                </span>
                                </a>
                            @endforeach
                        </div>
                        <div class="my-auto">
                            Тагове:
                            @foreach($draft->tags as $tag)
                                <a href="" style="text-decoration: none; color: black">
                                        <span style="background-color: gray; border-radius: 3px; margin: 4px">
                                        {{$tag->name}}
                                    </span>
                                </a>
                            @endforeach
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-4">
                                <small>Рейтинг: <b>{{$average}}/{{$max}}</b></small>
                            </div>
                            <div class="col-sm-4">
                                <small>Коментари: <b>{{ $draft->comments()->count()}}</b></small>
                            </div>
                            <div class="col-sm-4">
                                <small>Прегледи: <b>{{ implode(', ',  $draft->views()->pluck('views')->toArray()) }}</b>
                                </small>
                            </div>
                        </div>
                        <hr>
                            <form action="{{ route('ratings.values.update', $draft->id) }}" method="POST">
                                <div class="row">
                                    @csrf
                                    @method('PUT')
                                        <div class="col-md-6">
                                            <label for="ratings">Рейтинг :</label>
                                            <select class="form-control" id="ratings" name="rating_id" style="min-width: 150px">
                                                @foreach($ratingValues as $ratingValue)
                                                    <option value="{{$ratingValue->rating->id}}" name="rating_id">{{$ratingValue->rating->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6 my-auto">
                                            <button type="submit" class="btn btn-outline-success">Гласувай!</button>
                                        </div>
                                </div>
                            </form>
                        <hr>
                        <div class="my-1 py-2 mx-auto">
                            <div>
                                <label class="font-weight-bold">
                                    Управление:
                                </label>
                            </div>
                            <hr>
                            <div class="row" align="center">
                                <div class="col-sm-4">
                                    <a href="{{ route('drafts.recipes.show', $draft->draft_id) }}">
                                        <button type="button" class="btn btn-dark">Редактирай</button>
                                    </a>
                                </div>
                                <div class="col-sm-4">
                                    <form action="{{ route('drafts.recipes.destroy', $draft->draft_id) }}"
                                          method="POST">
                                        @csrf
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="btn btn-outline-danger">Изтрий</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="my-5 row" style="background-color: whitesmoke; border-radius: 5px">
                    <div class="col-md-7 offset-1 my-2">
                        <ul>
                            Необходими продукти:
                            @foreach($products as $product)
                                <li>
                                    {{ $product->product }};
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-md-4 my-2">
                        Необходимо време за приготвяне на рецептата:
                        <div class="font-weight-bold font-italic">
                            @if((int)implode('',$time->pluck('days')->toArray()) > 0)
                                @if((int)implode('',$time->pluck('days')->toArray()) < 2)
                                    {{(int)implode('',$time->pluck('days')->toArray())}} ден
                                @else
                                    {{(int)implode('',$time->pluck('days')->toArray())}} дни
                                @endif
                            @endif
                            @if((int)implode('',$time->pluck('hours')->toArray()) > 0)
                                @if((int)implode('',$time->pluck('hours')->toArray()) < 2)
                                    {{(int)implode('',$time->pluck('hours')->toArray())}} час
                                @else
                                    {{(int)implode('',$time->pluck('hours')->toArray())}} часа
                                @endif
                            @endif
                            @if((int)implode('',$time->pluck('minute')->toArray()) > 0)
                                @if((int)implode('',$time->pluck('minute')->toArray()) < 2)
                                    {{(int)implode('',$time->pluck('minute')->toArray())}} минута
                                @else
                                    {{(int)implode('',$time->pluck('minute')->toArray())}} минути
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
                <div id="how-to-cook" class="row my-5">
                    <div class="col-md-10 mx-auto" style="background-color: whitesmoke; border-radius: 5px">
                        <div align="justify">
                            @foreach(explode(PHP_EOL,$draft->how_to) as $paragraph)
                                <p>
                                    {{$paragraph}}
                                </p>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <form action="{{route('comments.comments.store')}}" method="POST">
                @csrf
                <div class="row my-1">
                    <div class="col-md-12 mx-auto">
                        <div class="card">
                            <div class="card-header">
                                Напиши коментар:
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="from" class="col-md-4 col-form-label text-md-right">Име:</label>
                                    <div class="col-md-6">
                                        <input id="from"
                                               type="text"
                                               class="form-control @error('from') is-invalid @enderror"
                                               name="from"
                                               value="{{ old('from') }}"
                                               required autocomplete="from" autofocus>

                                        @error('from')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group row">
                                    <label for="comment" class="col-md-4 col-form-label text-md-right">Коментар:</label>
                                    <div class="col-md-6">
                                <textarea style="min-height: 100px"
                                          id="comment"
                                          type="text"
                                          class="form-control @error('comment') is-invalid @enderror"
                                          name="comment"
                                          required autocomplete="comment"
                                          autofocus>{{ old('comment') }}</textarea>

                                        @error('comment')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div align="center">
                                    <button type="submit" class="btn btn-dark">
                                        Добави
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            @foreach($draft->comments as $comment)
                <div class="col-md-8 mx-auto my-1">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex justify-content-between">
                                <div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <img src="/storage/avatar/no-avatar.jpg" class="w-75 rounded-circle">
                                        </div>
                                        <div class="col-sm-8" style="margin-left: -40px">
                                            <div>{{$comment->from}}</div>
                                            <div>
                                                <small>{{$comment->created_at}}</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div align="right">
                                    <div class="row" align="right">
                                        <div class="col-sm-3" style="right: 0">
                                            <form action="{{route('comments.comments.edit', $comment->id)}}" method="GET">
                                                @csrf
                                                <input type="hidden" name="form_type" value="edit">
                                                <button type="submit" class="btn btn-primary">Редактирай</button>
                                            </form>
                                        </div>
                                        <div class="col-sm-3 my-auto" style="right: 0">
                                            @if($comment->approved == 1)
                                                <form action="{{route('comments.comments.update', $comment->id)}}" method="POST">
                                                    @csrf
                                                    @method('PUT')
                                                    <input type="hidden" name="form_type" value="hide">
                                                    <button type="submit" class="btn btn-danger rounded-circle">Скрий</button>
                                                </form>
                                            @else
                                                <form action="{{route('comments.comments.update', $comment->id)}}" method="POST">
                                                    @csrf
                                                    @method('PUT')
                                                    <input type="hidden" name="form_type" value="show">
                                                    <button type="submit" class="btn btn-success rounded-circle">Покажи</button>
                                                </form>
                                            @endif
                                        </div>
                                        <div class="col-sm-3 ">
                                            <form action="{{route('comments.values.update', $comment->id)}}" method="POST" >
                                                @csrf
                                                @method('PATCH')
                                                @if($ratings = $comment->commentRatingAddress->where('cookie', $cookie)->first())
                                                    @if($ratings->rating_comment_id == 1)
                                                        <div class="w-25" style="background-color: yellow">
                                                            <input type="image" src="/storage/home/thumbs-up.jpg" class="rounded-circle w-100" />
                                                        </div>
                                                    @else
                                                        <div class="w-25">
                                                            <input type="image" src="/storage/home/thumbs-up.jpg" class="rounded-circle w-100" />
                                                        </div>
                                                    @endif
                                                @else
                                                    <div class="w-25">
                                                        <input type="image" src="/storage/home/thumbs-up.jpg" class="rounded-circle w-100" />
                                                    </div>
                                                @endif
                                            </form>
                                        </div>
                                        <div class="col-sm-3">
                                            <form action="{{route('comments.values.update', $comment->id)}}" method="POST">
                                                @csrf
                                                @method('PUT')
                                                @if($ratings = $comment->commentRatingAddress->where('cookie', $cookie)->first())
                                                    @if($ratings->rating_comment_id == 2)
                                                        <div class="w-25" style="background-color: yellow">
                                                            <input type="image" src="/storage/home/thumbs-down.png" class="rounded-circle w-100"/>
                                                        </div>
                                                    @else
                                                        <div class="w-25">
                                                            <input type="image" src="/storage/home/thumbs-down.png" class="rounded-circle w-100"/>
                                                        </div>
                                                    @endif
                                                @else
                                                     <div class="w-25">
                                                         <input type="image" src="/storage/home/thumbs-down.png" class="rounded-circle w-100"/>
                                                     </div>
                                                @endif
                                            </form>
                                        </div>
                                    <div class="row" align="right">
                                    </div>
                                        <div class="col-sm-3" style="right: 10px">
                                        </div>
                                        <div class="col-sm-3" style="right: 10px">
                                        </div>
                                        <div class="col-sm-3">
                                               <small>{{ $comment->commentRatingAddress->where('rating_comment_id', 1)->count() }}</small>
                                        </div>
                                        <div class="col-sm-3">
                                                <small>{{ $comment->commentRatingAddress->where('rating_comment_id', 2)->count() }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            @foreach(explode(PHP_EOL, $comment->comment) as $paragraph)
                                <p align="justify">
                                    {{$paragraph}}
                                </p>
                            @endforeach

                        </div>
                    </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
