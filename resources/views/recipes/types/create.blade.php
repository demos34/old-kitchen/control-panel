@extends('layouts.app')

@section('style')
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Добави нова категория рецепти</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('recipes.types.store') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="type" class="col-md-4 col-form-label text-md-right">Наименование на категорията</label>
                            <div class="col-md-6">
                                <input id="type" type="text" class="form-control @error('type') is-invalid @enderror" name="type" value="{{ old('type') }}" required autocomplete="type" autofocus>

                                @error('type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <div class="col-md-12 font-italic font-weight-bold" align="center">
                                SEO
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <label for="lat_slug" class="col-md-4 col-form-label text-md-right">Наименование на латиница*</label>
                            <div class="col-md-6">
                                <input id="lat_slug" type="text" class="form-control @error('lat_slug') is-invalid @enderror" name="lat_slug" value="{{ old('lat_slug') }}" required autocomplete="lat_slug" autofocus>

                                @error('lat_slug')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <hr>
                            <div class="form-group row">
                                <div class="col-md-12 font-italic">
                                    *Забележка - наименование на латиница, а не на английски. Например, ако Наименованието на категорията е "Хляб", наименование на латиница би било "hlqb" или "hlyab". Ако наименованието е повече от една дума, напр. "Мек хляб", се изписва с тире (-): "mek-hlyab". Задължнително с малки букви!
                                </div>
                            </div>
                        <hr>
                        <div class="form-group row">
                            <label for="meta_description" class="col-md-4 col-form-label text-md-right">Кратко описание**</label>
                            <div class="col-md-6">
                                <input id="meta_description" type="text" class="form-control @error('meta_description') is-invalid @enderror" name="meta_description" value="{{ old('meta_description') }}" required autocomplete="meta_description" autofocus>

                                @error('meta_description')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <div class="col-md-12 font-italic">
                                **Забележка - Краткото описание служи за индексиране на страницата в търсачките. Такова кратко описание ще има за всички продукти, които ще се индексират. Трябва да е едно или две изречения, като ако Наименованието на категорията е "Хляб", то Краткото описание трябва да е нещо от вида на- "Място, на което ще намерите всички стари български рецепти за направа на Хляб и хлебни/тестени изделия".
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row" align="center">
                            <label for="tags" class="col-md-12">
                                Тагове:
                            </label>
                            <select id="tags" class="col-md-12 js-multiple" name="tags[]" multiple="multiple">
                                @foreach($tags as $tag)
                                    <option value="{{$tag->id}}">{{$tag->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <hr>
                        <div class="form-group row" align="center">
                            <label for="keywords" class="col-md-12">
                                Ключови думи:
                            </label>
                            <select id="keywords" class="col-md-12 js-multiple-keywords" name="keywords[]" multiple="multiple">
                                @foreach($keywords as $keyword)
                                    <option value="{{$keyword->id}}">{{$keyword->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <hr>

                        <div class="form-group row">
                            <div class="col-md-12 font-italic font-weight-bold" align="center">
                                Край на SEO
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">Описание</label>
                            <div class="col-md-6">
                                <textarea style="min-height: 200px"
                                          id="description"
                                          type="text"
                                          class="form-control @error('description') is-invalid @enderror"
                                          name="description"
                                          required autocomplete="description"
                                          autofocus>{{ old('description') }}</textarea>

                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="author" class="col-md-4 col-form-label text-md-right">Произход на снимката</label>
                            <div class="col-md-6">
                                <input id="author"
                                       type="text"
                                       class="form-control @error('author') is-invalid @enderror"
                                       name="author" value="{{ old('author') }}"
                                       required autocomplete="author" autofocus>

                                @error('author')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">Снимка</label>
                            <div class="col-md-6">
                                <input type="file" class="form-control-file" id="image" name="image">
                                @if($errors->has('image'))
                                        <strong>{{ $errors->first('image') }}</strong>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4" align="center">
                                <button type="submit" class="btn btn-primary">
                                    Добави
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.js-multiple').select2();
        });
        $(document).ready(function() {
            $('.js-multiple-keywords').select2();
        });
    </script>
@endsection
