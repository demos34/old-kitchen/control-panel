@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header" align="center">
                        <h3>
                            {{ $type->type }}
                        </h3>
                        <hr />
                        <div class="row">
                            <label class="col-md-4 col-form-label text-md-right">
                                Тагове:
                            </label>
                            <br>
                            <div class="col-md-6 my-auto">
                                @foreach($type->tags as $tag)
                                    <a href="{{ route('tags.tags.show', $tag->id) }}" style="text-decoration: none; color: black">
                                <span style="background-color: lightgray; border-radius: 3px; margin: 4px">
                                {{$tag->name}}
                            </span>
                                    </a>
                                @endforeach
                            </div>
                            <label class="col-md-4 col-form-label text-md-right">
                                Ключови думи:
                            </label>
                            <br>
                            <div class="col-md-6 my-auto">
                                @foreach($type->keywords as $keyword)
                                    <a href="{{ route('tags.tags.show', $keyword->id) }}" style="text-decoration: none; color: black">
                                <span style="background-color: lightgray; border-radius: 3px; margin: 4px">
                                {{$keyword->name}}
                            </span>
                                    </a>
                                @endforeach
                            </div>

                        </div>
                    </div>
                    <div class="card-body">
                        <div class="card-img pb-3 pt-2" align="center">
                            <img src="{{ implode(', ', $type->images()->get()->pluck('url')->toArray())}}"  style="max-width: 300px">
                        </div>
                        <hr>
                        <div class="mt-0" align="center">
                            <div align="center">
                                <p class="w-100">
                                    {{ $type->description }}
                                </p>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="row m-5">
                    @foreach($recipes->sortByDesc('created_at') as $recipe)
                        <div class="col-md-10 m-3">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="{{implode(',',$recipe->images->pluck('url')->toArray())}}" style="width: 100%; max-height: 300px" class="my-auto">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <small style="font-size: 10px">Рейтинг: <b>5/5</b></small>
                                        </div>
                                        <div class="col-sm-4">
                                            <small style="font-size: 10px">Коментари: <b>{{ $recipe->comments()->count()}}</b></small>
                                        </div>
                                        <div class="col-sm-4">
                                            <small style="font-size: 10px">Прегледи: <b>{{ implode(', ',  $recipe->views()->pluck('views')->toArray()) }}</b></small>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h5 class="mx-5">
                                        {{$recipe->title}}
                                    </h5>
                                    <hr>
                                    <div class="font-italic" style="font-size: 10px">
                                        Автор: {{ Auth::user()->username }}, на: {{ $recipe->created_at }}
                                    </div>
                                    <hr>
                                    <div class="font-weight-bold">
                                        {{ $recipe->subtitle }}
                                    </div>
                                    <div class="font-weight-normal" style="overflow: hidden; max-height: 100px; font-size: 11px">
                                        {{ $recipe->how_to }}
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="d-flex justify-content-center">
                                <div class="font-italic">
                                    <a href="{{ route('recipes.recipes.show', $recipe->slug) }}">
                                        Още
                                    </a>
                                </div>
                            </div>
                            <hr>
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="col-md-6">
                        {{ $recipes->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
