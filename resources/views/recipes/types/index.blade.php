@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" align="center">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h6>
                                <a href="{{ route('home.home.index') }}">
                                    <--
                                </a>
                            </h6>
                        </div>
                        <div>
                            Категории рецепти
                        </div>
                        <div>
                            *
                        </div>
                    </div>
                    <hr />
                    <div class="d-flex justify-content-center">
                        <a href="{{ route('recipes.types.create') }}">
                            <button type="button" class="btn btn-primary">
                                Добави нова
                            </button>
                        </a>
                    </div>
                </div>
                <div class="card-body">

                            @foreach($types as $type)
                                <div align="center">
                                    <div>
                                        <h3>
                                            <a href="{{ route('recipes.types.show', $type->slug )}}">
                                                {{ $type->type }}
                                            </a>
                                        </h3>
                                        <hr>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between ml-5 mr-5 mt-2 mb-2 pb-3 " style="border-bottom: 1px solid #333">
                                    <div class="row">
                                        <a class="w-100"  href="{{ route('recipes.types.show', $type->slug )}}">
                                        <img src="{{ implode(', ', $type->images()->get()->pluck('url')->toArray())}}"  style="max-width: 300px">
                                        </a>
                                    </div>
                                    <div class="mt-0" align="center">
                                        <div align="center">
                                            <p class="w-50" style="max-height: 180px; ; overflow: hidden;">
                                                {{ $type->description }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                        <div class="d-flex justify-content-center pb-5">
                            <a href="{{ route('recipes.types.edit',$type->id) }}" class="m-2">
                                <button type="button" class="btn btn-dark">Промени</button>
                            </a>
                            <form action="{{ route('recipes.types.destroy',$type->id) }}" method="POST" class="m-2">
                                @csrf
                                {{ method_field('DELETE') }}
                                <button onclick="alert('Are you sure?')" type="submit" class="btn btn-warning">Изтрий</button>
                            </form>
                        </div>
                            @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
