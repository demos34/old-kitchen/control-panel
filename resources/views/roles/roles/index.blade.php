@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" align="center">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h6>
                                <a href="{{ route('administrator.administrator.index') }}">
                                    <--
                                </a>
                            </h6>
                        </div>
                        <div>
                            Roles
                        </div>
                        <div>
                            *
                        </div>
                    </div>
                    <hr />
                    <div class="d-flex justify-content-center">
                        <a href="{{ route('roles.roles.create') }}">
                            <button type="button" class="btn btn-primary">
                                Add new role
                            </button>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Role</th>
                            <th scope="col">Actions</th>

                        </tr>
                        </thead>
                        <tbody>
                            @foreach($roles as $role)
                                <tr>
                                <th scope="row">{{ $role->id }}</th>
                                <td>
                                    <a href="">
                                        {{ $role->role }}
                                    </a>
                                </td>
                                 <td>
                                     <div class="d-flex justify-content-between">
                                         <a href="{{ route('roles.roles.edit',$role->id) }}">
                                             <button type="button" class="btn btn-dark">Edit</button>
                                         </a>
                                         <form action="{{ route('roles.roles.destroy',$role->id) }}" method="POST">
                                             @csrf
                                             {{ method_field('DELETE') }}
                                             <button onclick="alert('Are you sure?')" type="submit" class="btn btn-warning">Delete</button>
                                         </form>
                                     </div>
                                </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
