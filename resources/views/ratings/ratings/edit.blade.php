@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 m-auto">
                            <div class="well">
                                <h5 class="text-center">
                                    <label>
                                        Промени рейтинг: {{ $rating->name }}
                                    </label>
                                </h5>
                                <form action="{{ route('ratings.ratings.update', $rating->id) }}" method="POST">
                                    @csrf
                                    @method('PATCH')
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="name">
                                                Name:
                                            </label>
                                        </div>
                                        <div class="card-body">
                                            <h5>
                                                <input id="name"
                                                       type="text"
                                                       class="form-control @error('name') is-invalid @enderror"
                                                       name="name"
                                                       value=@if(old('name'))"{{old('name')}}"@else"{{$rating->name}}"@endif
                                                       required autocomplete="name" autofocus>
                                                @error('name')
                                                <br>
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="value">
                                                Стойност:
                                            </label>
                                        </div>
                                        <div class="card-body">
                                            <h5>
                                                <input id="value"
                                                       type="text"
                                                       class="form-control @error('value') is-invalid @enderror"
                                                       name="value"
                                                       value=@if(old('value'))"{{old('value')}}"@else"{{implode(',', $rating->ratingValues()->pluck('value')->toArray())}}"@endif
                                                       required autocomplete="value" autofocus>
                                                @error('value')
                                                <br>
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center m-2">
                                        <button type="submit" class="btn btn-dark">
                                            Промени
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
