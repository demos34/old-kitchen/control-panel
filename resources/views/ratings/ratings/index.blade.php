@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card my-3 mx-auto">
                                <div class="card-header">
                                    <h4>
                                        Рейтинги:
                                    </h4>
                                </div>
                                <div class="card-body">
                                    <table class="table table-borderless">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Value</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($ratings as $rating)
                                            <tr>
                                                <td>
                                                    {{$rating->id}}
                                                </td>
                                                <td>
                                                    {{$rating->name}}
                                                </td>
                                                <td>
                                                    {{implode(',', $rating->ratingValues()->pluck('value')->toArray())}}
                                                </td>
                                                <td>
                                                    <a href="{{route('ratings.ratings.edit', $rating->id)}}"><button type="button" class="btn btn-dark float-left">Промени</button></a>
                                                    <form action="{{ route('ratings.ratings.destroy', $rating->id) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger float-left" onclick="alert('Сигурни ли сте?')">Изтрий</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 my-3">
                            <div class="well">
                                <h4 class="text-center">
                                    <label>
                                        Добави рейтинг
                                    </label>
                                </h4>
                                <form action="{{ route('ratings.ratings.store') }}" method="POST">
                                    @csrf
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="name">
                                                Name:
                                            </label>
                                        </div>
                                        <div class="card-body">
                                            <h5>
                                                <input id="name"
                                                       type="text"
                                                       class="form-control @error('name') is-invalid @enderror"
                                                       name="name"
                                                       value="{{ old('name') }}"
                                                       required autocomplete="name" autofocus>
                                                @error('name')
                                                <br>
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="value">
                                                Стойност:
                                            </label>
                                        </div>
                                        <div class="card-body">
                                            <h5>
                                                <input id="value"
                                                       type="text"
                                                       class="form-control @error('value') is-invalid @enderror"
                                                       name="value"
                                                       value="{{ old('value') }}"
                                                       required autocomplete="value" autofocus>
                                                @error('value')
                                                <br>
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center m-2">
                                        <button type="submit" class="btn btn-dark">
                                            Добави
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
