<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DraftRecipe extends Model
{
    protected $guarded = [];

    public function images()
    {
        return $this->belongsToMany('App\Models\Image', 'draft_images', 'draft_id', 'img_id');
    }

    public function recipe()
    {
        return $this->hasOne('App\Models\Recipe');
    }

    public function draftProducts()
    {
        return $this->hasMany('App\Models\DraftProduct');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'draft_user', 'draft_id', 'user_id');
    }

    public function types()
    {
        return $this->belongsToMany('App\Models\Type', 'draft_type', 'draft_id', 'type_id');
    }

    public function timeToCook()
    {
        return $this->hasOne('App\Models\TimeToCook');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'draft_tag', 'draft_id', 'tag_id');
    }

    public function isReadyToPublish()
    {
        return $this->hasOne('App\Models\IsReadyToPublish');
    }

    public function PublishedDraft()
    {
        return $this->hasOne('App\Models\PublishedDraft');
    }

    public function keywords()
    {
        return $this->belongsToMany('App\Models\Keyword');
    }

    public function viewsBackup()
    {
        return $this->hasOne('App\Models\ViewsBackup');
    }

    public function backupComments()
    {
        return $this->hasMany('App\Models\BackupComment');
    }

    public function backupRatingRecipes()
    {
        return $this->hasMany('App\Models\BackupRatingRecipe');
    }
}
