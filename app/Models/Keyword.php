<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    protected $guarded = [];

    public function draftRecipes()
    {
        return $this->belongsToMany('App\Models\DraftRecipe');
    }

    public function recipes()
    {
        return $this->belongsToMany('App\Models\Recipe');
    }

    public function types()
    {
        return $this->belongsToMany('App\Models\Type');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Models\Post');
    }

    public function tubes()
    {
        return $this->belongsToMany('App\Models\Home\Tube');
    }
}
