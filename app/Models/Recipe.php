<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $guarded = [];

    public function images()
    {
        return $this->belongsToMany('App\Models\Image', 'recipes_images', 'recip_id', 'img_id');
    }

    public function ratings()
    {
        return $this->belongsToMany('App\Models\Rating', 'recipes_ratings', 'recip_id', 'rating_id');
    }

    public function draftRecipe()
    {
        return $this->belongsTo('App\Models\DraftRecipe');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function views()
    {
        return $this->hasMany('App\Models\Views');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function types()
    {
        return $this->belongsToMany('App\Models\Type', 'recipes_types', 'recipe_id', 'type_id');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag');
    }

    public function keywords()
    {
        return $this->belongsToMany('App\Models\Keyword');
    }

    public function ratingRecipes()
    {
        return $this->hasMany('App\Models\RatingRecipe');
    }

}
