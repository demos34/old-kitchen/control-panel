<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IndexTag extends Model
{
    protected $guarded = [];

    public function homeInfos()
    {
        return $this->belongsToMany('App\Models\HomeInfo');
    }
}
