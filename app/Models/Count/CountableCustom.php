<?php

namespace App\Models\Count;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CountableCustom extends Model
{
    protected $guarded = [];

    public function countableCustomRecords(): HasMany
    {
        return $this->hasMany(CountableCustomRecord::class);
    }
}
