<?php

namespace App\Models\Count;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CountType extends Model
{
    protected $guarded = [];

    public function counts(): HasMany
    {
        return $this->hasMany(Count::class);
    }

    public function countables(): HasMany
    {
        return $this->hasMany(Countable::class);
    }

    public function countableCustomRecords(): HasMany
    {
        return $this->hasMany(CountableCustomRecord::class);
    }

}
