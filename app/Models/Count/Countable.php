<?php

namespace App\Models\Count;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Countable extends Model
{
    protected $guarded = [];

    public function countType(): BelongsTo
    {
        return $this->belongsTo(CountType::class);
    }
}
