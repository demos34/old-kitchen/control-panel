<?php

namespace App\Models\Count;

use App\Models\Administrator\Site;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Count extends Model
{
    protected $guarded = [];

    public function countType(): BelongsTo
    {
        return $this->belongsTo(CountType::class);
    }

    public function site(): BelongsTo
    {
        return $this->belongsTo(Site::class);
    }
}
