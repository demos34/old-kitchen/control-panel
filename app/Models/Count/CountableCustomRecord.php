<?php

namespace App\Models\Count;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CountableCustomRecord extends Model
{
    protected $guarded = [];

    public function countType(): BelongsTo
    {
        return $this->belongsTo(CountType::class);
    }

    public function countableCustom(): BelongsTo
    {
        return $this->belongsTo(CountableCustom::class);
    }
}
