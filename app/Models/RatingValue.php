<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RatingValue extends Model
{
    protected $guarded = [];

    public function rating()
    {
        return $this->belongsTo('App\Models\Rating');
    }
}
