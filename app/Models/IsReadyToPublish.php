<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IsReadyToPublish extends Model
{
    protected $guarded = [];
    public function draft()
    {
        return $this->belongsTo('App\Models\DraftRecipe');
    }
}
