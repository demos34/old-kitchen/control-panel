<?php

namespace App\Models\Administrator;

use Illuminate\Database\Eloquent\Model;

class AnalystArchive extends Model
{
    protected $guarded = [];

    public function site(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Site::class);
    }
}
