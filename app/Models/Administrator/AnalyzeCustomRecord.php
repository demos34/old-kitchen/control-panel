<?php

namespace App\Models\Administrator;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AnalyzeCustomRecord extends Model
{
    protected $guarded = [];

    public function site(): BelongsTo
    {
        return $this->belongsTo(Site::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function analystCustom(): BelongsTo
    {
        return $this->belongsTo(AnalystCustom::class);
    }

    public function test($perPage)
    {
        dd($perPage);
    }
}
