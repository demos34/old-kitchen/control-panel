<?php

namespace App\Models\Administrator;

use App\Models\Count\Count;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Site extends Model
{
    protected $guarded = [];

    public function analysts(): HasMany
    {
        return $this->hasMany(Analyst::class);
    }

    public function analystArchives(): HasMany
    {
        return $this->hasMany(AnalystArchive::class);
    }

    public function analyzeCustomRecords(): HasMany
    {
        return $this->hasMany(AnalyzeCustomRecord::class);
    }

    public function counts(): HasMany
    {
        return $this->hasMany(Count::class);
    }
}
