<?php

namespace App\Models\Administrator;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class AnalystCustom extends Model
{
    protected $guarded = [];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function analyzeCustomRecords(): HasMany
    {
        return $this->hasMany(AnalyzeCustomRecord::class);
    }
}
