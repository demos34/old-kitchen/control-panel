<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $guarded = [];

    public function buyFrom()
    {
        return $this->belongsToMany('App\Models\BuyFrom', 'buying_books', 'book_id', 'from_where_id');
    }
}
