<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $guarded = [];

    public function ratingCommentsValues()
    {
        return $this->belongsToMany('App\Models\RatingCommentsValue');
    }

    public function recipe()
    {
        return $this->belongsTo('App\Models\Recipe');
    }

    public function commentRatingAddress()
    {
        return $this->hasMany('App\Models\CommentRatingAddress');
    }


}
