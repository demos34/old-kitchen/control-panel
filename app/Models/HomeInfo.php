<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeInfo extends Model
{
    protected $guarded = [];

    public function indexTags()
    {
        return $this->belongsToMany('App\Models\IndexTag');
    }
}
