<?php

namespace App\Models\Consult;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $guarded = [];

    public function clientInformation(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne('App\Models\Consult\ClientInformation');
    }

    public function consultSubcategories(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Consult\ConsultSubcategory');
    }

    public function consultCategories(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Consult\ConsultCategory');
    }

    public function clientCategoryAntherDishes(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Consult\ClientCategoryAntherDish');
    }

    public function message(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne('App\Models\Consult\ConsultMessage');
    }

    public function consultStatuses(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Consult\ConsultStatus');
    }

    public function consultType(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ConsultType::class);
    }

    public function businessMessage(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(BusinessMessage::class);
    }
}
