<?php

namespace App\Models\Consult;

use Illuminate\Database\Eloquent\Model;

class ConsultService extends Model
{
    protected $guarded = [];

    public function consultType(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ConsultType::class);
    }

    public function consultPrice(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(ConsultPrice::class);
    }
}
