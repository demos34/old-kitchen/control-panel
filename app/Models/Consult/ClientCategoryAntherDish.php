<?php

namespace App\Models\Consult;

use Illuminate\Database\Eloquent\Model;

class ClientCategoryAntherDish extends Model
{
    protected $guarded = [];

    public function client(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Consult\Client');
    }

    public function consultCategory(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Consult\ConsultCategory');
    }
}
