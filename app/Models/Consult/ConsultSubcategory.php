<?php

namespace App\Models\Consult;

use Illuminate\Database\Eloquent\Model;

class ConsultSubcategory extends Model
{
    protected $guarded = [];

    public function consultCategory(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Consult\ConsultCategory');
    }

    public function clients(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Consult\Client');
    }
}
