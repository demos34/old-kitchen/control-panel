<?php

namespace App\Models\Consult;

use Illuminate\Database\Eloquent\Model;

class ConsultCategory extends Model
{
    protected $guarded = [];

    public function consultSubcategories(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Consult\ConsultSubcategory');
    }

    public function clients(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Consult\Client');
    }

    public function clientCategoryAntherDishes(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany('App\Models\Consult\ClientCategoryAntherDish');
    }

}
