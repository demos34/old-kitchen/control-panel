<?php

namespace App\Models\Consult;

use Illuminate\Database\Eloquent\Model;

class ConsultStatus extends Model
{
    protected $guarded = [];

    public function clients(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany('App\Models\Consult\Client');
    }
}
