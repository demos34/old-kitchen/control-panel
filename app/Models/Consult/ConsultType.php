<?php

namespace App\Models\Consult;

use Illuminate\Database\Eloquent\Model;

class ConsultType extends Model
{
    protected $guarded = [];

    public function consultServices(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ConsultService::class);
    }

    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    public function client(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Client::class);
    }

    public function  consultMessages(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ConsultMessage::class);
    }

    public function consultComments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ConsultComment::class);
    }

    public function  consultTypeStatus(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(ConsultTypesStatus::class);
    }

    public function description(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(ConsultDescription::class);
    }

}
