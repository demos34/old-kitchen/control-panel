<?php

namespace App\Models\Consult;

use Illuminate\Database\Eloquent\Model;

class Spice extends Model
{
    protected $guarded = [];

    public function clientInformation(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne('App\Models\Consult\ClientInformation');
    }
}
