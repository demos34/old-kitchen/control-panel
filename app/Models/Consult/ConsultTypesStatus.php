<?php

namespace App\Models\Consult;

use Illuminate\Database\Eloquent\Model;

class ConsultTypesStatus extends Model
{
    protected $guarded = [];

    public function consultType(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ConsultType::class);
    }
}
