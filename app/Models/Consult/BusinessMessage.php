<?php

namespace App\Models\Consult;

use Illuminate\Database\Eloquent\Model;

class BusinessMessage extends Model
{
    protected $guarded = [];

    public function client(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Client::class);
    }
}
