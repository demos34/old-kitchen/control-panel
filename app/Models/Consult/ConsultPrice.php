<?php

namespace App\Models\Consult;

use Illuminate\Database\Eloquent\Model;

class ConsultPrice extends Model
{
    protected $guarded = [];

    public function consultService(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ConsultService::class);
    }
}
