<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $guarded = [];

    public function recipes()
    {
        return $this->belongsToMany('App\Models\Recipe', 'recipes_ratings', 'rating_id', 'recip_id');
    }

    public function ratingValues()
    {
        return $this->hasMany('App\Models\RatingValue');
    }

    public function ratingRecipes()
    {
        return $this->hasMany('App\Models\RatingRecipe');
    }
}
