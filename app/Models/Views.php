<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Views extends Model
{
    protected $guarded = [];

    public function recipe()
    {
        return $this->belongsTo('App\Models\Recipe');
    }
}
