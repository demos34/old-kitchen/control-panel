<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuyFrom extends Model
{

    protected $guarded = [];

    public function books()
    {
        return $this->belongsToMany('App\Models\Book','buying_books', 'from_where_id', 'book_id');
    }
}
