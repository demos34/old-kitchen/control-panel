<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $guarded = [];

    public function draftRecipes()
    {
        return $this->belongsToMany('App\Models\DraftRecipe', 'draft_images', 'img_id', 'draft_id');
    }

    public function recipes()
    {
        return $this->belongsToMany('App\Models\Recipe', 'recipes_images', 'img_id', 'recip_id');
    }

    public function profiles()
    {
        return $this->belongsToMany('App\Models\Profile');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function types()
    {
        return $this->belongsToMany('App\Models\Type');
    }
}
