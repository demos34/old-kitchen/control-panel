<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommentRatingAddress extends Model
{
    protected $guarded = [];

    public function comments()
    {
        return $this->belongsTo('App\Models\Comment');
    }

    public function ratingComments()
    {
        return $this->belongsTo('App\Models\RatingCommentsValue');
    }
}
