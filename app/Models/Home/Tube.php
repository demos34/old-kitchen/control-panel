<?php

namespace App\Models\Home;

use Illuminate\Database\Eloquent\Model;

class Tube extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag');
    }

    public function keywords()
    {
        return $this->belongsToMany('App\Models\Keyword');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
