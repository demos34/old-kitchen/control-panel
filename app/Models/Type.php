<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $guarded = [];

    public function recipes()
    {
        return $this->belongsToMany('App\Models\Recipe','recipes_types', 'type_id', 'recipe_id');
    }

    public function drafts()
    {
        return $this->belongsToMany('App\Models\DraftRecipe', 'draft_type', 'type_id', 'draft_id');
    }

    public function images()
    {
        return $this->belongsToMany('App\Models\Image');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag');
    }

    public function keywords()
    {
        return $this->belongsToMany('App\Models\Keyword');
    }
}
