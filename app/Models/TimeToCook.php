<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimeToCook extends Model
{
    protected $guarded = [];

    public function draftRecipe()
    {
        return $this->belongsTo('App\Models\DraftRecipe');
    }
}
