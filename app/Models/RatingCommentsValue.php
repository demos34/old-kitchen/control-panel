<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RatingCommentsValue extends Model
{
    protected $guarded = [];

    public function comments()
    {
        return $this->belongsToMany('App\Models\Comment');
    }

    public function commentRatingAddress()
    {
        return $this->hasMany('App\Models\CommentRatingAddress');
    }
}
