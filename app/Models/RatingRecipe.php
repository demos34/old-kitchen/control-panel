<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RatingRecipe extends Model
{
    protected $guarded = [];

    public function  recipe()
    {
        return $this->belongsTo('App\Models\Recipe');
    }

    public function  rating()
    {
        return $this->belongsTo('App\Models\Rating');
    }
}
