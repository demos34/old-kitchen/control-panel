<?php
namespace App\Repositories;

use App\Models\DraftRecipe;
use Illuminate\Http\Request;

interface DraftsRepositoryInterface
{
    public function addNewDraft(Request $request);

    public function updateExistingDraft(Request $request, DraftRecipe $draftRecipe);

    public function publishDraftRecipe(Request $request, DraftRecipe $draftRecipe);

    public function publishManyDraftRecipes(Request $request, $draftRecipes);

    public function destroyDraftRecipe(DraftRecipe $draftRecipe);

    public function preparingToPublish(Request $request, DraftRecipe $draftRecipe);
}