<?php
/**
 * Created by PhpStorm.
 * User: Inso
 * Date: 22.5.2020 г.
 * Time: 11:51
 */

namespace App\Repositories;

use App\Models\Role;

interface RolesRepositoryInterface
{
    public function addNewRole($request);

    public function updateRole(Role $role, $request);

    public function deleteRole(Role $role);
}