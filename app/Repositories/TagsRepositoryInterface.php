<?php
namespace App\Repositories;

use App\Models\Tag;
use Illuminate\Http\Request;

interface TagsRepositoryInterface
{
    public function addTag(Request $request);

    public function updateTag(Request $request, Tag $tag);

    public function deleteTag(Tag $tag);

    public function addIndexTag(Request $request);

    public function updateIndexTag(Request $request, Tag $tag);

    public function deleteIndexTag(Tag $tag);
}