<?php
namespace App\Repositories;


use App\Models\Rating;
use App\Models\RatingValue;
use Illuminate\Http\Request;

class RatingsRepository implements RatingsRepositoryInterface
{
    public function addNewRating(Request $request)
    {
        $data = $request->validate(
            [
                'name' => 'required|min:3|max:50|unique:ratings',
                'value' => 'required|min:1|max:2|unique:rating_values',
            ]
        );

        $rating = Rating::create(
            [
                'name' => $data['name'],
            ]
        );

        RatingValue::create(
            [
                'rating_id' => $rating->id,
                'value' => $data['value'],
            ]
        );
    }

    public function updateRating(Request $request, Rating $rating)
    {
        $checkName = $this->checkName($request, $rating);
        $checkValue = $this->checkValue($request, $rating);

        $rating->update(
            [
                'name' => $checkName,
            ]
        );

        $rating->ratingValues()->update(
            [
                'value' => $checkValue,
            ]
        );

    }

    public function getAllRatings()
    {
        return Rating::all();
    }

    public function deleteRating(Rating $rating)
    {
        return $rating->delete();
    }

    protected function checkName(Request $request, Rating $rating)
    {
        if ($request->name == $rating->name){
            return $request->name;
        } else {
            $check = $request->validate(
            [
                'name' => 'required|min:3|max:50|unique:ratings',
            ]
        );
            return $check['name'];
        }
    }

    protected function checkValue(Request $request, Rating $rating)
    {
        if ((int)$request->value === (int)implode(',', $rating->ratingValues->pluck('value')->toArray())){
            return (int)implode(',', $rating->ratingValues->pluck('value')->toArray());
        } else {
            $check = $request->validate(
                [
                    'value' => 'required|min:1|max:2|unique:rating_values',
                ]
            );
            return $check['value'];
        }
    }
}
