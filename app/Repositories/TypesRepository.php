<?php
namespace App\Repositories;


use App\Models\Image;
use App\Models\Tag;
use App\Models\Type;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class TypesRepository implements TypesRepositoryInterface
{
    public function addNewType($request)
    {

        $data = $request->validate(
            [
                'type' => 'required|unique:types|min:3|max:50',
                'lat_slug' => 'required|unique:types|min:3|max:255',
                'meta_description' => 'required|min:3|max:255',
                'description' => 'required',
                'author' => 'required|min:3|max:255',
                'image' => 'required|image',
            ]
        );

        $type = Type::create([
            'type' => $data['type'],
            'lat_slug' => $data['lat_slug'],
            'meta_description' => $data['meta_description'],
            'description' => $data['description'],
        ]);

        $arrayTitle = explode(' ', Str::lower($type->type));
        $slugifiedTitle = implode('-', $arrayTitle);

        $slug = $type->id .'-'. $slugifiedTitle;

        $type->update(
            [
                'slug' => $slug,
            ]
        );

        if(isset($request->tags))
        {
            $tags = $request->tags;
            foreach ($tags as $tag){
                $type->tags()->attach($tag);
            }
        }

        if(isset($request->keywords))
        {
            $type->keywords()->attach($request->keywords);
        }

        $image = $request['image']->store('uploads', 'public');

        $imagePath = '/storage/'.$image;

        $userId = Auth::user()->id;

        $createdImage = Image::create(
            [
                'url' => $imagePath,
                'author' => $data['author'],
            ]
        );

        $createdImageId = $createdImage->id;
        $type->images()->attach($createdImageId);


        $dataToInsert = array('img_id' => $createdImageId, 'user_id' => $userId);
        DB::table('uploaded_img_from_users')->insert($dataToInsert);
    }

    public function update(Type $type, $request)
    {
        $type->update(
            [
                'type' => 'type',
                'lat_slug' => 'lat_slug',
            ]
        );

        $data = $request->validate(
            [
                'type' => 'required|unique:types|min:3|max:50',
                'lat_slug' => 'required|unique:types|min:3|max:255',
                'meta_description' => 'required|min:3|max:255',
                'description' => 'required',
            ]
        );

        $tags = $request->tags;
        $keywords = $request->keywords;
        $type->tags()->sync($tags);
        $type->keywords()->sync($keywords);


        if($request['image'] === NULL) {
            $updated = $type->update($data);

            $arrayTitle = explode(' ', Str::lower($type->type));
            $slugifiedTitle = implode('-', $arrayTitle);

            $slug = $type->id .'-'. $slugifiedTitle;

            $type->update(
                [
                    'slug' => $slug,
                ]
            );

            return $updated;
        } else {
            $updated = $type->update($data);

            $arrayTitle = explode(' ', Str::lower($type->type));
            $slugifiedTitle = implode('-', $arrayTitle);

            $slug = $type->id .'-'. $slugifiedTitle;

            $type->update(
                [
                    'slug' => $slug,
                ]
            );

            $image = $request['image']->store('uploads', 'public');


            $imagePath = '/storage/'.$image;



            $userId = Auth::user()->id;

            $createdImage = Image::create(
                [
                    'url' => $imagePath,
                    'author' => $request['author'],
                ]
            );

            $createdImageId = $createdImage->id;
            $type->images()->sync($createdImageId);

            $dataToInsert = array('img_id' => $createdImageId, 'user_id' => $userId);
            DB::table('uploaded_img_from_users')->insert($dataToInsert);
        }
    }

    public function destroy(Type $type)
    {
        $type->delete();
    }

    public function getAllByType(Type $type)
    {
        // TODO: Implement getAllByType() method.
    }
}