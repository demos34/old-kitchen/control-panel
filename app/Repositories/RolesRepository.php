<?php
/**
 * Created by PhpStorm.
 * User: Inso
 * Date: 22.5.2020 г.
 * Time: 11:50
 */

namespace App\Repositories;


use App\Models\Role;

class RolesRepository implements RolesRepositoryInterface
{
    public function addNewRole($request)
    {
        $newRole = $request->validate(
            [
                'role' => 'required|unique:roles|min:3|max:255'
            ]
        );

        return Role::create(
            [
                'role' => $newRole
            ]
        );
    }

    public function updateRole(Role $role, $request)
    {
        $newRole = $request->validate(
            [
                'role' => 'required|unique:roles|min:3|max:255'
            ]
        );
        return $role->update($newRole);

    }

    public function deleteRole(Role $role)
    {
        $role->delete();
    }
}