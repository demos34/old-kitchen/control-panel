<?php
/**
 * Created by PhpStorm.
 * User: Inso
 * Date: 5.6.2020 г.
 * Time: 11:45
 */

namespace App\Repositories;

use App\Models\Recipe;
use Illuminate\Http\Request;

interface RecipesRepositoryInterface
{
    public function viewAll();

    public function getProducts($recipeId);

    public function getTimeToCook($draftId);

    public function increaseViews(Recipe $recipe);

    public function getSumOfRatings(Recipe $recipe);

    public function  getAllRatingsValues();

    public function getMaxValue();

    public function setRating(Request $request, Recipe $recipe);

    public function getLastNRecipes($count);
}
