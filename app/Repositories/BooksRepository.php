<?php
namespace App\Repositories;


use App\Models\Book;
use App\Models\BuyFrom;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BooksRepository implements BooksRepositoryInterface
{
    public function addNewBook(Request $request)
    {
        $data = $request->validate(
            [
                'title' => 'required|min:3|max:255|unique:books',
                'slug' => 'required|min:3|max:255|unique:books',
                'description' => 'required|min:3|max:1000|unique:books',
                'price' => 'required|min:3|max:255',
                'image' => 'required|image',
            ]
        );

        $image = $data['image']->store('books', 'public');

        $imagePath = '/storage/'.$image;

        $userId = Auth::user()->id;

        $createdImage = Image::create(
            [
                'url' => $imagePath,
                'author' => Auth::user()->username,
            ]
        );

        $createdImageId = $createdImage->id;


        $dataToInsert = array('img_id' => $createdImageId, 'user_id' => $userId);
        DB::table('uploaded_img_from_users')->insert($dataToInsert);

        return Book::create(
            [
                'title' => $data['title'],
                'description' => $data['description'],
                'price' => $data['price'],
                'slug' => $data['slug'],
                'image' => $imagePath,
            ]
        );
    }

    public function editExistingBook(Request $request, Book $book)
    {

        $title = $this->validateTitle($request, $book);
        $slug = $this->validateSlug($request, $book);
        $data = $request->validate(
            [
                'description' => 'required|min:3|max:1000',
                'price' => 'required|min:3|max:255',
            ]
        );

        if(!empty($request->image)) {
            self::deleteProductImage($book);
            $validatedImage = $request->validate(
                [
                    'image' => 'image',
                ]
            );
            $image = $validatedImage['image']->store('books', 'public');

            $imagePath = '/storage/'.$image;

            $userId = Auth::user()->id;

            $createdImage = Image::create(
                [
                    'url' => $imagePath,
                    'author' => Auth::user()->username,
                ]
            );

            $createdImageId = $createdImage->id;


            $dataToInsert = array('img_id' => $createdImageId, 'user_id' => $userId);
            DB::table('uploaded_img_from_users')->insert($dataToInsert);
        } else {
            $imagePath = $book->image;
        }

        $book->update(
            [
                'title' => $title['title'],
                'description' => $data['description'],
                'price' => $data['price'],
                'slug' => $slug['slug'],
                'image' => $imagePath,
            ]
        );

        $book->buyFrom()->sync($request->publishers);
    }

    public function deleteBook(Book $book)
    {
        $book->delete();
    }

    public function addPublisher(Request $request)
    {
        $data = $request->validate(
            [
                'from_where' => 'required|min:3|max:255|unique:buy_froms',
                'slug' => 'required|min:3|max:255|unique:buy_froms',
                'url' => 'required|min:3|max:255',
                'image' => 'required|image',
            ]
        );

        $image = $data['image']->store('publishers', 'public');

        $imagePath = '/storage/'.$image;

        $userId = Auth::user()->id;

        $createdImage = Image::create(
            [
                'url' => $imagePath,
                'author' => Auth::user()->username,
            ]
        );

        $createdImageId = $createdImage->id;


        $dataToInsert = array('img_id' => $createdImageId, 'user_id' => $userId);
        DB::table('uploaded_img_from_users')->insert($dataToInsert);

        return BuyFrom::create(
            [
                'from_where' => $data['from_where'],
                'url' => $data['url'],
                'slug' => $data['slug'],
                'image' => $imagePath,
            ]
        );
    }

    public function editExistingPublisher(Request $request, BuyFrom $buyFrom)
    {
        // TODO: Implement getPublisher() method.
    }

    public function deletePublisher(BuyFrom $buyFrom)
    {
        $buyFrom->delete();
    }

    public function getBookBySlug($slug)
    {
        // TODO: Implement getPublisher() method.
    }

    public function  getAllBooks()
    {
        return Book::all();
    }

    public function getPublisher($slug)
    {
        // TODO: Implement getPublisher() method.
    }

    public function getAllPublishers()
    {
        return BuyFrom::all();
    }

    protected function validateTitle(Request $request, Book $book)
    {
        if($request->title == $book->title){
            return $title[] = ['title' => $book->title];
        } else {
            return $request->validate(
                [
                    'title' => 'required|min:3|max:255|unique:books',
                ]
            );
        }


    }

    protected function validateSlug(Request $request, Book $book)
    {
        if($request->slug == $book->slug){
            return $slug[] = ['slug' => $book->slug];
        } else {
            return $request->validate(
                [
                'slug' => 'required|min:3|max:255|unique:books',
                ]
            );
        }
    }

    protected static function deleteProductImage(Book $book)
    {
        $originalImage = $book->image;
        $array = explode('/', $originalImage);
        $fileName = $array['3'];
        $path = '/public/' . $array['2'] . '/' . $fileName;
        Storage::delete($path);
    }
}
