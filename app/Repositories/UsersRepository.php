<?php
namespace App\Repositories;


use App\Models\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsersRepository implements UsersRepositoryInterface
{

    public function updateUser(User $user, $request)
    {
        if($user->username == $request->username){
            if($user->email == $request->email){
                $validated = [
                    'username' => $user->username,
                    'email' => $user->email
                ];
            } else {
                $validated = $request->validate(
                    [
                        'email' => 'required|unique:users|min:3|max:255'
                    ]
                );
            }
        } elseif ($user->email == $request->email) {
            if($user->username == $request->username){
                $validated = [
                    'username' => $user->username,
                    'email' => $user->email
                ];
            } else {
                $validated = $request->validate(
                    [
                        'username' => 'required|unique:users|min:3|max:255'
                    ]
                );
            }
        }
        else {
            $validated = $request->validate(
                [
                    'username' => 'required|unique:users|min:3|max:255',
                    'email' => 'required|unique:users|min:3|max:255'
                ]
            );
        }
        $user->update($validated);
        $user->roles()->sync($request->role);
    }

    public function registerUser($request)
    {
        $newUser = $this->validator($request->all())->validate();
            $user = User::create(
                [
                    'username' => $newUser['username'],
                    'email' => $newUser['email'],
                    'password' =>  Hash::make($newUser['password']),
                ]
            );

            if ($request->role > 0) {
                $user->roles()->attach($request->role);
            }

            Profile::create(
                [
                    'user_id' => $user->id
                ]
            );




    }

    public function changePassword(Request $request)
    {
        $checkedPassword = $this->passwordValidator($request->all())->validate();
        $user = $this->returnUser();
        $oldPassword = $user->password;
        $oldRequestedPassword = $checkedPassword['old-password'];
        if (Hash::check($oldRequestedPassword, $oldPassword) === TRUE){
            $newPassword = $checkedPassword['password'];
            $newHashedPassword = Hash::make($newPassword);
            return $user->update(
                [
                    'password' => $newHashedPassword,
                ]
            );
        } else {
            return false;
        }
    }

    public function returnProfile()
    {
        return Profile::findOrFail(Auth::user()->id);
    }

    public function returnUser()
    {
        return Auth::user();
    }

    public function returnAuthUser()
    {
        return Auth::user()->id;
    }



    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'max:255', 'min:4', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    protected function passwordValidator(array $data)
    {
        return Validator::make($data, [
            'old-password' => ['required', 'string', 'min:8'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }
}
