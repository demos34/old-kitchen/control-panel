<?php

namespace App\Repositories;


use App\Models\Comment;
use App\Models\CommentRatingAddress;
use App\Models\Recipe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use MongoDB\Driver\Server;

class CommentsRepository implements CommentsRepositoryInterface
{
    public function  addNewComment(Request $request, $recipeId)
    {

        $data = $request->validate(
            [
                'from' => 'required|min:3|max:50',
                'comment' => 'required|min:3|max:1000',
            ]
        );

        return Comment::create(
            [
                'from' => $data['from'],
                'comment' => $data['comment'],
                'approved' => TRUE,
                'recipe_id' => $recipeId,
            ]
        );
    }

    public function updateComment(Request $request, Comment $comment)
    {
        if($request->method() == "PUT"){
            $formType = $request->form_type;
            if($formType == 'show'){
                return $this->show($comment);
            } elseif($formType == 'hide') {
                return $this->hide($comment);
            }
        } else {
            return $this->editExistingComment($request, $comment);
        }
    }

    public function editExistingComment(Request $request, Comment $comment)
    {
        $data = $request->validate(
            [
                'from' => 'required|min:3|max:50',
                'comment' => 'required|min:3|max:1000',
            ]
        );

        return $comment->update(
            [
                'from' => $data['from'],
                'comment' => $data['comment'],
                'approved' => TRUE,
            ]
        );
    }

    public function thumpsComment(Request $request, Comment $comment)
    {
        if($request->method() === "PUT") {
            $this->thumbsDown($comment);
        } elseif ($request->method() === "PATCH") {
            $this->thumbsUp($comment);
        }
    }

    protected function thumbsUp(Comment $comment)
    {

        $userAddress = $_SERVER['REMOTE_ADDR'];
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        $cookies = Cookie::get();

        if($this->check($comment) === FALSE){
            $commentRating = CommentRatingAddress::where('cookie', $cookies['comments_rating_cookie'])->first();

            return $commentRating->update(
                [
                    'rating_comment_id' => 1,
                ]
            );
        }

        $rating = CommentRatingAddress::create(
            [
                'comment_id' => $comment->id,
                'rating_comment_id' => 1,
                'from_ip_address' => $userAddress,
                'user_agent' => $userAgent,
            ]
        );

        Cookie::queue('comments_rating_cookie', $rating->id, '525600');

        return $rating->update(
            [
                'cookie' => $rating->id,
            ]
        );
    }

    protected function thumbsDown(Comment $comment)
    {
        $userAddress = $_SERVER['REMOTE_ADDR'];
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        $cookies = Cookie::get();



        if($this->check($comment) === FALSE){
            $commentRating = CommentRatingAddress::where('cookie', $cookies['comments_rating_cookie'])->first();

            return $commentRating->update(
                [
                    'rating_comment_id' => 2,
                ]
            );
        }
        $rating = CommentRatingAddress::create(
            [
                'comment_id' => $comment->id,
                'rating_comment_id' => 2,
                'from_ip_address' => $userAddress,
                'user_agent' => $userAgent,
            ]
        );

        Cookie::queue('comments_rating_cookie', $rating->id, '525600');

        return $rating->update(
            [
                'cookie' => $rating->id,
            ]
        );
    }

    protected function check(Comment $comment)
    {
        $cookie = $this->checkCookie($comment);
//        $ip = $this->checkIp($comment);
//        $agent = $this->checkAgent($comment);

        if($cookie === FALSE) {
            return false;
        } else {
            return true;
        }
    }

    protected function checkCookie(Comment $comment)
    {
        $cookies = Cookie::get();
        if(empty($cookies['comments_rating_cookie'])){
            return true;
        } else {
            $ratings = CommentRatingAddress::where('cookie', $cookies['comments_rating_cookie'])->get();
            if((int)implode(', ',$ratings->pluck('id')->toArray()) > 0){
                return false;
            } else {
                return true;
            }
        }

    }

    protected function checkIp(Comment $comment)
    {

        $_SERVER['HTTP_USER_AGENT'];
        $ratings = CommentRatingAddress::where('from_ip_address', $_SERVER['REMOTE_ADDR'])->get();
        if((int)implode(', ',$ratings->pluck('id')->toArray()) > 0){
            return false;
        } else {
            return true;
        }
    }

    protected function checkAgent(Comment $comment)
    {
        $ratings = CommentRatingAddress::where('user_agent', $_SERVER['HTTP_USER_AGENT'])->get();
        if((int)implode(', ',$ratings->pluck('id')->toArray()) > 0){
            return false;
        } else {
            return true;
        }
    }

    protected function hide(Comment $comment)
    {
        return $comment->update(
            [
                'approved' => FALSE,
            ]
        );
    }



    protected function show(Comment $comment)
    {
        return $comment->update(
            [
                'approved' => TRUE,
            ]
        );
    }
}
