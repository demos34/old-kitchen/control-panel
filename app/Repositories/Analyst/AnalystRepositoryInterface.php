<?php

namespace App\Repositories\Analyst;

interface AnalystRepositoryInterface
{
    public function getAll();
}
