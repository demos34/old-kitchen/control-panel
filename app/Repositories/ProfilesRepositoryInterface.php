<?php
/**
 * Created by PhpStorm.
 * User: Inso
 * Date: 24.5.2020 г.
 * Time: 12:23
 */

namespace App\Repositories;

use App\Models\Profile;

interface ProfilesRepositoryInterface
{
    public function updateMyProfile(Profile $profile, $request);
    public function getMyProfile($userId);
    public function getAuthUser();
}