<?php
namespace App\Repositories;


use App\Models\IndexTag;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagsRepository implements TagsRepositoryInterface
{
    public function addTag(Request $request)
    {
        $data = $request->validate(
            [
                'name' => 'required|string|max:255|unique:tags',
            ]
        );
        return Tag::create($data);
    }

    public function updateTag(Request $request, Tag $tag)
    {

    }

    public function deleteTag(Tag $tag)
    {

    }

    public function addIndexTag(Request $request)
    {
        $data = $request->validate(
            [
                'name' => 'required|string|max:255|unique:index_tags',
            ]
        );
        $tag = IndexTag::create($data);

        $tag->homeInfos()->attach(1);
    }

    public function updateIndexTag(Request $request, Tag $tag)
    {
        // TODO: Implement updateIndexTag() method.
    }

    public function deleteIndexTag(Tag $tag)
    {
        // TODO: Implement deleteIndexTag() method.
    }
}