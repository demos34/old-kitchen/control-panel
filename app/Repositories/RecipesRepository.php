<?php
namespace App\Repositories;


use App\Models\Product;
use App\Models\Rating;
use App\Models\RatingRecipe;
use App\Models\RatingValue;
use App\Models\Recipe;
use App\Models\TimeToCook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class RecipesRepository implements RecipesRepositoryInterface
{
    public function viewAll()
    {
        return Recipe::orderBy('updated_at', 'DESC')->paginate(5);
    }

    public function getProducts($recipeId)
    {
        return Product::where('recip_id', $recipeId)->get();
    }

    public function getTimeToCook($draftId)
    {
        return TimeToCook::where('draft_recipe_id', $draftId)->get();
    }

    public function increaseViews(Recipe $recipe)
    {
        $views = (int)implode(', ',  $recipe->views()->pluck('views')->toArray());
        $views++;
        $recipe->views()->update(
            [
                'views' => $views,
            ]
        );
    }

    public function getSumOfRatings(Recipe $recipe)
    {
        $ratings = $recipe->ratingRecipes;

        $sum = [];
        foreach ($ratings as $rating) {
            $sum[] = implode(', ', $rating->rating->ratingValues->pluck('value')->toArray());
        }
        $values = array_sum($sum);
        $ratingsCount = RatingRecipe::all()->count();

        if ($ratingsCount > 0) {
            return  $values/$ratingsCount;
        } else {
            return $values;
        }
    }

    public function getAllRatingsValues()
    {
        return RatingValue::with('rating')->get();
    }

    public function getMaxValue()
    {
        return RatingValue::max('value');
    }

    public function setRating(Request $request, Recipe $recipe)
    {


        $cookies = Cookie::get();


        if($this->check($recipe) === FALSE){
            $commentRating = RatingRecipe::where('cookie', $cookies['rating_cookie'])->first();

            return $commentRating->update(
                [
                    'rating_id' => $request->rating_id,
                ]
            );
        }

        $rating = RatingRecipe::create(
            [
                'recipe_id' => $recipe->id,
                'rating_id' => $request->rating_id,
            ]
        );

        Cookie::queue('rating_cookie', $rating->id, '525600');

        return $rating->update(
            [
                'cookie' => $rating->id,
            ]
        );
    }

    public function getLastNRecipes($count)
    {
        return Recipe::orderBy('updated_at', 'DESC')->limit($count)->get();
    }

    protected function check(Recipe $recipe)
    {
        $cookie = $this->checkCookie($recipe);

        if($cookie === FALSE) {
            return false;
        } else {
            return true;
        }
    }

    protected function checkCookie(Recipe $recipe)
    {
        $cookies = Cookie::get();
        if(empty($cookies['rating_cookie'])){
            return true;
        } else {
            $ratings = RatingRecipe::where('cookie', $cookies['rating_cookie'])->get();
            if((int)implode(', ',$ratings->pluck('id')->toArray()) > 0){
                return false;
            } else {
                return true;
            }
        }

    }
}
