<?php
namespace App\Repositories;

use App\User;
use Illuminate\Http\Request;

interface UsersRepositoryInterface
{
    public function updateUser(User $user, $request);

    public function registerUser($request);

    public function changePassword(Request $request);

    public function returnProfile();

    public function returnUser();

    public function returnAuthUser();

}