<?php


namespace App\Repositories;


use App\Models\Home\Tube;
use App\Models\Keyword;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostsRepository implements PostsRepositoryInterface
{
    public function getAllPosts()
    {
        return Post::orderBy('created_at', 'desc')->get();
    }

    public function getAllYouTubePosts()
    {
        return Tube::orderBy('created_at', 'desc')->get();
    }

    public function  getAllTags()
    {
        return Tag::all();
    }

    public function  getAllKeywords()
    {
        return Keyword::all();
    }

    public function storePost(Request $request)
    {
        $validated = self::validatePost($request);
        if($request->has('image'))
        {
            $imagePath = self::storeImage($request);
        } else {
            $imagePath = '/storage/logo/logo.png';
        }

        $post = self::createPost($validated, $imagePath);

        $post->tags()->attach($validated['tags']);
        $post->keywords()->attach($validated['keywords']);

        $post->update(
            [
                'slug' => Str::slug($post->title . '-' . $post->id),
            ]
        );

        return $post;
    }

    public function updatePost(Request $request, Post $post)
    {

        $validated = self::validatePost($request);
        if ($request->has('image')){
            $imagePath = self::storeImage($request);
            self::deleteProductImage($post);
            $post->update(
                [
                    'image' => $imagePath,
                ]
            );
        }
        $post->update(
            [
                'title' => $validated['title'],
                'subtitle' => $validated['subtitle'],
                'body' => $validated['body'],
                'description' => $validated['description'],
            ]
        );

        $post->tags()->sync($validated['tags']);
        $post->keywords()->sync($validated['keywords']);

        return $post;
    }

    public function deletePost(Post $post)
    {
        return $post->delete();
    }

    public function storeTube(Request $request)
    {
        $validated = self::validateTube($request);
        $tube_link = self::getURL($validated['tube_link']);
        $tube = self::createTube($validated, $tube_link);


        $tube->tags()->attach($validated['tags']);
        $tube->keywords()->attach($validated['keywords']);

        $tube->update(
            [
                'slug' => Str::slug($tube->title . '-' . $tube->id),
            ]
        );
        return $tube;
    }

    public function updateTubePost(Request $request, Tube $tube)
    {
        $validated = self::validateTube($request);
        $tube_link = self::getURL($validated['tube_link']);

        $tube->update(
            [
                'title' => $validated['title'],
                'subtitle' => $validated['subtitle'],
                'body' => $validated['body'],
                'description' => $validated['description'],
                'tube_link' => $tube_link,
                'user_id' => Auth::user()->id,
            ]
        );
        $tube->tags()->sync($validated['tags']);
        $tube->keywords()->sync($validated['keywords']);
    }

    public function deleteTubePost(Tube $tube)
    {
        $tube->delete();
    }

    protected static function validatePost(Request $request)
    {
        return $request->validate(
            [
                'title' => 'required|min:3|max:100|string',
                'subtitle' => 'required|min:3|max:100|string',
                'body' => 'required|min:3|max:20000',
                'keywords' => 'array|required|exists:keywords,id',
                'tags' => 'array|required|exists:tags,id',
                'description' => 'required|min:3|max:100|string',
            ]
        );
    }

    protected static function validateTube(Request $request)
    {
        return $request->validate(
            [
                'title' => 'required|min:3|max:100|string',
                'subtitle' => 'required|min:3|max:100|string',
                'body' => 'required|min:3|max:20000',
                'keywords' => 'array|required|exists:keywords,id',
                'tags' => 'array|required|exists:tags,id',
                'description' => 'required|min:3|max:100|string',
                'tube_link' => 'required|min:3|max:500|string',
            ]
        );
    }

    protected static function getURL($url)
    {
        $urlArray = explode('/', $url);
        return 'https://www.youtube.com/embed/' . end($urlArray);
    }

    protected static function createPost($validated, $imagePath)
    {
        return  Post::create(
            [
                'title' => $validated['title'],
                'subtitle' => $validated['subtitle'],
                'body' => $validated['body'],
                'description' => $validated['description'],
                'image' => $imagePath,
                'user_id' => Auth::user()->id,
            ]
        );
    }

    protected static function createTube($validated, $tube_link)
    {
        return Tube::create(
            [
                'title' => $validated['title'],
                'subtitle' => $validated['subtitle'],
                'body' => $validated['body'],
                'description' => $validated['description'],
                'tube_link' => $tube_link,
                'user_id' => Auth::user()->id,
            ]
        );
    }

    protected function storeImage(Request $request)
    {
        $validatedImage = $this->validateImage($request);

        $file = $validatedImage['image']->getClientOriginalName();
        $fileName = pathinfo($file, PATHINFO_FILENAME);
        $fileExtension = pathinfo($file, PATHINFO_EXTENSION);
        $date = date('Y-m-d-H-i-s');
        $fileFullName = $date . '-' . $fileName . '.' . $fileExtension;
        $imagePath = '/storage/posts/' . $fileFullName;
        $request->image->storeAs('posts', $fileFullName, 'public');
        return $imagePath;
    }

    protected function validateImage(Request $request)
    {

        return $request->validate(
            [
                'image' => 'image',
            ]
        );
    }

    protected static function deleteProductImage(Post $post)
    {
        $originalImage = $post->image;
        $array = explode('/', $originalImage);
        $fileName = $array['3'];
        $path = '/public/' . $array['2'] . '/' . $fileName;
        Storage::delete($path);
    }

}
