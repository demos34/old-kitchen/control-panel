<?php

namespace App\Repositories;


use App\Models\Consult\BusinessMessage;
use App\Models\Consult\Client;
use App\Models\Consult\ClientCategoryAntherDish;
use App\Models\Consult\ClientInformation;
use App\Models\Consult\ConsultCategory;
use App\Models\Consult\ConsultMessage;
use App\Models\Consult\ConsultService;
use App\Models\Consult\ConsultStatus;
use App\Models\Consult\ConsultSubcategory;
use App\Models\Consult\ConsultType;
use App\Models\Consult\Spice;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Self_;

class ConsultRepository implements ConsultRepositoryInterface
{
    public function getMessages()
    {
        return ConsultMessage::orderBy('created_at', 'DESC')->paginate(5);
    }

    public function getAllCategories()
    {
        return ConsultCategory::all();
    }

    public function getAllSpices()
    {
        return Spice::all();
    }

    public function getAllStatuses()
    {
        return ConsultStatus::all();
    }

    public function getAllServices()
    {
        return ConsultService::all();
    }

    public function getAllServiceTypes()
    {
        return ConsultType::all();
    }

    public function getAllMessagesByStatus(ConsultStatus $consultStatus, ConsultType $consultType): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        return $consultStatus
            ->clients()
            ->where('consult_type_id', $consultType->id)
            ->orderBy('created_at', 'DESC')
            ->paginate(5);
    }

    public function storeCategory(Request $request, $action, $id = null): array
    {
        if ($action === 'store') {
            $validated = self::validateCategory($request);
            if (self::storeValidatedCategory($validated)) {
                $msg = self::msg('success', 'Added Successfully!');
            } else {
                $msg = self::msg('alert', 'Ooops! Something went wrong!');
            }
        } elseif ($action === 'update') {
            $cat = ConsultCategory::findOrFail($id);
            $validated = self::validateCategory($request);
            if ($cat->update(['name' => $validated['category']])) {
                $msg = self::msg('success', 'Added Successfully!');
            } else {
                $msg = self::msg('alert', 'Ooops! Something went wrong!');
            }
        } elseif ($action === 'subStore') {
            $validated = self::validateSubCategory($request);
            if (self::storeValidatedSubcategory($validated)) {
                $msg = self::msg('success', 'Added Successfully!');
            } else {
                $msg = self::msg('alert', 'Ooops! Something went wrong!');
            }
        } elseif ($action === 'subUpdate') {
            $sub = ConsultSubcategory::findOrFail($id);
            $validated = self::validateSubCategory($request, $sub->consult_category_id);
            if ($sub->update(['name' => $validated['subcategory']])) {
                $msg = self::msg('success', 'Added Successfully!');
            } else {
                $msg = self::msg('alert', 'Ooops! Something went wrong!');
            }
        } else {
            $msg = self::msg('alert', 'Ooops! Something went wrong!');
        }
        return $msg;
    }

    public function storeNewClientForm(Request $request, ConsultType $consultType): bool
    {
        $validatedClient = self::validateClientPersonalInfo($request, $consultType);
        if (!$client = self::storeValidatedClient($validatedClient, $consultType)) return false;
        if($consultType->id == 1) {
            $validatedInfo = self::validateClientInformation($request);
            self::storeValidatedClientInformation($validatedInfo, $client);
            self::storeCategoriesAndSubcategoriesForClient($request, $client);
        } else {
            self::storeBusinessMessage($validatedClient, $client);
        }
        self::sendConsultMessage($client, $consultType);
        $client->consultStatuses()->attach(1);

        return true;
    }

    public function readConsultMessage(ConsultMessage $consultMessage)
    {
        if (!$consultMessage->is_read)
            $consultMessage->update(
                [
                    'is_read' => true,
                ]
            );
    }

    public function updateSpice(Client $client, Request $request): array
    {
        $spice = $request->validate(
            [
                'spice' => 'required|exists:spices,id',
            ]
        );
        $client->clientInformation->update(
            [
                'spice_id' => $spice['spice']
            ]
        );
        return $msg = self::msg('success', 'Успешно променено на ' . $client->clientInformation->spice->name);
    }

    public function storeService(Request $request): array
    {
        $validated = self::validateService($request);
        if (self::storeOrUpdateService($validated)) {
            $msg = self::msg('success', 'Услугата е успешно добавена');
        } else {
            $msg = self::msg('danger', 'Има някакъв проблем! Моля опитайте пак!');
        }
        return $msg;
    }

    public function updateService(Request $request, ConsultService $consultService)
    {
        $validated = self::validateService($request);
        if (self::storeOrUpdateService($request, $consultService)) {
            $msg = self::msg('success', 'Услугата е успешно променена!');
        } else {
            $msg = self::msg('danger', 'Има някакъв проблем! Моля опитайте пак!');
        }
        return $msg;
    }

    public function setOrUpdatePrice(Request $request, ConsultService $consultService)
    {
        $validated = self::validatePrice($request);
        if (self::storeOrUpdatePrice($validated, $consultService)) {
            $msg = self::msg('success', 'Услугата е успешно променена!');
        } else {
            $msg = self::msg('danger', 'Има някакъв проблем! Моля опитайте пак!');
        }
        return $msg;
    }

    protected static function storeBusinessMessage($validated, Client $client)
    {
        BusinessMessage::create(
            [
                'client_id' => $client->id,
                'from' => $client->first_name . ' ' . $client->last_name,
                'company' => $validated['company'],
                'email' => $client->email,
                'phone' => $client->phone,
                'session_id' => $client->session_id,
                'type' => $validated['type'],
                'message' => $validated['info'],
            ]
        );
    }

    protected static function validatePrice(Request $request): array
    {
        return $request->validate(
            [
                'price' => 'required',
            ]
        );
    }

    protected static function storeOrUpdatePrice($validated, ConsultService $consultService)
    {
        if ($consultService->consultPrice === NULL) return $consultService->consultPrice()->create(['price' => $validated['price'],]);
        return $consultService->consultPrice()->update(['price' => $validated['price'],]);
    }

    protected static function validateService(Request $request): array
    {
        return $request->validate(
            [
                'service' => 'required|min:3|max:50|string',
                'type' => 'required|exists:consult_types,id',
            ]
        );
    }

    protected static function storeOrUpdateService($validated, ConsultService $consultService = null)
    {
        if ($consultService === NULL) {
            return ConsultService::create(
                [
                    'service' => $validated['service'],
                    'consult_type_id' => $validated['type'],
                ]
            );
        } else {
            return $consultService->update(
                [
                    'service' => $validated['service'],
                    'consult_type_id' => $validated['type'],
                ]
            );
        }
    }

    protected static function sendConsultMessage(Client $client, ConsultType $consultType)
    {
        $message = $client->first_name . ' ' . $client->last_name . ' Поиска консултация за крайна дата ' . $client->start_date;
        $msg = ConsultMessage::create(
            [
                'client_id' => $client->id,
                'message' => $message,
                'is_read' => false,
                'is_business' => false,
            ]
        );
        if($consultType->id == 2) $msg->update(['is_business' => true,]);
    }


    protected static function storeCategoriesAndSubcategoriesForClient(Request $request, Client $client): array
    {
        $consultCategories = ConsultCategory::all();
        $msg = [];
        foreach ($consultCategories as $category) {
            $cat = $category->name;
            if ($request->has($cat)) {
                $client->consultCategories()->attach($category->id);
                $isAnotherField = false;
                foreach ($request->$cat as $value) {
                    if ($value === $cat) $isAnotherField = true;
                }
                if ($isAnotherField) {
                    $descr = $category->name . '_descr';
                    self::storeInAnotherCategoryTable($client->id, $category->id, $request->$descr);
                    $count = count($request->$cat);
                    $existedElements = [];
                    for ($i = 0; $i < $count - 1; $i++) {
                        $elId = $request->$cat[$i];
                        if (ConsultSubcategory::find($elId)) array_push($existedElements, $elId);
                    }
                    $client->consultSubcategories()->attach($existedElements);
                } else {
                    $validatedSubsId = $request->validate(
                        [
                            $cat => 'required|exists:consult_subcategories,id',
                        ]
                    );
                    foreach ($validatedSubsId as $item) {
                        $client->consultSubcategories()->attach($item);
                    }
                }
            }
        }
        return $msg;
    }

    protected static function storeInAnotherCategoryTable($clientId, $categoryId, $description)
    {

        ClientCategoryAntherDish::create(
            [
                'client_id' => $clientId,
                'consult_category_id' => $categoryId,
                'description' => $description,
            ]
        );
    }

    protected static function storeValidatedClientInformation($validated, Client $client)
    {
        return ClientInformation::create(
            [
                'client_id' => $client->id,
                'spice_id' => $validated['spice'],
                'fav_foods' => $validated['fav_foods'],
                'fav_rest' => $validated['fav_rest'],
                'allergic' => $validated['allergic'],
                'info' => $validated['info'],
            ]
        );
    }

    protected static function validateClientInformation(Request $request): array
    {
        return $request->validate(
            [
                'spice' => 'required|exists:spices,id',
                'fav_foods' => 'nullable|string|min:3|max:200',
                'fav_rest' => 'nullable|string|min:3|max:200',
                'allergic' => 'required|string|min:1|max:200',
                'info' => 'required|string|min:1|max:2000',
            ]
        );
    }

    protected static function validateClientPersonalInfo(Request $request, ConsultType $consultType): array
    {
        if ($consultType->id == 1) {
            return $request->validate(
                [
                    'first_name' => 'required|string|min:3|max:20',
                    'last_name' => 'required|string|min:3|max:20',
                    'country' => 'nullable|min:1|max:30|string',
                    'city' => 'nullable|min:1|max:30|string',
                    'address_one' => 'nullable|min:1|max:200|string',
                    'address_two' => 'nullable|min:1|max:200|string',
                    'email' => 'required|min:3|max:50|email',
                    'phone' => 'required|numeric|min:5',
                    'start_date' => 'required|date||after:today',
                    'company' => 'nullable|min:1|max:255|string',
                    'type' => 'nullable|min:1|max:255|string',
                ]
            );
        } else {
            return $request->validate(
                [
                    'first_name' => 'required|string|min:3|max:20',
                    'last_name' => 'required|string|min:3|max:20',
                    'country' => 'nullable|min:1|max:30|string',
                    'city' => 'nullable|min:1|max:30|string',
                    'address_one' => 'nullable|min:1|max:200|string',
                    'address_two' => 'nullable|min:1|max:200|string',
                    'email' => 'required|min:3|max:50|email',
                    'phone' => 'required|numeric|min:5',
                    'start_date' => 'required|date||after:today',
                    'company' => 'required|min:1|max:255|string',
                    'type' => 'nullable|min:1|max:255|string',
                    'info' => 'required|string|min:1|max:2000',
                ]
            );
        }
    }

    protected static function storeValidatedClient($validatedClient, ConsultType $consultType)
    {
        $session = session()->getId();

        $role = implode(',', auth()->user()->roles()->get()->pluck('role')->toArray());
        if ($role !== 'Developer' && $role !== 'Administrator') {
            if ($client = Client::where('session_id', $session)->first()) {
                $expiry = $client->created_at->addMinutes(30);
                if (now() < $expiry) return false;
                $client->delete();
            }
        }
        return Client::create(
            [
                'first_name' => $validatedClient['first_name'],
                'last_name' => $validatedClient['last_name'],
                'country' => $validatedClient['country'],
                'city' => $validatedClient['city'],
                'address_one' => $validatedClient['address_one'],
                'address_two' => $validatedClient['address_two'],
                'email' => $validatedClient['email'],
                'phone' => $validatedClient['phone'],
                'start_date' => $validatedClient['start_date'],
                'session_id' => $session,
                'consult_type_id' => $consultType->id,
            ]
        );
    }

    protected static function validateCategory(Request $request): array
    {
        return $request->validate(
            [
                'category' => 'required|min:3|max:50|unique:consult_categories,name',
            ]
        );
    }

    protected static function validateSubCategory(Request $request, $categoryId = null): array
    {
        if ($categoryId === NULL) {
            return $request->validate(
                [
                    'subcategory' => 'required|min:3|max:50|unique:consult_subcategories,name',
                    'category' => 'required|exists:consult_categories,id',
                ]
            );
        }
        return $request->validate(
            [
                'subcategory' => 'required|min:3|max:50|unique:consult_subcategories,name',
            ]
        );
    }

    protected static function storeValidatedCategory($validated)
    {
        return ConsultCategory::create(
            [
                'name' => $validated['category'],
            ]
        );
    }

    protected static function storeValidatedSubcategory($validated)
    {
        return ConsultSubcategory::create(
            [
                'name' => $validated['subcategory'],
                'consult_category_id' => $validated['category'],
            ]
        );
    }

    protected static function msg($session, $message): array
    {
        return [
            'session' => $session,
            'message' => $message,
        ];
    }
}
