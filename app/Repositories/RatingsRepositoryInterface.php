<?php

namespace App\Repositories;

use App\Models\Rating;
use Illuminate\Http\Request;

interface RatingsRepositoryInterface
{
    public function addNewRating(Request $request);

    public function updateRating(Request $request, Rating $rating);

    public function  getAllRatings();

    public function deleteRating(Rating $rating);
}
