<?php

namespace App\Repositories;

use App\Models\Consult\Client;
use App\Models\Consult\ConsultCategory;
use App\Models\Consult\ConsultMessage;
use App\Models\Consult\ConsultService;
use App\Models\Consult\ConsultStatus;
use App\Models\Consult\ConsultType;
use Illuminate\Http\Request;

interface ConsultRepositoryInterface
{
    public function getMessages();

    public function getAllCategories();

    public function getAllSpices();

    public function getAllStatuses();

    public function getAllServices();

    public function getAllServiceTypes();

    public function getAllMessagesByStatus(ConsultStatus $consultStatus, ConsultType $consultType);

    public function storeCategory(Request $request, $action, $id = null);

    public function storeNewClientForm(Request $request, ConsultType $consultType);

    public function readConsultMessage(ConsultMessage $consultMessage);

    public function updateSpice(Client $client, Request $request);

    public function storeService(Request $request);

    public function updateService(Request $request, ConsultService $consultService);

    public function setOrUpdatePrice(Request $request, ConsultService $consultService);
}
