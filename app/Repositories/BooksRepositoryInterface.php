<?php

namespace App\Repositories;

use App\Models\Book;
use App\Models\BuyFrom;
use Illuminate\Http\Request;

interface BooksRepositoryInterface
{
    public function addNewBook(Request $request);

    public function editExistingBook(Request $request, Book $book);

    public function deleteBook(Book $book);

    public function addPublisher(Request $request);

    public function editExistingPublisher(Request $request, BuyFrom $buyFrom);

    public function deletePublisher(BuyFrom $buyFrom);

    public function getBookBySlug($slug);

    public function getAllBooks();

    public function getPublisher($slug);

    public function getAllPublishers();
}
