<?php

namespace App\Repositories;

use App\Models\Home\Tube;
use App\Models\Post;
use Illuminate\Http\Request;

interface PostsRepositoryInterface
{
    public function getAllPosts();

    public function getAllYouTubePosts();

    public function  getAllTags();

    public function  getAllKeywords();

    public function storePost(Request $request);

    public function updatePost(Request $request, Post $post);

    public function deletePost(Post $post);

    public function storeTube(Request $request);

    public function updateTubePost(Request $request, Tube $tube);

    public function deleteTubePost(Tube $tube);
}
