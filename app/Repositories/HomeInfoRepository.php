<?php
namespace App\Repositories;

use App\Models\HomeInfo;
use App\Models\Message;
use App\Models\MessageType;
use App\Models\Partner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HomeInfoRepository implements HomeInfoRepositoryInterface
{

    public function update(Request $request)
    {
        $data = $request->validate(
            [
                'logo_image_url' => 'image',
                'top_home_text' => 'required|string',
                'top_image_url' => 'image',
                'middle_home_text' => 'required|string',
                'middle_image_url' => 'image',
                'recipes_count' => 'required|integer',
            ]
        );

        $homeInfo = HomeInfo::find(1);

        if($request['logo_image_url'] === NULL){
            $logoImagePath = $homeInfo->logo_image_url;
        } else {
            $logoImage = $request->logo_image_url->store('logo', 'public');
            $logoImagePath = '/storage/'.$logoImage;
        }
        if($request['top_image_url'] === NULL){
            $topImagePath = $homeInfo->top_image_url;
        } else {
            $topImage = $request->top_image_url->store('home', 'public');
            $topImagePath = '/storage/' . $topImage;
        }
        if($request['middle_image_url'] === NULL){
            $middleImagePath = $homeInfo->middle_image_url;
        } else {
            $middleImage = $request->middle_image_url->store('home', 'public');
            $middleImagePath = '/storage/'.$middleImage;
        }

        return $homeInfo->update([
            'logo_image_url' => $logoImagePath,
            'top_home_text' => $data['top_home_text'],
            'top_image_url' => $topImagePath,
            'middle_home_text' => $data['middle_home_text'],
            'middle_image_url' => $middleImagePath,
            'recipes_count' => $data['recipes_count'],
        ]);
    }

    public function getAllPartners()
    {
        return Partner::all();
    }

    public function storePartner(Request $request)
    {
        return self::partners($request);
    }

    public function updatePartner(Request $request, Partner $partner)
    {
        if(self::partnersUpdate($request, $partner)){
            $msg = self::msg('success', 'Записът е променен успешно!');
        } else {
            $msg = self::msg('alert', 'Възникна някакъв проблем! Моля опитайте пак!');
        }
        return $msg;
    }

    public function delete(Partner $partner)
    {
        self::deleteImage($partner);

        if($partner->delete()){
            $msg = self::msg('success', 'Записът е изтрит успешно!');
        } else {
            $msg = self::msg('alert', 'Възникна някакъв проблем! Моля опитайте пак!');
        }
        return $msg;
    }

    public function getMessages(MessageType $messageType)
    {

    }

    protected static function partners(Request $request)
    {
        $validated = self::validatePartnerToStore($request);
        $imagePath = self::storeImage($request);
        self::insertPartnersIntoDB($validated, $imagePath);
//        $msg = self::msg('success', 'Записът е добавен успешно!');
        if(self::insertPartnersIntoDB($validated, $imagePath)){
            $msg = self::msg('success', 'Записът е добавен успешно!');
        } else {
            $msg = self::msg('alert', 'Възникна някакъв проблем! Моля опитайте пак!');
        }

        return $msg;
    }

    protected static function partnersUpdate(Request $request, Partner $partner)
    {
        $title = self::checkPartnerTitle($request, $partner);
        $slug = self::checkPartnerSlug($request, $partner);
        $validated = self::validateToUpdate($request);
        if ($request->has('image')){
            $imagePath = self::storeImage($request);
            self::deleteImage($partner);
            $partner->update(
                [
                    'image' => $imagePath,
                ]
            );

        }

        return $partner->update(
            [
                'title' => $title['title'],
                'slug' => $slug['slug'],
                'description' => $validated['description'],
                'url' => $validated['url'],
            ]
        );
    }

    protected static function checkPartnerTitle(Request $request, Partner $partner)
    {
//        dd($request->all());
        if (strtolower($request->title) == strtolower($partner->title)){
            $title = $request->validate(
                [
                    'title' => 'required|string|min:3|max:100',
                ]
            );
        } else {
            $title = $request->validate(
                [
                    'title' => 'required|string|min:3|max:100|unique:partners',
                ]
            );
        }

        return $title;
    }

    protected static function checkPartnerSlug(Request $request, Partner $partner)
    {
        if (strtolower($request->slug) == strtolower($partner->slug)){
            $slug = $request->validate(
                [
                    'slug' => 'required|string|min:3|max:100',
                ]
            );
        } else {
            $slug = $request->validate(
                [
                    'slug' => 'required|string|min:3|max:100|unique:partners',
                ]
            );
        }

        return $slug;
    }

    protected static function validateToUpdate(Request $request)
    {
        return $request->validate(
            [
                'url' => 'max:150',
                'description' => 'max:2000',
            ]
        );
    }

    protected static function validatePartnerToStore(Request $request)
    {
        return $request->validate(
            [
                'title' => 'required|string|min:3|max:100|unique:partners',
                'slug' => 'required|string|min:3|max:100|unique:partners',
                'url' => 'max:150',
                'description' => 'max:2000',
            ]
        );
    }

    protected static function storeImage(Request $request)
    {
        $validatedImage = self::validateImage($request);

        $file = $validatedImage['image']->getClientOriginalName();
        $fileName = pathinfo($file, PATHINFO_FILENAME);
        $fileExtension = pathinfo($file, PATHINFO_EXTENSION);
        $date = date('Y-m-d-H-i-s');
        $fileFullName = $date . '-' . $fileName . '.' . $fileExtension;
        $imagePath = '/storage/partners/' . $fileFullName;
        $request->image->storeAs('partners', $fileFullName, 'public');

        return $imagePath;
    }

    private static function validateImage(Request $request)
    {
        return $request->validate(
            [
                'image' => 'image',
            ]
        );
    }

    protected static function insertPartnersIntoDB($validated, $imagePath)
    {
        return Partner::create(
            [
                'title' => $validated['title'],
                'slug' => $validated['slug'],
                'image' => $imagePath,
                'description' => $validated['description'],
                'url' => $validated['url'],
            ]
        );
    }

    protected static function msg($session, $message)
    {
        return [
            'session' => $session,
            'message' => $message,
        ];
    }

    protected static function deleteImage(Partner $partner)
    {
        $originalImage = $partner->image;
        $array = explode('/', $originalImage);
        $path = '/public/' . $array['2'] . '/' .$array['3'];
        Storage::delete($path);
    }
}
