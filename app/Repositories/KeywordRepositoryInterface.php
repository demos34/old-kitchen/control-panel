<?php
/**
 * Created by PhpStorm.
 * User: Inso
 * Date: 4.6.2020 г.
 * Time: 10:49
 */

namespace App\Repositories;

use App\Models\Keyword;
use Illuminate\Http\Request;

interface KeywordRepositoryInterface
{
    public function addNewKeyword(Request $request);

    public function updateExistingKeyword(Keyword $keyword, Request $request);

    public function deleteExistingKeyword(Keyword $keyword);

    public function getAllKeywordsById($id);

    public function getAllKeywords();
}