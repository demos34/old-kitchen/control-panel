<?php
/**
 * Created by PhpStorm.
 * User: Inso
 * Date: 25.5.2020 г.
 * Time: 20:16
 */

namespace App\Repositories;

use App\Models\MessageType;
use App\Models\Partner;
use Illuminate\Http\Request;

interface HomeInfoRepositoryInterface
{
    public function update(Request $request);

    public function getAllPartners();

    public function storePartner(Request $request);

    public function updatePartner(Request $request, Partner $partner);

    public function delete(Partner $partner);

    public function getMessages(MessageType $messageType);
}
