<?php
/**
 * Created by PhpStorm.
 * User: Inso
 * Date: 8.6.2020 г.
 * Time: 14:22
 */

namespace App\Repositories;

use App\Models\Comment;
use App\Models\Recipe;
use Illuminate\Http\Request;

interface CommentsRepositoryInterface
{
    public function addNewComment(Request $request, $recipeId);

    public function updateComment(Request $request, Comment $comment);

    public function editExistingComment(Request $request, Comment $comment);

    public function thumpsComment(Request $request, Comment $comment);

}
