<?php

namespace App\Repositories;


use App\Models\BackupComment;
use App\Models\BackupRatingRecipe;
use App\Models\Comment;
use App\Models\DraftProduct;
use App\Models\DraftRecipe;
use App\Models\Image;
use App\Models\Product;
use App\Models\RatingRecipe;
use App\Models\Recipe;
use App\Models\TimeToCook;
use App\Models\ViewsBackup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DraftsRepository implements DraftsRepositoryInterface
{
    public function addNewDraft(Request $request)
    {

        $data = $request->validate(
            [
                'title' => 'required|max:255',
                'subtitle' => 'required|max:255',
                'slug' => 'required|max:255',
                'types' => 'required|max:255',
                'tags' => 'required|max:255',
                'meta_description' => 'required|max:255',
                'how_to' => 'required',
                'author' => 'required|max:255',
                'image' => 'required|image',
            ]
        );

        $draft = DraftRecipe::create(
            [
                'title' => $data['title'],
                'subtitle' => $data['subtitle'],
                'meta_description' => $data['meta_description'],
                'how_to' => $data ['how_to'],
            ]
        );

        ViewsBackup::create(
            [
                'draft_recipe_id' => $draft->id,
                'views' => 1,
            ]
        );


        $days = $request['days'];
        if ($request['hours'] > 23) {
            $hours = 23;
        } elseif ($request['hours'] < 0) {
            $hours = 0;
        } else {
            $hours = $request['hours'];
        }
        if ($request['minutes'] > 59) {
            $minutes = 59;
        } elseif ($request['minutes'] < 0) {
            $minutes = 0;
        } else {
            $minutes = $request['minutes'];
        }

        TimeToCook::create(
            [
                'draft_recipe_id' => $draft->id,
                'days' => $days,
                'hours' => $hours,
                'minute' => $minutes,
            ]
        );

        $image = $request['image']->store('uploads', 'public');

        $imagePath = '/storage/' . $image;

        $userId = Auth::user()->id;

        $createdImage = Image::create(
            [
                'url' => $imagePath,
                'author' => $data['author'],
            ]
        );

        $createdImageId = $createdImage->id;
        $dataToInsert = array('img_id' => $createdImageId, 'user_id' => $userId);
        DB::table('uploaded_img_from_users')->insert($dataToInsert);

        $arrayTitle = explode(' ', Str::lower($draft->title));
        $slugifiedTitle = implode('-', $arrayTitle);

        $slug = $draft->id . '-' . $slugifiedTitle;

        $urlSlug = $draft->id . '-' . $data['slug'];
        $draft->update(
            [
                'slug' => $urlSlug,
                'lat_slug' => $slug,
            ]
        );

        if (isset($request->keywords)) {
            $draft->keywords()->attach($request->keywords);
        }

        $queryDraftImage = array('draft_id' => $draft->id, 'img_id' => $createdImageId);
        DB::table('draft_images')->insert($queryDraftImage);

        $products = explode(';', $request->products);

        foreach ($products as $product) {
            DraftProduct::create(
                [
                    'draft_recipe_id' => $draft->id,
                    'product' => $product,
                ]
            );
        }

        if (isset($request->types)) {
            $types = $request->types;
            foreach ($types as $type) {
                $queryDraftType = array('draft_id' => $draft->id, 'type_id' => $type);
                DB::table('draft_type')->insert($queryDraftType);
            }
        }

        if (isset($request->tags)) {
            $tags = $request->tags;
            $draft->tags()->attach($tags);
        }

        $queryDraftUser = array('draft_id' => $draft->id, 'user_id' => $userId);
        DB::table('draft_user')->insert($queryDraftUser);

        $draft->publishedDraft()->create(
            [
                'is_published' => 0,
            ]
        );

        $draft->isReadyToPublish()->create(
            [
                'is_ready_to_publish' => 0,
            ]
        );

        return $draft->slug;
    }

    public function updateExistingDraft(Request $request, DraftRecipe $draftRecipe)
    {
        $data = $request->validate(
            [
                'title' => 'required|max:255',
                'subtitle' => 'required|max:255',
                'slug' => 'required|max:255',
                'types' => 'required|max:255',
                'tags' => 'nullable|max:255',
                'meta_description' => 'required|max:255',
                'how_to' => 'required',
                'author' => 'required|max:255',
            ]
        );

        $draftRecipe->update(
            [
                'title' => $data['title'],
                'subtitle' => $data['subtitle'],
                'meta_description' => $data['meta_description'],
                'how_to' => $data ['how_to'],
            ]
        );


        $days = $request['days'];
        if ($request['hours'] > 23) {
            $hours = 23;
        } elseif ($request['hours'] < 0) {
            $hours = 0;
        } else {
            $hours = $request['hours'];
        }
        if ($request['minutes'] > 59) {
            $minutes = 59;
        } elseif ($request['minutes'] < 0) {
            $minutes = 0;
        } else {
            $minutes = $request['minutes'];
        }

        $draftRecipe->timeToCook()->update(
            [
                'days' => $days,
                'hours' => $hours,
                'minute' => $minutes,
            ]
        );

        $userId = Auth::user()->id;



        if($recipe = Recipe::where('draft_id', $draftRecipe->id)->first()) {
            $recipe->update(
                [
                    'title' => $data['title'],
                    'subtitle' => $data['subtitle'],
                    'meta_description' => $data['meta_description'],
                    'how_to' => $data ['how_to'],
                ]
            );
        }

        if ($request['image'] === NULL) {
            true;
        } else {
            $image = $request['image']->store('uploads', 'public');

            $imagePath = '/storage/' . $image;

            $createdImage = Image::create(
                [
                    'url' => $imagePath,
                    'author' => $data['author'],
                ]
            );

            $createdImageId = $createdImage->id;
            $dataToInsert = array('img_id' => $createdImageId, 'user_id' => $userId);
            DB::table('uploaded_img_from_users')->insert($dataToInsert);

            $draftRecipe->images()->sync($createdImageId);
            if($recipe) {
                $recipe->images()->sync($createdImageId);
            }

            $queryDraftImage = array('draft_id' => $draftRecipe->id, 'img_id' => $createdImageId);
            DB::table('draft_images')->update($queryDraftImage)->where('draft_id');
        }


        $arrayTitle = explode(' ', Str::lower($draftRecipe->title));
        $slugifiedTitle = implode('-', $arrayTitle);

        $slug = $draftRecipe->id . '-' . $slugifiedTitle;

        $urlSlug = $draftRecipe->id . '-' . $data['slug'];

        $draftRecipe->update(
            [
                'slug' => $urlSlug,
                'lat_slug' => $slug,
            ]
        );

        if (isset($request->keywords)) {
            $draftRecipe->keywords()->sync($request->keywords);
            if($recipe){
                $recipe->keywords()->sync($request->keywords);
            }
        }

        $products = explode(';', $request->products);

        if($recipe){
            $recipeProducts = Product::where('recip_id', $recipe->id)->get();

            foreach ($recipeProducts as $recipeProduct){
                $recipeProduct->delete();
            }
        }

        $draftRecipe->draftProducts()->delete();

        foreach ($products as $product) {
            DraftProduct::create(
                [
                    'draft_recipe_id' => $draftRecipe->id,
                    'product' => $product,
                ]
            );

            if($recipe){
                Product::create(
                    [
                        'recip_id' => $recipe->id,
                        'product' => $product,
                    ]
                );
            }
        }

        if (isset($request->types)) {
            $types = $request->types;
            $draftRecipe->types()->sync($types);
            if($recipe){
                $recipe->types()->sync($types);
            }
        }

        if (isset($request->tags)) {
            $tags = $request->tags;
            $draftRecipe->tags()->sync($tags);
            if($recipe){
                $recipe->tags()->sync($tags);
            }
        }

//        $draftRecipe->publishedDraft()->update(
//            [
//                'is_published' => 0,
//            ]
//        );
//
//        $draftRecipe->isReadyToPublish()->update(
//            [
//                'is_ready_to_publish' => 0,
//            ]
//        );

        return $draftRecipe->slug;
    }

    public function publishDraftRecipe(Request $request, DraftRecipe $draftRecipe)
    {
        $this->deleteRecipe($draftRecipe);

        if ($request->set == 0) {
            $draftRecipe->isReadyToPublish()->update(
                [
                    'is_ready_to_publish' => $request->set,
                ]
            );
        } else {
            $imageId = (int)implode(', ', $draftRecipe->images->pluck('id')->toArray());
            $tags = $draftRecipe->tags->pluck('id')->toArray();
            $type = $draftRecipe->types->pluck('id')->toArray();
            $keywords = $draftRecipe->keywords->pluck('id')->toArray();
            $products = $draftRecipe->draftProducts->pluck('product')->toArray();
            $backupComments = $draftRecipe->backupComments;
            $backupRatings = $draftRecipe->backupRatingRecipes;

            $publisherId = (int)implode(', ', $draftRecipe->users->pluck('id')->toArray());

            $recipe = Recipe::create(
                [
                    'title' => $draftRecipe->title,
                    'subtitle' => $draftRecipe->subtitle,
                    'meta_description' => $draftRecipe->meta_description,
                    'slug' => $draftRecipe->slug,
                    'lat_slug' => $draftRecipe->lat_slug,
                    'how_to' => $draftRecipe->how_to,
                    'draft_id' => $draftRecipe->id,
                ]
            );

            foreach ($backupRatings as $backupRating)
            {
                RatingRecipe::create(
                    [
                        'rating_id' => $backupRating->rating_id,
                        'recipe_id' => $recipe->id,
                        'comment' => 'NULL',
                        'cookie' => $backupRating->cookie,
                    ]
                );
            }

            foreach ($backupComments as $backupComment)
            {
                Comment::create(
                    [
                        'from' => $backupComment->from,
                        'comment' => $backupComment->comment,
                        'recipe_id' => $recipe->id,
                        'approved' => $backupComment->approved,
                    ]
                );
            }

            $recipe->views()->create(
                [
                    'views' => $draftRecipe->viewsBackup->views,
                ]
            );

            $recipe->images()->attach($imageId);
            $recipe->tags()->attach($tags);
            $recipe->types()->attach($type);
            $recipe->keywords()->attach($keywords);
            $recipe->users()->attach($publisherId);
            foreach ($products as $product) {
                Product::create(
                    [
                        'recip_id' => $recipe->id,
                        'product' => $product,
                    ]
                );
            }
        }

        return $draftRecipe->PublishedDraft()->update(
            [
                'is_published' => $request->set,
            ]
        );
    }

    public function publishManyDraftRecipes(Request $request, $draftRecipes)
    {

    }

    public function destroyDraftRecipe(DraftRecipe $draftRecipe)
    {

    }

    public function preparingToPublish(Request $request, DraftRecipe $draftRecipe)
    {
        $this->deleteRecipe($draftRecipe);
        if ($request->set == 0) {
            $draftRecipe->PublishedDraft()->update(
                [
                    'is_published' => $request->set,
                ]
            );
        }

        return $draftRecipe->isReadyToPublish()->update(
            [
                'is_ready_to_publish' => $request->set,
            ]
        );
    }

    protected function deleteRecipe(DraftRecipe $draftRecipe)
    {
        $recipe = Recipe::where('draft_id', $draftRecipe->id)->first();
        $backup = $draftRecipe->viewsBackup;

        if ($recipe === NULL) {
            return true;
        } else {

            $comments = $recipe->comments;
            $ratings = $recipe->ratingRecipes;
            foreach ($draftRecipe->backupRatingRecipes as $backupRating)
            {
                $backupRating->delete();
            }

            foreach ($ratings as $rating)
            {
                BackupRatingRecipe::create(
                    [
                        'rating_id' => $rating->rating_id,
                        'draft_recipe_id' => $draftRecipe->id,
                        'cookie' => $rating->cookie,
                    ]
                );
            }

            foreach ($draftRecipe->backupComments as $backupComment)
            {
                $backupComment->delete();
            }

            foreach ($comments as $comment)
            {
                BackupComment::create(
                    [
                        'draft_recipe_id' => $draftRecipe->id,
                        'from' => $comment->from,
                        'comment' => $comment->comment,
                        'approved' => $comment->approved,
                    ]
                );
            }

            $views = (int)implode(', ', $recipe->views->pluck('views')->toArray());
            $backup->update(['views' => $views,]);
            $recipe->views()->delete();
            $recipe->comments()->delete();
            $recipe->ratingRecipes()->delete();

            return $recipe->delete();
        }
    }
}
