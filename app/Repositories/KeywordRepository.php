<?php
namespace App\Repositories;


use App\Models\Keyword;
use Illuminate\Http\Request;

class KeywordRepository implements KeywordRepositoryInterface
{
    public function addNewKeyword(Request $request)
    {
        $data = $request->validate(
            [
                'name' => 'required|min:3|max:255|unique:keywords',
                'eng_name' => 'required|min:3|max:255|unique:keywords',
            ]
        );

        return Keyword::create($data);

    }

    public function updateExistingKeyword(Keyword $keyword, Request $request)
    {

    }

    public function deleteExistingKeyword(Keyword $keyword)
    {
        return $keyword->delete();
    }

    public function getAllKeywordsById($id)
    {

    }

    public function getAllKeywords()
    {
        return Keyword::all();
    }
}