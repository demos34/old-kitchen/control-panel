<?php
namespace App\Repositories;


use App\Models\Traffic\Traffic;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class TrafficRepository implements TrafficRepositoryInterface
{
    public function getRemoteAddress($action)
    {

        /*
         * actions:
         * index /class get
         * create /class/create get
         * store /class post
         * show /class/id get
         * edit /class/id/edit get
         * update /class/ id put
         * destroy /class/id delete
         */
        if(isset(Auth::user()->id)) {
            $userId = Auth::user()->id;
            $username = Auth::user()->username;
            $userRole = Auth::user()->roles->first()->role;
        } else {
            $userId = 'Guest';
            $username = 'Guest';
            $userRole = 'Guest';
        }

        $remoteAddress = $_SERVER['REMOTE_ADDR'];
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        if(!isset($_SERVER['HTTP_COOKIE'])){
            $httpCookie = $_SERVER['HTTP_COOKIE'];
        } else {
            $httpCookie = 'not set yet';
        }
        if(!empty(Session::previousUrl())){
            $previousURL = Session::previousUrl();
        } else {
            $previousURL = 'not set url';
        }

        $method = 'method';
        $traffic = $_SERVER['REQUEST_URI'];
        $args = explode("/", $traffic);
        $directory = $args['1'];
        if(count($args) < 3) {
            $directory = 'home';
            $class = 'HomeController';
        } else {
            $class = ucfirst($args['2']) . 'Controller';
        }
        $id = '0';

        if($action == 'index'){
            $method = 'get';
        } elseif ($action == 'create'){
            $method = 'get';
        } elseif ($action == 'store'){
            $method = 'post';
        } elseif ($action =='show'){
            $method = 'get';
        } elseif ($action == 'edit') {
            $method = 'get';
            $this->getId($args);
        } elseif ($action == 'update'){
            $method = 'patch';
            $this->getId($args);
        } elseif ($action == 'destroy') {
            $method = 'delete';
            $this->getId($args);
        }

        return Traffic::create(
            [
                'username' => $username,
                'user_id' => $userId,
                'user_role' =>  $userRole,
                'remote_address' => $remoteAddress,
                'user_agent' => $userAgent,
                'http_cookie' =>  $httpCookie,
                'previous_url' => $previousURL,
                'method' => $method,
                'request_uri' => $traffic,
                'directory' => $directory,
                'class' => $class,
                'action' => $action,
                'given_id_in_action' => $id
            ]
        );
    }

    protected function getId(array $args)
    {
        if($args['1'] == 'partners' || $args['1'] == 'posts'){
            $id = $args['2'];
        } else {
            $id = $args['3'];
        }
        return $id;
    }
}
