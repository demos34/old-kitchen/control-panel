<?php

namespace App\Repositories;


use App\Models\Image;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProfilesRepository implements ProfilesRepositoryInterface
{
    public function updateMyProfile(Profile $profile, $request)
    {
        $user = $this->getAuthUser();

        if($request->email != $user->email){
            $email = $request->validate(
                [
                    'email' => 'required|min:5|max:100|unique:users'
                ]
            );
            $user->update(
                [
                    'email' => $email['email'],
                ]
            );
        }

        if($user->username == $request->username)
        {
            $data = $request->validate(
                [
                    'first_name' => 'required|min:3|max:50|',
                    'second_name' => 'required|min:3|max:50|',
                    'third_name' => 'required|min:3|max:50|',
                ]
            );
            $profile->update($data);
        } else {
            $data = $request->validate(
                [
                    'username' => 'required|min:3|max:50|unique:users',
                    'first_name' => 'required|min:3|max:50|',
                    'second_name' => 'required|min:3|max:50|',
                    'third_name' => 'required|min:3|max:50|',
                ]
            );
            $profile->update([
                'first_name' => $data['first_name'],
                'second_name' => $data['second_name'],
                'third_name' => $data['third_name'],
            ]);
            $user->update([
                'username' => $data['username'],
            ]);
        }

        if($request['image'] !== NULL){
            $image = $request['image']->store('uploads', 'public');
            $imagePath = '/storage/' . $image;
            $createdImage = Image::create(
                [
                    'url' => $imagePath,
                ]
            );
            $createdImageId = $createdImage->id;
            $profile->images()->sync($createdImageId);
            $dataToInsert = array('img_id' => $createdImageId, 'user_id' => $user->id);
            DB::table('uploaded_img_from_users')->insert($dataToInsert);
        }
    }

    public function getMyProfile($userId)
    {
        return Profile::findOrFail($userId);
    }

    public function getAuthUser()
    {
        return Auth::user();
    }
}
