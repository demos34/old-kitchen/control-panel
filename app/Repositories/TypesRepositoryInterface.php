<?php
namespace App\Repositories;

use App\Models\Type;

interface TypesRepositoryInterface
{
    public function addNewType($request);

    public function update(Type $type, $request);

    public function destroy(Type $type);

    public function getAllByType(Type $type);
}