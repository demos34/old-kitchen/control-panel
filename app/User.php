<?php

namespace App;

use App\Models\Administrator\AnalystCustom;
use App\Models\Administrator\AnalyzeCustomRecord;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role');
    }

    public function profile()
    {
        return $this->hasOne('App\Models\Profile');
    }

    public function hasAnyRoles($roles){
        if($this->roles()->whereIn('role', $roles)->first()){
            return true;
        }
        return false;
    }

    public function hasRole($role){
        if($this->roles()->where('role', $role)->first()){
            return true;
        }
        return false;
    }

    public function recipes()
    {
        return $this->belongsToMany('App\Models\Recipe');
    }

    public function drafts()
    {
        return $this->belongsToMany('App\Models\Draft', 'draft_user', 'user_id', 'draft_id');
    }

    public function images()
    {
        return $this->hasMany('App\Models\Image');
    }

    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }

    public function tubes()
    {
        return $this->hasMany('App\Models\Home\Tube');
    }

    public function analystCustom(): HasMany
    {
        return $this->hasMany(AnalystCustom::class);
    }

    public function analyzeCustomRecords(): HasMany
    {
        return $this->hasMany(AnalyzeCustomRecord::class);
    }

}
