<?php

namespace App\Http\Controllers\Roles;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Repositories\RolesRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    private $trafficRepository;
    /**
     * @var RolesRepositoryInterface
     */
    private $rolesRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository, RolesRepositoryInterface $rolesRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->rolesRepository = $rolesRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);
        $roles = Role::all();
        return view('roles.roles.index')->with('roles', $roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'create';
        $this->trafficRepository->getRemoteAddress($action);
        return view('roles.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);
        $newRole = $this->rolesRepository->addNewRole($request);
        return redirect()->route('roles.roles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);
        return $this->index();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $action = 'edit';
        $this->trafficRepository->getRemoteAddress($action);
        return view('roles.roles.edit')->with('role', $role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);
        $roles = $this->rolesRepository->updateRole($role, $request);

        return redirect()->route('roles.roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        $this->rolesRepository->deleteRole($role);

        return redirect()->route('roles.roles.index');
    }
}
