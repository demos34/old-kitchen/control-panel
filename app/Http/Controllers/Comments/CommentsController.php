<?php

namespace App\Http\Controllers\Comments;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Recipe;
use App\Repositories\CommentsRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CommentsController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var CommentsRepositoryInterface
     */
    private $commentsRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                CommentsRepositoryInterface $commentsRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->commentsRepository = $commentsRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        return false;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'create';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        $previousUrl = Session::previousUrl();
        $urlArray = explode('/', $previousUrl);
        $slug = end($urlArray);
        $recipe = Recipe::where('slug', $slug)->first();

        $comment = $this->commentsRepository->addNewComment($request, $recipe->id);

        return redirect()->route('recipes.recipes.show', $slug);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        $action = 'edit';
        $this->trafficRepository->getRemoteAddress($action);

        return view('comments.comments.edit')->with('comment', $comment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);
        $update = $this->commentsRepository->updateComment($request, $comment);

        if ($update === TRUE) {
            $request->session()->flash('success', 'The approve status is changed successfully!');
        } else {
            $request->session()->flash('danger', 'There was an error updating the approve status');
        }
        return redirect()->route('recipes.recipes.show', $comment->recipe->slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }
}
