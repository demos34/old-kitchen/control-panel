<?php

namespace App\Http\Controllers\Comments;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\ratingCommentsValue;
use App\Models\Recipe;
use App\Repositories\CommentsRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class RatingCommentsValuesController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var CommentsRepositoryInterface
     */
    private $commentsRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                CommentsRepositoryInterface $commentsRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->commentsRepository = $commentsRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'create';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ratingCommentsValue  $ratingCommentsValue
     * @return \Illuminate\Http\Response
     */
    public function show(ratingCommentsValue $ratingCommentsValue)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ratingCommentsValue  $ratingCommentsValue
     * @return \Illuminate\Http\Response
     */
    public function edit(ratingCommentsValue $ratingCommentsValue)
    {
        $action = 'edit';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Comment  $value
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $value)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);


        $recipe = Recipe::where('id', $value->recipe_id)->first();
        $cookies = Cookie::get();
        if(empty($cookies['laravel_cookie_consent'])){
            return redirect()->route('recipes.recipes.show', $recipe->slug);
        } else {
           Cookie::queue('1', '1', '1');
        }

        $this->commentsRepository->thumpsComment($request, $value);

        return redirect()->route('recipes.recipes.show', $recipe->slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ratingCommentsValue  $ratingCommentsValue
     * @return \Illuminate\Http\Response
     */
    public function destroy(ratingCommentsValue $ratingCommentsValue)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }
}
