<?php

namespace App\Http\Controllers\Recipes;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Recipe;
use App\Models\TimeToCook;
use App\Repositories\RecipesRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class RecipesController extends Controller
{

    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var RecipesRepositoryInterface
     */
    private $recipesRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                RecipesRepositoryInterface $recipesRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->recipesRepository = $recipesRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);


        $recipes = $this->recipesRepository->viewAll();
        return view('recipes.recipes.index')->with('recipes', $recipes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'create';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Recipe  $recipes
     * @return \Illuminate\Http\Response
     */
    public function show(Recipe $recipes)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);
        $this->recipesRepository->increaseViews($recipes);

        $average = $this->recipesRepository->getSumOfRatings($recipes);

        $ratingWithValues = $this->recipesRepository->getAllRatingsValues();

        $max = $this->recipesRepository->getMaxValue();

        $time = $this->recipesRepository->getTimeToCook($recipes->draft_id);


        $products = $this->recipesRepository->getProducts($recipes->id);

//        $cookie = (int)Cookie::get()['comments_rating_cookie'];
        $cookie = 'cookie';

        return view('recipes.recipes.show')->with(
            [
                'draft' => $recipes,
                'products' => $products,
                'time' => $time,
                'cookie' => $cookie,
                'average' => $average,
                'ratingValues' => $ratingWithValues,
                'max' => $max,
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit(Recipe $recipe)
    {
        $action = 'edit';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recipe $recipe)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Recipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recipe $recipe)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }
}
