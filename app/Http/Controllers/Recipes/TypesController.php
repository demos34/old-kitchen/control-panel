<?php

namespace App\Http\Controllers\Recipes;

use App\Http\Controllers\Controller;
use App\Models\Keyword;
use App\Models\Tag;
use App\Models\Type;
use App\Repositories\TrafficRepositoryInterface;
use App\Repositories\TypesRepositoryInterface;
use Illuminate\Http\Request;

class TypesController extends Controller
{

    private $trafficRepository;
    /**
     * @var TypesRepositoryInterface
     */
    private $typesRepository;


    public function __construct(TrafficRepositoryInterface $trafficRepository, TypesRepositoryInterface $typesRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->typesRepository = $typesRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $types = Type::all();
        return view('recipes.types.index')->with('types', $types);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'create';
        $this->trafficRepository->getRemoteAddress($action);

        $tags = Tag::all();
        $keywords = Keyword::all();

        return view('recipes.types.create')->with(
            [
                'tags' => $tags,
                'keywords' => $keywords,
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        $this->typesRepository->addNewType($request);

        return redirect()->route('recipes.types.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function show(Type $type)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        $recipes = $type->recipes()->paginate(6);

        return view('recipes.types.show')->with(
            [
                'type' => $type,
                'recipes' => $recipes,
            ]
        );


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function edit(Type $type)
    {
        $action = 'edit';
        $this->trafficRepository->getRemoteAddress($action);


        $tags = Tag::all();
        $keywords = Keyword::all();

        return view('recipes.types.edit')->with(
            [
                'type' => $type,
                'tags' => $tags,
                'keywords' => $keywords,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Type $type)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        $this->typesRepository->update($type, $request);

        return redirect()->route('recipes.types.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function destroy(Type $type)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        $this->typesRepository->destroy($type);

        return redirect()->route('recipes.types.index');
    }
}
