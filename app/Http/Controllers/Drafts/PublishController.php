<?php

namespace App\Http\Controllers\Drafts;

use App\Http\Controllers\Controller;
use App\Models\DraftRecipe;
use App\Repositories\DraftsRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class PublishController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var DraftsRepositoryInterface
     */
    private $draftsRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                DraftsRepositoryInterface $draftsRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->draftsRepository = $draftsRepository;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DraftRecipe  $publish
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DraftRecipe $publish)
    {

        $action = 'update with' . $request->set;
        $this->trafficRepository->getRemoteAddress($action);

        $ready = $this->draftsRepository->publishDraftRecipe($request, $publish);

        if($request->set == 0) {
            if ($ready === 1) {
                $request->session()->flash('danger', 'The recipe must be prepared!');
            } else {
                $request->session()->flash('danger', 'There was an error');
            }
        } else {
            if ($ready === 1) {
                $request->session()->flash('success', 'Now the recipe is ready to be published');
            } else {
                $request->session()->flash('danger', 'There was an error');
            }
        }
        return redirect()->route('drafts.recipes.show', $publish->id);
    }
}
