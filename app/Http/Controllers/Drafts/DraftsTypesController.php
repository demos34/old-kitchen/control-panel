<?php

namespace App\Http\Controllers\Drafts;

use App\Http\Controllers\Controller;
use App\Models\DraftRecipe;
use App\Models\Type;
use App\Repositories\TrafficRepositoryInterface;
use App\Repositories\TypesRepositoryInterface;
use Illuminate\Http\Request;

class DraftsTypesController extends Controller
{

    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var TypesRepositoryInterface
     */
    private $typesRepository;

    public  function __construct(TrafficRepositoryInterface $trafficRepository,
                                 TypesRepositoryInterface $typesRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->typesRepository = $typesRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'create';
        $this->trafficRepository->getRemoteAddress($action);
        return $this->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function show(Type $type)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        $drafts = $type->drafts()->paginate(6);

        return view('drafts.types.show')->with(
            [
                'type' => $type,
                'drafts' => $drafts,
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function edit(Type $type)
    {
        $action = 'edit';
        $this->trafficRepository->getRemoteAddress($action);
        return $this->index();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Type $type)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);
        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function destroy(Type $type)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);
        return $this->index();
    }
}
