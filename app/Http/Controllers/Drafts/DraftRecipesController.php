<?php

namespace App\Http\Controllers\Drafts;

use App\Http\Controllers\Controller;
use App\Models\DraftRecipe;
use App\Models\Keyword;
use App\Models\Tag;
use App\Models\Type;
use App\Repositories\DraftsRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class DraftRecipesController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var DraftsRepositoryInterface
     */
    private $draftsRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                DraftsRepositoryInterface $draftsRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->draftsRepository = $draftsRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $drafts = DraftRecipe::orderBy('updated_at', 'DESC')->paginate(5);
        return view('drafts.recipes.index')->with('drafts', $drafts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'create';
        $this->trafficRepository->getRemoteAddress($action);

        $types = Type::all();
        $tags = Tag::all();
        $keywords = Keyword::all();

        return view('drafts.recipes.create')->with(
            [
                'types' => $types,
                'tags' => $tags,
                'keywords' => $keywords,
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        $draft = $this->draftsRepository->addNewDraft($request);

        return redirect()->route('drafts.recipes.show', $draft);
    }

    public function show(DraftRecipe $recipe)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        return view('drafts.recipes.show')->with('draft', $recipe);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DraftRecipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit(DraftRecipe $recipe)
    {
        $action = 'edit';
        $this->trafficRepository->getRemoteAddress($action);


        $arraySlug = explode('-', $recipe->slug);
        $shifted = array_shift($arraySlug);
        $slug = implode('-', $arraySlug);

        $types = Type::all();
        $tags = Tag::all();
        $keywords = Keyword::all();

        return view('drafts.recipes.edit')->with(
            [
                'types' => $types,
                'tags' => $tags,
                'keywords' => $keywords,
                'draft' => $recipe,
                'slug' => $slug,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DraftRecipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DraftRecipe $recipe)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        $slug = $this->draftsRepository->updateExistingDraft($request, $recipe);
        return redirect()->route('drafts.recipes.show', $slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DraftRecipe  $recipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(DraftRecipe $recipe)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        $recipe->delete();

        return redirect()->route('drafts.recipes.index')->with('success', 'Рецептата е изтрита!');
    }
}
