<?php

namespace App\Http\Controllers\Drafts;

use App\Http\Controllers\Controller;
use App\Models\DraftProduct;
use Illuminate\Http\Request;

class DraftProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DraftProduct  $draftProduct
     * @return \Illuminate\Http\Response
     */
    public function show(DraftProduct $draftProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DraftProduct  $draftProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(DraftProduct $draftProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DraftProduct  $draftProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DraftProduct $draftProduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DraftProduct  $draftProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(DraftProduct $draftProduct)
    {
        //
    }
}
