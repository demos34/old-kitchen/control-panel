<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\ContactUs;
use App\Models\Message;
use App\Models\MessageType;
use App\Repositories\HomeInfoRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var HomeInfoRepositoryInterface
     */
    private $homeInfoRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                HomeInfoRepositoryInterface $homeInfoRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->homeInfoRepository = $homeInfoRepository;
    }

    public function index(MessageType $messageType)
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);


        return view('home.contacts.index')->with(
            [
                'type' => $messageType,
            ]
        );
    }

    public function delete(Message $message)
    {
        $action = 'delete';
        $this->trafficRepository->getRemoteAddress($action);
        $message->delete();
        return redirect()->back()->with('success', 'Message is successfully deleted!');
    }



}
