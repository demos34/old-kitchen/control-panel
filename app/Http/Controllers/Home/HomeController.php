<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\HomeInfo;
use App\Models\IndexTag;
use App\Repositories\HomeInfoRepositoryInterface;
use App\Repositories\RecipesRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var HomeInfoRepositoryInterface
     */
    private $homeInfoRepository;
    /**
     * @var RecipesRepositoryInterface
     */
    private $recipesRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                HomeInfoRepositoryInterface $homeInfoRepository,
                                RecipesRepositoryInterface $recipesRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->homeInfoRepository = $homeInfoRepository;
        $this->recipesRepository = $recipesRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $homeInfo = HomeInfo::find('1');
        $tags = IndexTag::all();
        $lastTag = DB::table('index_tags')->orderBy('id', 'DESC')->first();



        $recipesCount = $homeInfo->recipes_count;
        $lastNRecipes = $this->recipesRepository->getLastNRecipes($recipesCount);

        return view('home.home.index')->with(
            [
                'homeInfo' => $homeInfo,
                'tags' => $tags,
                'lastTag' => $lastTag,
                'lastRecipes' => $lastNRecipes,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'create';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HomeInfo  $homeInfo
     * @return \Illuminate\Http\Response
     */
    public function show(HomeInfo $homeInfo)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HomeInfo  $homeInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(HomeInfo $homeInfo)
    {
        $action = 'edit';
        $this->trafficRepository->getRemoteAddress($action);
        $home = HomeInfo::find(1);

        return view('home.home.edit')->with('homeInfo', $home);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HomeInfo  $homeInfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HomeInfo $homeInfo)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        $update = $this->homeInfoRepository->update($request);

        if ($update === TRUE) {
            $request->session()->flash('success', 'The home information is successfully updated!');
        } else {
            $request->session()->flash('danger', 'There was an error updating the home information');
        }
        return redirect()->route('home.home.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HomeInfo  $homeInfo
     * @return \Illuminate\Http\Response
     */
    public function destroy(HomeInfo $homeInfo)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }
}
