<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Home\Tube;
use App\Models\Post;
use App\Repositories\PostsRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var PostsRepositoryInterface
     */
    private $postsRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                PostsRepositoryInterface $postsRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->postsRepository = $postsRepository;
    }

    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $posts = $this->postsRepository->getAllPosts();
        $tags = $this->postsRepository->getAllTags();
        $keywords = $this->postsRepository->getAllKeywords();

        return view('home.posts.index')->with(
            [
                'posts' => $posts,
                'tags' => $tags,
                'keywords' => $keywords,
            ]
        );
    }

    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        $post = $this->postsRepository->storePost($request);

        return redirect()->back()->with('success', 'The new post is added successfully');
    }

    public function show(Post $post)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        $tags = $this->postsRepository->getAllTags();
        $keywords = $this->postsRepository->getAllKeywords();

        return view('home.posts.show')->with(
            [
                'post' => $post,
                'tags' => $tags,
                'keywords' => $keywords,
            ]
        );
    }

    public function update(Request $request, Post $post)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        $post = $this->postsRepository->updatePost($request, $post);

        return redirect()->back()->with('success', 'Post is successfully updated!');
    }

    public function destroy(Post $post)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        $this->postsRepository->deletePost($post);

        return redirect()->route('blog')->with('success', 'Post is successfully deleted!');
    }

    public function tube()
    {
        $action = 'tube';
        $this->trafficRepository->getRemoteAddress($action);

        $posts = $this->postsRepository->getAllYouTubePosts();
        $tags = $this->postsRepository->getAllTags();
        $keywords = $this->postsRepository->getAllKeywords();

        return view('home.youtube.index')->with(
            [
                'posts' => $posts,
                'tags' => $tags,
                'keywords' => $keywords,
            ]
        );
    }

    public function tubeStore(Request $request)
    {
        $action = 'tube-store';
        $this->trafficRepository->getRemoteAddress($action);

        $tube = $this->postsRepository->storeTube($request);

        return redirect()->route('tube-show', $tube->slug)->with('success', 'The new post is added successfully');
    }

    public function tubeShow(Tube $tube)
    {
        $action = 'tube-store';
        $this->trafficRepository->getRemoteAddress($action);

        $tags = $this->postsRepository->getAllTags();
        $keywords = $this->postsRepository->getAllKeywords();

        return view('home.youtube.show')->with(
            [
                'post' => $tube,
                'tags' => $tags,
                'keywords' => $keywords,
            ]
        );
    }

    public function tubeUpdate(Request $request, Tube $tube)
    {
        $action = 'tube-update';
        $this->trafficRepository->getRemoteAddress($action);
        $this->postsRepository->updateTubePost($request, $tube);

        return redirect()->back()->with('success', 'Post is successfully deleted!');
    }

    public function tubeDestroy(Tube $tube)
    {
        $action = 'tube-destroy';
        $this->trafficRepository->getRemoteAddress($action);

        $this->postsRepository->deleteTubePost($tube);

        return redirect()->route('youtube')->with('success', 'Post is successfully deleted!');
    }
}
