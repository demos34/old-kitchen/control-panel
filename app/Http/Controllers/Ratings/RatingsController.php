<?php

namespace App\Http\Controllers\Ratings;

use App\Http\Controllers\Controller;
use App\Models\Rating;
use App\Repositories\RatingsRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class RatingsController extends Controller
{

    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var RatingsRepositoryInterface
     */
    private $ratingsRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                RatingsRepositoryInterface $ratingsRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->ratingsRepository = $ratingsRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $ratings = $this->ratingsRepository->getAllRatings();

        return view('ratings.ratings.index')->with('ratings', $ratings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'create';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        $this->ratingsRepository->addNewRating($request);

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function show(Rating $rating)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function edit(Rating $rating)
    {
        $action = 'edit';
        $this->trafficRepository->getRemoteAddress($action);

        return view('ratings.ratings.edit')->with('rating', $rating);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rating $rating)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        $this->ratingsRepository->updateRating($request, $rating);

        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rating $rating)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        $this->ratingsRepository->deleteRating($rating);

        return $this->index();
    }
}
