<?php

namespace App\Http\Controllers\Ratings;

use App\Http\Controllers\Controller;
use App\Models\RatingValue;
use App\Models\Recipe;
use App\Repositories\RecipesRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class RatingValuesController extends Controller
{

    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var RecipesRepositoryInterface
     */
    private $recipesRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                RecipesRepositoryInterface $recipesRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->recipesRepository = $recipesRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = 'edit';
        $this->trafficRepository->getRemoteAddress($action);

        return redirect('recipes.recipes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'create';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RatingValue  $ratingValue
     * @return \Illuminate\Http\Response
     */
    public function show(RatingValue $ratingValue)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RatingValue  $ratingValue
     * @return \Illuminate\Http\Response
     */
    public function edit(RatingValue $ratingValue)
    {
        $action = 'edit';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Recipe  $value
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recipe $value)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);


        $cookies = Cookie::get();
        if(empty($cookies['laravel_cookie_consent'])){
            return redirect()->route('recipes.recipes.show', $value->slug);
        } else {
            Cookie::queue('2', '2', '2');
        }

        $attach = $this->recipesRepository->setRating($request, $value);

        return redirect()->route('recipes.recipes.show', $value->slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RatingValue  $ratingValue
     * @return \Illuminate\Http\Response
     */
    public function destroy(RatingValue $ratingValue)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }
}
