<?php

namespace App\Http\Controllers\Tags;

use App\Http\Controllers\Controller;
use App\Models\IndexTag;
use App\Models\Tag;
use App\Repositories\TagsRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class IndexTagsController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var TagsRepositoryInterface
     */
    private $tagsRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository, TagsRepositoryInterface $tagsRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->tagsRepository = $tagsRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        return redirect()->route('tags.tags.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'create';
        $this->trafficRepository->getRemoteAddress($action);

        return redirect()->route('tags.tags.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        $this->tagsRepository->addIndexTag($request);

        return redirect()->route('tags.tags.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\IndexTag  $index
     */
    public function show(IndexTag $index)
    {

        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        return view('tags.tags.index-tag-show')->with('tag', $index);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\IndexTag  $indexTag
     * @return \Illuminate\Http\Response
     */
    public function edit(IndexTag $indexTag)
    {
        $action = 'edit';
        $this->trafficRepository->getRemoteAddress($action);

        return redirect()->route('tags.tags.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\IndexTag  $indexTag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IndexTag $index)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        if($request->name == $index->name){
            $validated = $request->validate(
                [
                    'name' => 'required',
                ]
            );
        } else {
            $validated = $request->validate(
                [
                    'name' => 'required|unique:tags|min:3|max:100',
                ]
            );
        }
        $index->update(
            [
                'name' => $validated['name'],
            ]
        );

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\IndexTag  $indexTag
     * @return \Illuminate\Http\Response
     */
    public function destroy(IndexTag $index)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        $index->delete();

        return redirect()->route('tags.tags.index');
    }
}
