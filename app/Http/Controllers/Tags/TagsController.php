<?php

namespace App\Http\Controllers\Tags;

use App\Http\Controllers\Controller;
use App\Models\HomeInfo;
use App\Models\IndexTag;
use App\Models\Keyword;
use App\Models\Tag;
use App\Repositories\TagsRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var TagsRepositoryInterface
     */
    private $tagsRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository, TagsRepositoryInterface $tagsRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->tagsRepository = $tagsRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $tags = Tag::all();
        $keywords = Keyword::all();
        $indexTags = IndexTag::all();
        $homeInfo = HomeInfo::find(1);
        return view('tags.tags.index')->with(
            [
                'tags' => $tags,
                'indexTags' => $indexTags,
                'homeInfo' => $homeInfo,
                'keywords' => $keywords,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'create';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        $this->tagsRepository->addTag($request);
        $tags = Tag::all();

        return redirect()->route('tags.tags.index')->with('tags', $tags);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        return view('tags.tags.show')->with('tag', $tag);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        $action = 'edit';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);
        if($request->name == $tag->name){
            $validated = $request->validate(
                [
                    'name' => 'required',
                ]
            );
        } else {
            $validated = $request->validate(
                [
                    'name' => 'required|unique:tags|min:3|max:100',
                ]
            );
        }
        $tag->update(
            [
                'name' => $validated['name'],
            ]
        );

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        $tag->delete();
        return $this->index();
    }
}
