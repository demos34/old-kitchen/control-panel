<?php

namespace App\Http\Controllers\Tags;

use App\Http\Controllers\Controller;
use App\Models\Keyword;
use App\Repositories\KeywordRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class KeywordsController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var KeywordRepositoryInterface
     */
    private $keywordRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository, KeywordRepositoryInterface $keywordRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->keywordRepository = $keywordRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $keywords = $this->keywordRepository->getAllKeywords();
        return view('tags.keywords.index')->with('keywords', $keywords);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'create';
        $this->trafficRepository->getRemoteAddress($action);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        $keywords = $this->keywordRepository->addNewKeyword($request);

        if (!empty($keywords)) {
            $request->session()->flash('success', 'The keyword is successfully added!');
        } else {
            $request->session()->flash('danger', 'There was an error adding the keyword');
        }

        return redirect()->route('tags.keywords.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function show(Keyword $keyword)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function edit(Keyword $keyword)
    {
        $action = 'edit';
        $this->trafficRepository->getRemoteAddress($action);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Keyword $keyword)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function destroy(Keyword $keyword, Request $request)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        $delete = $this->keywordRepository->deleteExistingKeyword($keyword);

        if ($delete === TRUE) {
            $request->session()->flash('success', 'The keyword is successfully deleted!');
        } else {
            $request->session()->flash('danger', 'There was an error deleting the keyword');
        }

        return redirect()->route('tags.keywords.index');
    }
}
