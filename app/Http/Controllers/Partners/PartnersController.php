<?php

namespace App\Http\Controllers\Partners;

use App\Http\Controllers\Controller;
use App\Models\Partner;
use App\Repositories\HomeInfoRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class PartnersController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var HomeInfoRepositoryInterface
     */
    private $homeInfoRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository, HomeInfoRepositoryInterface  $homeInfoRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->homeInfoRepository = $homeInfoRepository;
    }

    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $partners = $this->homeInfoRepository->getAllPartners();

        return view('home.partners.index')->with(
            [
                'partners' => $partners,
            ]
        );
    }

    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->homeInfoRepository->storePartner($request);

        return redirect()->back()->with($msg['session'], $msg['message']);
//        return $this->index()->with($msg['session'], $msg['message']);
    }

    public function show(Partner $partner)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        return view('home.partners.show')->with(
            [
                'partner' => $partner,
            ]
        );
    }

    public function update(Request $request, Partner $partner)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->homeInfoRepository->updatePartner($request, $partner);

        return redirect()->back()->with($msg['session'], $msg['message']);


    }

    public function destroy(Partner  $partner)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->homeInfoRepository->delete($partner);

        return redirect()->back()->with($msg['session'], $msg['message']);
    }
}
