<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Profile;
use Gate;
use App\Models\Role;
use App\Repositories\TrafficRepositoryInterface;
use App\Repositories\UsersRepositoryInterface;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    private $trafficRepository;
    /**
     * @var UsersRepositoryInterface
     */
    private $usersRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository, UsersRepositoryInterface $usersRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->usersRepository = $usersRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = 'index';

        $this->trafficRepository->getRemoteAddress($action);
        $users = User::all();
        return view('users.users.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $action = 'create';
        $this->trafficRepository->getRemoteAddress($action);

        $roles = Role::all();

        return view('users.users.create')->with('roles', $roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        $this->usersRepository->registerUser($request);
        return redirect()->route('users.users.index');
//        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $action = 'show';

        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if(Gate::denies('edit-information')){
            return redirect(route('users.users.index'));
        }
        $action = 'edit';

        $this->trafficRepository->getRemoteAddress($action);
        $roles = Role::all();
        return view('users.users.edit')->with(
            [
                'user' => $user,
                'roles' => $roles
            ]
        );
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $action = 'update';

        $this->trafficRepository->getRemoteAddress($action);
        $this->usersRepository->updateUser($user, $request);
        return redirect()->route('users.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $action = 'destroy';

        $this->trafficRepository->getRemoteAddress($action);
        $user->roles()->detach();
        $user->delete();

        return redirect()->route('users.users.index');
    }
}
