<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Models\Profile;
use App\Repositories\ProfilesRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfilesController extends Controller
{
    private $trafficRepository;
    /**
     * @var ProfilesRepositoryInterface
     */
    private $profilesRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                ProfilesRepositoryInterface $profilesRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->profilesRepository = $profilesRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $user = $this->profilesRepository->getAuthUser();
        $profile = $this->profilesRepository->getMyProfile($user->id);

        return view('users.profile.index')->with(
            [
                'user' => $user,
                'profile' => $profile,
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        $action = 'show';
        $traffic = $this->trafficRepository->getRemoteAddress($action);
        dd($traffic);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $user = $this->profilesRepository->getAuthUser();
        return view('users.profile.edit')->with(
            [
                'user' => $user,
                'profile' => $profile,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $this->profilesRepository->updateMyProfile($profile, $request);

        return redirect()->route('users.profile.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
