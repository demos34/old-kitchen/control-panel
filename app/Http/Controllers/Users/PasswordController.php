<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Repositories\TrafficRepositoryInterface;
use App\Repositories\UsersRepositoryInterface;
use App\User;
use Illuminate\Http\Request;

class PasswordController extends Controller
{

    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var UsersRepositoryInterface
     */
    private $usersRepository;

    public function  __construct(TrafficRepositoryInterface $trafficRepository,
                                 UsersRepositoryInterface $usersRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->usersRepository = $usersRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        return redirect()->route('users.profile.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'create';
        $this->trafficRepository->getRemoteAddress($action);

        return redirect()->route('users.profile.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        return redirect()->route('users.profile.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        return redirect()->route('users.profile.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $action = 'edit';
        $this->trafficRepository->getRemoteAddress($action);

        $profile = $this->usersRepository->returnProfile();
        $user = $this->usersRepository->returnUser();

        return view('users.password.edit')->with(
            [
                'user' => $user,
                'profile' => $profile,

            ]
        );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        $update = $this->usersRepository->changePassword($request);

        if ($update === TRUE) {
            $request->session()->flash('success', 'The password is changed successfully!');
        } else {
            $request->session()->flash('danger', 'There was an error updating the password');
        }
        return redirect()->route('users.profile.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        return redirect()->route('users.profile.index');
    }
}
