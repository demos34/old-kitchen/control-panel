<?php

namespace App\Http\Controllers\Books;

use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Repositories\BooksRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class BooksController extends Controller
{

    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var BooksRepositoryInterface
     */
    private $booksRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                BooksRepositoryInterface $booksRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->booksRepository = $booksRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        $books = $this->booksRepository->getAllBooks();

        return view('books.books.index')->with('books', $books);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'create';
        $this->trafficRepository->getRemoteAddress($action);

        $books = $this->booksRepository->getAllBooks();

        return view('books.books.create')->with('books', $books);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);

        $store = $this->booksRepository->addNewBook($request);
        if (!empty($store)) {
            $request->session()->flash('success', 'The the book is stored successfully!');
        } else {
            $request->session()->flash('danger', 'There was an error storing the book');
        }
        return redirect()->route('books.books.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Book  $books
     * @return \Illuminate\Http\Response
     */
    public function show(Book $books)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        $publishers = $this->booksRepository->getAllPublishers();

        return view('books.books.show')->with(
            [
                'book' => $books,
                'publishers' => $publishers,
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        $action = 'edit';
        $this->trafficRepository->getRemoteAddress($action);

        $publishers = $this->booksRepository->getAllPublishers();

        return view('books.books.edit')->with(
            [
                'book' => $book,
                'publishers' => $publishers,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        $this->booksRepository->editExistingBook($request, $book);

        return $this->show($book);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        $this->booksRepository->deleteBook($book);

        return $this->create();
    }
}
