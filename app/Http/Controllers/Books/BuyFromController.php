<?php

namespace App\Http\Controllers\Books;

use App\Http\Controllers\Controller;
use App\Models\BuyFrom;
use App\Repositories\BooksRepositoryInterface;
use App\Repositories\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class BuyFromController extends Controller
{


    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var BooksRepositoryInterface
     */
    private $booksRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                BooksRepositoryInterface $booksRepository)
    {
        $this->trafficRepository = $trafficRepository;
        $this->booksRepository = $booksRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $action = 'index';
        $this->trafficRepository->getRemoteAddress($action);

        dd(1);

        return $this->index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = 'create';
        $this->trafficRepository->getRemoteAddress($action);

        $publishers = $this->booksRepository->getAllPublishers();

        return view('books.buy.create')->with('publishers', $publishers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = 'store';
        $this->trafficRepository->getRemoteAddress($action);
        $store = $this->booksRepository->addPublisher($request);
        if (!empty($store)) {
            $request->session()->flash('success', 'The the publisher is stored successfully!');
        } else {
            $request->session()->flash('danger', 'There was an error storing the publisher');
        }
        return redirect()->route('books.buy.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BuyFrom  $buy
     * @return \Illuminate\Http\Response
     */
    public function show(BuyFrom $buy)
    {
        $action = 'show';
        $this->trafficRepository->getRemoteAddress($action);

        return view('books.buy.show')->with('publisher', $buy);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BuyFrom  $buyFrom
     * @return \Illuminate\Http\Response
     */
    public function edit(BuyFrom $buyFrom)
    {
        $action = 'edit';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BuyFrom  $buyFrom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BuyFrom $buyFrom)
    {
        $action = 'update';
        $this->trafficRepository->getRemoteAddress($action);

        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BuyFrom  $buy
     * @return \Illuminate\Http\Response
     */
    public function destroy(BuyFrom $buy)
    {
        $action = 'destroy';
        $this->trafficRepository->getRemoteAddress($action);

        $this->booksRepository->deletePublisher($buy);

        return $this->create();
    }
}
