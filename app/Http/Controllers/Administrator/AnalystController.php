<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\Administrator\Analyst;
use App\Models\Administrator\AnalystArchive;
use App\Models\Administrator\AnalystCustom;
use App\Models\Administrator\AnalyzeCustomRecord;
use App\Models\Administrator\Site;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class AnalystController extends Controller
{
    public function index()
    {
        $analyzes = Analyst::orderBy('created_at', 'DESC')->paginate(10);
        return view('administrator.analyst.index', compact('analyzes'));
    }

    public function sort($value, $where)
    {
        if($this->validateValue($value) === false || $this->validateWhere($where) === false)
        {
            return redirect()->back()->with('danger', 'Грешна стойност!');
        }
        $analyzes = Analyst::orderBy($value, $where)->paginate(10);
        return view('administrator.analyst.index', compact('analyzes'));
    }

    public function look($value, $look)
    {
        if($this->validateValue($value) === false)
        {
            return redirect()->back()->with('message', 'Грешна стойност!');
        }

        $analyzes = Analyst::where($value, $look)->paginate(10);
        $count = Analyst::where($value, $look)->get()->count();

        if($value === 'created_at')
        {
            $analyzes = Analyst::where('created_at', 'LIKE',  "%{$look}%")->paginate(10);
            $count = Analyst::where('created_at', 'LIKE',  "%{$look}%")->get()->count();
        }

        if($value === 'referer')
        {
            $site = Site::where('string_id', $look)->first();
            $analyzes = Analyst::where('site_id', $site->id)->paginate(10);
            $count = Analyst::where('site_id', $site->id)->get()->count();
            $look = $site->url;
        }

        return view('administrator.analyst.look', compact('analyzes', 'value' , 'look', 'count'));
    }

    public function byDate($key)
    {
        if($this->validateByDateKey($key) === false)
        {
            return redirect()->back()->with('message', 'Грешна стойност!');
        }

        $today = Carbon::now();
        $analyzes = Analyst::where('created_at', '>', $today->subDays($key))->orderBy('created_at', 'desc')->paginate(15);
        $count = Analyst::where('created_at', '>', $today->subDays($key))->get()->count();
        if($key === '30'){
            $analyzes = Analyst::where('created_at', '>', $today->subMonth())->orderBy('created_at', 'desc')->paginate(15);
            $count = Analyst::where('created_at', '>', $today->subMonth())->get()->count();
        }
        return view('administrator.analyst.by-date', compact('analyzes', 'count' , 'today', 'key'));

    }

    public function custom()
    {
        $myAnalysts = AnalystCustom::where('user_id', auth()->id())->get();
        $myAnalysts->map(function ($item) {
            return date('Y-m-d', strtotime($item->start_date));
        });
        return view('administrator.analyst.custom', compact('myAnalysts'));
    }

    public function customStore(Request $request): RedirectResponse
    {
        $data = $request->validate(
            [
                'name' => 'string|min:1|max:50|required',
                'start_date' => 'date|required',
                'end_date' => 'date|required',
            ]
        );

        $data = array_merge($data, ['user_id' => auth()->id()]);

        $custom = AnalystCustom::create($data);
        $analyzes = Analyst::whereBetween('created_at', [$custom->start_date, $custom->end_date])
            ->orderBy('created_at', 'desc')->get();
        $analyzes->map(function ($analyze) use ($custom) {
            $analyze['user_id'] = auth()->id();
            $analyze['analyst_custom_id'] = $custom->id;
            return $analyze;
        });
        foreach ($analyzes as $analyze){
            AnalyzeCustomRecord::create($analyze->toArray());
        }

        return redirect()->back()->with('success', "Успешно създаден запис!");
    }

    public function customShow(AnalystCustom $analystCustom)
    {
        if($analystCustom->user_id !== auth()->id())
        {
            return redirect()->route('analyst.custom-analyst')->with('alert', "Този анализатор не е Ваш! Нямате право да го виждате!");
        }

        return view('administrator.analyst.custom-show', compact('analystCustom'));
    }

    public function customSort(AnalystCustom $analystCustom, $value, $where)
    {
        if($this->validateValue($value) === false || $this->validateWhere($where) === false)
        {
            return redirect()->back()->with('danger', 'Грешна стойност!');
        }

        $analyzes = $analystCustom->analyzeCustomRecords()->orderBy($value, $where)->paginate(15);

        return view('administrator.analyst.custom-show-sort', compact('analyzes', 'analystCustom'));
    }

    public function customDelete(AnalystCustom $analystCustom): RedirectResponse
    {
        $analystCustom->delete();
        return redirect()->back()->with('success', "Успешно изтрит запис!");
    }

    public function archive()
    {
        $analyzes = AnalystArchive::orderBy('created_at', 'DESC')->paginate(10);
        return view('administrator.analyst.archive', compact('analyzes'));
    }


    public function archiveSort($value, $where)
    {
        if($this->validateValue($value) === false || $this->validateWhere($where) === false)
        {
            return redirect()->back()->with('danger', 'Грешна стойност!');
        }
        $analyzes = AnalystArchive::orderBy($value, $where)->paginate(10);
        return view('administrator.analyst.archive', compact('analyzes'));
    }

    public function archiveDelete(): RedirectResponse
    {
        AnalystArchive::truncate();
        return redirect()->back()->with('success', "Архивът е успешно изпразнен!");

    }

    public function archiveChosen(Request $request): RedirectResponse
    {
        if(!$request->has('archives'))
        {
            return redirect()->back()->with('warning', "Трябва да бъде избран поне един запис!");
        }
        AnalystArchive::whereIn('id', $request->archives)->delete();
        return redirect()->back()->with('success', "Успешно изтрит/и запис/и!");

    }

    public function inProgress()
    {
        dd('Не е готово');
    }

    protected function validateValue($value): bool
    {
        if($value === 'country' || $value === 'ip_address' || $value === 'session_id' || $value === 'referer' || $value === 'created_at')
        {
            return true;
        }

        return false;
    }

    protected function validateWhere($where): bool
    {
        if($where === 'asc' || $where === 'desc')
        {
            return true;
        }

        return false;
    }

    protected function validateByDateKey($key): bool
    {
        if($key === '1' || $key === '7' || $key === '30')
        {
            return true;
        }

        return false;
    }

    protected function moveToArchive()
    {
        $today = Carbon::today();
        $archives = Analyst::where('created_at', '<', $today->subMonths(3))->get();
        foreach ($archives as $archive){
            AnalystArchive::create($archive->toArray());
            $archive->delete();
        }
    }
}
