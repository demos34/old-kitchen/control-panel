<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\Count\Count;
use App\Models\Count\Countable;
use App\Models\Count\CountableCustom;
use App\Models\Count\CountableCustomRecord;
use App\Models\Count\CountType;
use App\Models\Home\Tube;
use App\Models\Post;
use App\Models\Product;
use App\Models\Recipe;
use App\Models\Type;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CountsController extends Controller
{
    public function index($key)
    {
        if ($this->checkKey($key) === false) {
            return redirect()->back()->with('danger', 'Подадени са грешно стойности, моля опитайте пак!');
        }
        if ($key === 'sum') {
            $counts = Count::all();
        } else {
            $counts = Count::where('created_at', '>', $this->todaySubKey($key))->get();
        }
        return view('administrator.counts.index', compact('counts', 'key'));
    }

    public function by(CountType $countType, $key)
    {

        if ($this->checkKey($key) === false) {
            return redirect()->back()->with('danger', 'Подадени са грешно стойности, моля опитайте пак!');
        }
        if ($key === 'sum') {
            $counts = Count::where('count_type_id', $countType->id)
                ->get();
        } else {
            $counts = Count::where('count_type_id', $countType->id)
                ->where('created_at', '>', $this->todaySubKey($key))
                ->get();
        }

        $mostViewed = $this->mostViewed($countType);

        $type = $this->getTypeInCyrillic($countType);
        return view
        ('administrator.counts.by',
            compact
            (
                'counts', 'type', 'key', 'countType', 'mostViewed'
            )
        );
    }

    public function custom()
    {
        $custom = CountableCustom::all();
        $types = CountType::all();
        $cyrType = [
            'Общо',
            'По категории',
            'По рецепти',
            'Блог',
            'Ютюб',
            '',
        ];
        $types->map(function ($type) use ($cyrType) {
            $type['cyr'] = $cyrType[$type->id - 1];
            return $type;
        });
        return view('administrator.counts.custom', compact('custom', 'types'));
    }

    public function customStore(Request $request)
    {
        $data = $request->validate(
            [
                'name' => 'string|min:1|max:50|required',
                'start_date' => 'date|required',
                'end_date' => 'date|required',
                'count_type_id' => 'required|exists:count_types,id'
            ]
        );

        $data = array_merge($data, ['user_id' => auth()->id()]);
        $custom = CountableCustom::create($data);
        $countTypeId = $custom->count_type_id;



        if($countTypeId == 1)
        {
            $counts = Count::whereBetween('created_at', [$custom->start_date, $custom->end_date])->get();

            CountableCustomRecord::create(
                [
                    'countable_custom_id' => $custom->id,
                    'count_type_id' => $countTypeId,
                    'countable' => 0,
                    'counts' => $counts->count(),
                ]
            );
        } else {
            $counts = Count::where('count_type_id', $countTypeId)
                ->whereBetween('created_at', [$custom->start_date, $custom->end_date])
                ->orderBy('created_at', 'desc')->get();
            foreach ($counts as $count) {
                if ($count->countable !== 'first'
                    && $count->countable !== ''
                    && $count->countable !== 'first'
                    && $count->countable !== 'NONE')
                {
                    if (CountableCustomRecord::where('count_type_id', $countTypeId)->where('countable', $count->countable)->first())
                    {
                        $countable = CountableCustomRecord::where('count_type_id', $countTypeId)->where('countable', $count->countable)->first();
                        $countable->update(
                            [
                                'counts' => $countable->counts + 1,
                            ]
                        );
                    } else {
                        CountableCustomRecord::create(
                            [
                                'countable_custom_id' => $custom->id,
                                'count_type_id' => $countTypeId,
                                'countable' => $count->countable,
                                'counts' => 1,
                            ]
                        );
                    }
                };
            }
        }
        return redirect()->route('counter.custom.show', $custom->id)->with('success', "Успешно създаден запис!");
    }

    public function customShow(CountableCustom $countableCustom)
    {
        if($countableCustom->user_id !== auth()->id())
        {
            return redirect()->route('counter.custom')->with('alert', "Този анализатор не е Ваш! Нямате право да го виждате!");
        }

        $countType = CountType::find($countableCustom->count_type_id);
        $type = $this->getTypeInCyrillic($countType);
        $most = Countable::where('count_type_id', $countType->id)->orderBy('counts', 'DESC')->first();
        if ($countType->id === 2) {
            $mostViewed = Type::find($most->countable);
        } elseif ($countType->id === 3) {
            $mostViewed = Recipe::find($most->countable);
        } elseif ($countType->id === 4) {
            $mostViewed = Post::find($most->countable);
        } elseif ($countType->id === 5) {
            $mostViewed = Tube::find($most->countable);
        } elseif ($countType->id === 1) {
            $mostViewed = null;
        } else {
            return redirect()->back()->with('danger', 'Грешен тип!');
        }

        return view('administrator.counts.show', compact('countableCustom', 'type', 'mostViewed'));
    }

    public function customDelete(CountableCustom $countableCustom): RedirectResponse
    {
        $countableCustom->delete();
        return redirect()->route('counter.custom')->with('success', 'Записът е изтрит успешно!');
    }

    public function admin($pass): RedirectResponse
    {
        if ($pass !== 'blah') {
            abort_if($pass !== 'blah', 403);
        }
        Countable::truncate();

        self::fillCountableTable();

        return redirect()->route('counter.index', 'sum');
    }

    public function adminChangeDraftToRecipes($pass)
    {

        if ($pass !== 'blah') {
            abort_if($pass !== 'blah', 403);
        }

        $drafts = Countable::where('count_type_id', 3)->get();
        foreach ($drafts as $draft)
        {
            $recipe = Recipe::where('draft_id', $draft->countable)->first();
            $draft->update(
                [
                    'countable' => $recipe->id,
                ]
            );
        }
    }

    protected static function fillCountableTable()
    {
        $countTypes = CountType::where('id', '>', 1)
            ->where('id', '<', 6)
            ->get();
        foreach ($countTypes as $countType) {
            foreach ($countType->counts as $ads) {
                if ($ads->countable !== 'first' && $ads->countable !== '' && $ads->countable !== 'first' && $ads->countable !== 'NONE') {
                    if ($countType->countables()->where('countable', $ads->countable)->first()) {
                        $countable = $countType->countables()->where('countable', $ads->countable)->first();
                        $countable->update(
                            [
                                'counts' => $countable->counts + 1,
                            ]
                        );
                    } else {
                        $countType->countables()->create(
                            [
                                'countable' => $ads->countable,
                            ]
                        );
                    }
                }
            }
        }
    }

    protected function checkKey($key): bool
    {
        if ($key === 'sum' || $key === '1' || $key === '7' || $key === '30') {
            return true;
        } else {
            return false;
        }
    }

    protected function getTypeInCyrillic(CountType $countType): string
    {
        $type = 'Общо';
        if ($countType->id === 2) {
            $type = 'По категории';
        }
        if ($countType->id === 3) {
            $type = 'По рецепти';
        }
        if ($countType->id === 4) {
            $type = 'Блог';
        }
        if ($countType->id === 5) {
            $type = 'Ютюб';
        }

        return $type;
    }

    protected function todaySubKey($key): Carbon
    {
        return Carbon::now()->subDays($key);
    }

    protected function mostViewed(CountType $countType)
    {
        $most = Countable::where('count_type_id', $countType->id)->orderBy('counts', 'DESC')->first();
        if ($countType->id === 2) {
            return Type::find($most->countable);
        } elseif ($countType->id === 3) {
            return Recipe::find($most->countable);
        } elseif ($countType->id === 4) {
            return Post::find($most->countable);
        } elseif ($countType->id === 5) {
            return Tube::find($most->countable);
        }
        return redirect()->back()->with('danger', 'Грешен тип!');
    }

}
