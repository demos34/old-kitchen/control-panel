<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class ArtisanActionsController extends Controller
{
    public function migrate(): \Illuminate\Http\RedirectResponse
    {
        Artisan::call('migrate');
        return back()->with('success', 'DB is migrated!');
    }

    public function clear(): \Illuminate\Http\RedirectResponse
    {
        Artisan::call('cache:clear');
        Artisan::call('route:clear');
        Artisan::call('config:clear');
        Artisan::call('view:clear');

        return back()->with('success', 'Clear!');
    }
}
