<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\Consult\Client;
use App\Models\Consult\ConsultActivity;
use App\Models\Consult\ConsultCategory;
use App\Models\Consult\ConsultComment;
use App\Models\Consult\ConsultDescription;
use App\Models\Consult\ConsultMessage;
use App\Models\Consult\ConsultService;
use App\Models\Consult\ConsultStatus;
use App\Models\Consult\ConsultSubcategory;
use App\Models\Consult\ConsultType;
use App\Models\Consult\ConsultTypesStatus;
use App\Repositories\ConsultRepositoryInterface;
use Illuminate\Http\Request;

class ConsultController extends Controller
{
    /**
     * @var ConsultRepositoryInterface
     */
    private $consultRepository;

    public function __construct(ConsultRepositoryInterface $consultRepository)
    {
        $this->consultRepository = $consultRepository;
    }

    public function index()
    {
        $messages = $this->consultRepository->getMessages();
        return view('administrator.consult.index')->with(
            [
                'messages' => $messages
            ]
        );
    }

    public function form()
    {
        $categories = $this->consultRepository->getAllCategories();
        return view('administrator.consult.form')->with(
            [
                'categories' => $categories,
            ]
        );
    }

    public function categoryStore(Request $request): \Illuminate\Http\RedirectResponse
    {
        $msg = $this->consultRepository->storeCategory($request, 'store');
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function categoryUpdate(Request $request, ConsultCategory $consultCategory): \Illuminate\Http\RedirectResponse
    {
        $msg = $this->consultRepository->storeCategory($request, 'update', $consultCategory->id);
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function subcategoryStore(Request $request): \Illuminate\Http\RedirectResponse
    {
        $msg = $this->consultRepository->storeCategory($request, 'subStore');
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function subcategoryUpdate(Request $request, ConsultSubcategory $consultSubcategory): \Illuminate\Http\RedirectResponse
    {
        $msg = $this->consultRepository->storeCategory($request, 'subUpdate', $consultSubcategory->id);
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function categoryShow(ConsultCategory $consultCategory)
    {
        return view('administrator.consult.category-show')->with(
            [
                'category' => $consultCategory,
            ]
        );
    }

    public function subcategoryShow(ConsultSubcategory $consultSubcategory)
    {
        return view('administrator.consult.subcategory-show')->with(
            [
                'subcategory' => $consultSubcategory,
            ]
        );
    }

    public function categoryDelete(ConsultCategory $consultCategory): \Illuminate\Http\RedirectResponse
    {
        $consultCategory->delete();
        return redirect()->route('consult-form')->with('success', 'Записът е успешно изтрит!');
    }

    public function subcategoryDelete(ConsultSubcategory $consultSubcategory): \Illuminate\Http\RedirectResponse
    {
        $categoryId = $consultSubcategory->consult_category_id;
        $consultSubcategory->delete();
        return redirect()->route('consult-form-category-show', $categoryId)->with('success', 'Записът е успешно изтрит!');
    }

    public function formShow()
    {
        $categories = $this->consultRepository->getAllCategories();
        $spices = $this->consultRepository->getAllSpices();
        $type = ConsultType::findOrFail(1);
        return view('administrator.consult.form-show')->with(
            [
                'categories' => $categories,
                'spices' => $spices,
                'type' => $type,
            ]
        );
    }

    public function formShowPost(Request $request, ConsultType $consultType): \Illuminate\Http\RedirectResponse
    {
        if(!$this->consultRepository->storeNewClientForm($request, $consultType)){
            return redirect()->back()->with('warning', 'Трябва да изчакате 30 минути преди да въведете отново!');
        } else {
            return redirect()->back()->with('success', 'Формулярът е изпратен успешно!');
        }
    }

    public function inbox(ConsultMessage $consultMessage)
    {
        $this->consultRepository->readConsultMessage($consultMessage);
        $spices = $this->consultRepository->getAllSpices();
        //to save $message->client in all of lines in front-end
        $client = $consultMessage->client;
        $statuses = $this->consultRepository->getAllStatuses();
        return view('administrator.consult.message-show')->with(
            [
                'message' => $consultMessage,
                'client' => $client,
                'spices' => $spices,
                'statuses' => $statuses,
            ]
        );
    }

    public function updateSpice(Request $request, Client $client): \Illuminate\Http\RedirectResponse
    {
        $msg = $this->consultRepository->updateSpice($client, $request);
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function status(ConsultStatus $consultStatus, Client $client)
    {
        if(!empty($client->consultStatuses)) {
            $client->consultStatuses()->sync($consultStatus->id);
        } else {
            $client->consultStatuses()->attach($consultStatus->id);
        }
        return back()->with('success', 'Успешно зададено състояние!');
    }

    public function viewAllByStatutes(ConsultStatus $consultStatus, ConsultType $consultType)
    {
        $statuses = $this->consultRepository->getAllStatuses();
        $clients = $this->consultRepository->getAllMessagesByStatus($consultStatus, $consultType);
        return view('administrator.consult.status-index')->with(
            [
                'statuses' => $statuses,
                'default' => $consultStatus,
                'clients' => $clients,
                'type' => $consultType,
            ]
        );
    }

    public function turnOnOffConsult()
    {
        $activity = ConsultActivity::findOrFail(1);

        $activity->update(
            [
                'is_on' => !$activity->is_on,
            ]
        );

        $newActivity = ConsultActivity::findOrFail(1);
        if($newActivity->is_on == false) return back()->with('danger', 'Меню "Консултации" е изключено!');
        return back()->with('success', 'Меню "Консултации" е включено!');
    }

    public function services()
    {
        $services = $this->consultRepository->getAllServices();
        $types = $this->consultRepository->getAllServiceTypes();
        return view('administrator.consult.services')->with(
            [
                'services' => $services,
                'types' => $types,
            ]
        );
    }

    public function serviceShow(ConsultService $consultService)
    {
        $types = $this->consultRepository->getAllServiceTypes();
        return view('administrator.consult.service-show')->with(
            [
                'service' => $consultService,
                'types' => $types,
            ]
        );
    }

    public function serviceStore(Request $request): \Illuminate\Http\RedirectResponse
    {
        $msg = $this->consultRepository->storeService($request);
        return back()->with($msg['session'], $msg['message']);
    }

    public function serviceUpdate(Request $request, ConsultService $consultService): \Illuminate\Http\RedirectResponse
    {
        $msg = $this->consultRepository->updateService($request, $consultService);
        return back()->with($msg['session'], $msg['message']);
    }

    public function serviceDelete(ConsultService $consultService): \Illuminate\Http\RedirectResponse
    {
        $consultService->consultPrice()->delete();
        $consultService->delete();
        return redirect()->route('consult-services')->with('success', 'Записът е успешно изтрит!');
    }

    public function serviceTypeShow(ConsultType $consultType)
    {
        return view('administrator.consult.type-show')->with(
            [
                'type' => $consultType,
            ]
        );
    }

    public function servicePrice(Request $request, ConsultService $consultService): \Illuminate\Http\RedirectResponse
    {
        $msg = $this->consultRepository->setOrUpdatePrice($request, $consultService);
        return back()->with($msg['session'], $msg['message']);
    }

    public function businessFormShow()
    {
        $type = ConsultType::findOrFail(2);
        return view('administrator.consult.business-form-show')->with(
            [
                'type' => $type,
            ]
        );
    }

    public function  commentsIndex(ConsultType $consultType)
    {
        $comments = ConsultComment::where('consult_type_id', $consultType->id)->orderByDesc('created_at')->paginate(5);
        return view('administrator.consult.comments',
            compact('consultType', 'comments'));
    }

    public function  showHide(ConsultComment $consultComment, $value)
    {
        if ($value !== 'show' && $value !== 'hide') return redirect()->back()->with('danger', 'Не е зададена вярна стойност');
        if($value == 'show') {
            $consultComment->update(['is_visible' => true]);
            $msg = [
                'session' => 'success',
                'message' => 'Коментарът е успешно възстановен и може да се вижда от всички',
            ];
        }
        if($value == 'hide') {
            $consultComment->update(['is_visible' => false]);
            $msg = [
                'session' => 'warning',
                'message' => 'Коментарът е успешно възстановен и не може да се вижда от всички',
            ];
        }

        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function turnOnOff($id, $value)
    {
        if($value !== 'on' && $value !== 'off') return back();
        $type = ConsultTypesStatus::where('consult_type_id', $id)->first();
        if($value == 'on'){
            $type->update(['is_on' => true]);
            $msg = 'Консултациите са включени';
        }
        if($value == 'off'){
            $type->update(['is_on' => false]);
            $msg = 'Консултациите са изключени';
        }
        return back()->with('success', $msg);
    }

    public function description(ConsultType $consultType)
    {
        return view('administrator.consult.description', compact('consultType'));
    }

    public function descriptionStore(Request $request, ConsultType $consultType): \Illuminate\Http\RedirectResponse
    {
        $data = $request->validate(
            [
                'description' => ['required', 'min:3', 'max:5000'],
            ]
        );
        $consultType->description()->create($data);
        return redirect()->back()->with('success', 'Успешно зададено описание!');
    }

    public function descriptionUpdate(Request $request, ConsultDescription $consultDescription): \Illuminate\Http\RedirectResponse
    {
        $data = $request->validate(
            [
                'description' => ['required', 'min:3', 'max:5000'],
            ]
        );
        $consultDescription->update($data);
        return redirect()->back()->with('success', 'Успешно обновено описание!');
    }

}
