<?php

namespace App\Http\Middleware;

use App\Models\Administrator\Analyst;
use App\Models\Administrator\AnalystArchive;
use Carbon\Carbon;
use Closure;

class MoveToArchiveMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $today = Carbon::today();
        $archives = Analyst::where('created_at', '<', $today->subMonths(3))->get();
        foreach ($archives as $archive){
            AnalystArchive::create($archive->toArray());
            $archive->delete();
        }
        return $next($request);
    }
}
