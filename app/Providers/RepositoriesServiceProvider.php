<?php

namespace App\Providers;

use App\Repositories\Analyst\AnalystRepository;
use App\Repositories\Analyst\AnalystRepositoryInterface;
use App\Repositories\BooksRepository;
use App\Repositories\BooksRepositoryInterface;
use App\Repositories\CommentsRepository;
use App\Repositories\CommentsRepositoryInterface;
use App\Repositories\ConsultRepository;
use App\Repositories\ConsultRepositoryInterface;
use App\Repositories\DraftsRepository;
use App\Repositories\DraftsRepositoryInterface;
use App\Repositories\HomeInfoRepository;
use App\Repositories\HomeInfoRepositoryInterface;
use App\Repositories\KeywordRepository;
use App\Repositories\KeywordRepositoryInterface;
use App\Repositories\PostsRepository;
use App\Repositories\PostsRepositoryInterface;
use App\Repositories\ProfilesRepository;
use App\Repositories\ProfilesRepositoryInterface;
use App\Repositories\RatingsRepository;
use App\Repositories\RatingsRepositoryInterface;
use App\Repositories\RecipesRepository;
use App\Repositories\RecipesRepositoryInterface;
use App\Repositories\RolesRepository;
use App\Repositories\RolesRepositoryInterface;
use App\Repositories\TagsRepository;
use App\Repositories\TagsRepositoryInterface;
use App\Repositories\TrafficRepository;
use App\Repositories\TrafficRepositoryInterface;
use App\Repositories\TypesRepository;
use App\Repositories\TypesRepositoryInterface;
use App\Repositories\UsersRepository;
use App\Repositories\UsersRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(TrafficRepositoryInterface::class, TrafficRepository::class);
        $this->app->singleton(RolesRepositoryInterface::class, RolesRepository::class);
        $this->app->singleton(UsersRepositoryInterface::class, UsersRepository::class);
        $this->app->singleton(TypesRepositoryInterface::class, TypesRepository::class);
        $this->app->singleton(ProfilesRepositoryInterface::class, ProfilesRepository::class);
        $this->app->singleton(HomeInfoRepositoryInterface::class, HomeInfoRepository::class);
        $this->app->singleton(TagsRepositoryInterface::class, TagsRepository::class);
        $this->app->singleton(DraftsRepositoryInterface::class, DraftsRepository::class);
        $this->app->singleton(KeywordRepositoryInterface::class, KeywordRepository::class);
        $this->app->singleton(RecipesRepositoryInterface::class, RecipesRepository::class);
        $this->app->singleton(CommentsRepositoryInterface::class, CommentsRepository::class);
        $this->app->singleton(RatingsRepositoryInterface::class, RatingsRepository::class);
        $this->app->singleton(BooksRepositoryInterface::class, BooksRepository::class);
        $this->app->singleton(PostsRepositoryInterface::class, PostsRepository::class);
        $this->app->singleton(ConsultRepositoryInterface::class, ConsultRepository::class);
        $this->app->singleton(AnalystRepositoryInterface::class, AnalystRepository::class);
    }
}
