<?php

namespace App\Providers;

use App\Models\Consult\ConsultActivity;
use App\Models\Consult\ConsultMessage;
use App\Models\Consult\ConsultTypesStatus;
use App\Models\HomeInfo;
use App\Models\Message;
use App\Models\MessageType;
use App\Models\Profile;
use App\Models\Type;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
            view()->composer(['partials.navbar', 'administrator.consult.consult-layout'], function ($view) {
                $view->with
                ([
                    'types' => Type::all(),
                    'homeInfo' => HomeInfo::find(1),
                    'messages' => Message::all(),
                    'unread' => Message::where('is_read', 0)->get(),
                    'buyType' => Message::where('type', 1)->where('is_read', 0)->get(),
                    'contactType' => Message::where('type', 2)->where('is_read', 0)->get(),
                    'unreadConsult' => ConsultMessage::where('is_read', 0)->get(),
                    'on' => ConsultActivity::firstOrFail(),
                    'consultBeginner' => ConsultTypesStatus::where('consult_type_id', 1)->first(),
                    'consultBusiness' => ConsultTypesStatus::where('consult_type_id', 2)->first(),
                ]);
            });

    }

//create a function independently

    public function partialNav()
    {
        //
    }
}
